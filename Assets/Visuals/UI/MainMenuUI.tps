<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>MainMenuUI.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Raw/bannerHanging.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>134,30,268,59</rect>
                <key>scale9Paddings</key>
                <rect>134,30,268,59</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/bannerModern.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>132,25,264,50</rect>
                <key>scale9Paddings</key>
                <rect>132,25,264,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/bannerScroll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>130,32,261,63</rect>
                <key>scale9Paddings</key>
                <rect>130,32,261,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/button_metalBlank.png</key>
            <key type="filename">Raw/button_metalCircle.png</key>
            <key type="filename">Raw/button_metalClose.png</key>
            <key type="filename">Raw/button_woodBlank.png</key>
            <key type="filename">Raw/button_woodCircle.png</key>
            <key type="filename">Raw/button_woodClose.png</key>
            <key type="filename">Raw/button_woodPaperBlank.png</key>
            <key type="filename">Raw/button_woodPaperCircle.png</key>
            <key type="filename">Raw/button_woodPaperClose.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/button_rectangleMetal.png</key>
            <key type="filename">Raw/button_rectangleMetalClose.png</key>
            <key type="filename">Raw/button_rectangleRed.png</key>
            <key type="filename">Raw/button_rectangleRedClose.png</key>
            <key type="filename">Raw/button_rectangleWood.png</key>
            <key type="filename">Raw/button_rectangleWoodClose.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,10,40,20</rect>
                <key>scale9Paddings</key>
                <rect>20,10,40,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/hexagon_metal.png</key>
            <key type="filename">Raw/hexagon_metalDark.png</key>
            <key type="filename">Raw/hexagon_metalGreen.png</key>
            <key type="filename">Raw/hexagon_metalRed.png</key>
            <key type="filename">Raw/hexagon_wood.png</key>
            <key type="filename">Raw/hexagon_woodPaper.png</key>
            <key type="filename">Raw/hexagon_woodPaperWear.png</key>
            <key type="filename">Raw/hexagon_woodWear.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,32,52,64</rect>
                <key>scale9Paddings</key>
                <rect>26,32,52,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapDirection_E.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,15,21</rect>
                <key>scale9Paddings</key>
                <rect>8,11,15,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapDirection_N.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,20,21</rect>
                <key>scale9Paddings</key>
                <rect>10,11,20,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapDirection_S.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,17,21</rect>
                <key>scale9Paddings</key>
                <rect>8,11,17,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapDirection_W.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,11,26,21</rect>
                <key>scale9Paddings</key>
                <rect>13,11,26,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapIcon_arrowA.png</key>
            <key type="filename">Raw/minimapIcon_arrowD.png</key>
            <key type="filename">Raw/progress_blueBorderSmall.png</key>
            <key type="filename">Raw/progress_greenBorderSmall.png</key>
            <key type="filename">Raw/progress_redBorderSmall.png</key>
            <key type="filename">Raw/progress_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapIcon_arrowB.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,9,16,18</rect>
                <key>scale9Paddings</key>
                <rect>8,9,16,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapIcon_arrowC.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapIcon_exclamationRed.png</key>
            <key type="filename">Raw/minimapIcon_exclamationWhite.png</key>
            <key type="filename">Raw/minimapIcon_exclamationYellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,7,6,14</rect>
                <key>scale9Paddings</key>
                <rect>3,7,6,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapIcon_jewelRed.png</key>
            <key type="filename">Raw/minimapIcon_jewelWhite.png</key>
            <key type="filename">Raw/minimapIcon_jewelYellow.png</key>
            <key type="filename">Raw/minimapIcon_starRed.png</key>
            <key type="filename">Raw/minimapIcon_starWhite.png</key>
            <key type="filename">Raw/minimapIcon_starYellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapMap_blank.png</key>
            <key type="filename">Raw/minimapMap_stoneRing.png</key>
            <key type="filename">Raw/minimapMap_woodRing.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/minimapMap_stone.png</key>
            <key type="filename">Raw/minimapMap_wood.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,68,136,136</rect>
                <key>scale9Paddings</key>
                <rect>68,68,136,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/panel_blueprintPaperWear.png</key>
            <key type="filename">Raw/panel_blueprintWear.png</key>
            <key type="filename">Raw/panel_boltsBlue.png</key>
            <key type="filename">Raw/panel_boltsDetail.png</key>
            <key type="filename">Raw/panel_boltsDetailSquare.png</key>
            <key type="filename">Raw/panel_boltsDetailSquare_blank.png</key>
            <key type="filename">Raw/panel_boltsGreen.png</key>
            <key type="filename">Raw/panel_boltsRed.png</key>
            <key type="filename">Raw/panel_metal.png</key>
            <key type="filename">Raw/panel_metalBlank.png</key>
            <key type="filename">Raw/panel_metalBlue.png</key>
            <key type="filename">Raw/panel_metalBolts.png</key>
            <key type="filename">Raw/panel_metalBoltsDark.png</key>
            <key type="filename">Raw/panel_metalDark.png</key>
            <key type="filename">Raw/panel_metalGreen.png</key>
            <key type="filename">Raw/panel_metalRed.png</key>
            <key type="filename">Raw/panel_wood.png</key>
            <key type="filename">Raw/panel_woodArrows.png</key>
            <key type="filename">Raw/panel_woodDetail.png</key>
            <key type="filename">Raw/panel_woodDetailArrows.png</key>
            <key type="filename">Raw/panel_woodDetailSquare.png</key>
            <key type="filename">Raw/panel_woodDetail_blank.png</key>
            <key type="filename">Raw/panel_woodPaper.png</key>
            <key type="filename">Raw/panel_woodPaperArrows.png</key>
            <key type="filename">Raw/panel_woodPaperDetail.png</key>
            <key type="filename">Raw/panel_woodPaperDetailArrows.png</key>
            <key type="filename">Raw/panel_woodPaperDetailSquare.png</key>
            <key type="filename">Raw/panel_woodPaperWear.png</key>
            <key type="filename">Raw/panel_woodWear.png</key>
            <key type="filename">Raw/panel_woodWear_blank.png</key>
            <key type="filename">Raw/round_metal.png</key>
            <key type="filename">Raw/round_metalDark.png</key>
            <key type="filename">Raw/round_metalGreen.png</key>
            <key type="filename">Raw/round_metalRed.png</key>
            <key type="filename">Raw/round_wood.png</key>
            <key type="filename">Raw/round_woodPaper.png</key>
            <key type="filename">Raw/round_woodPaperWear.png</key>
            <key type="filename">Raw/round_woodWear.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/patternStripesMetal_large.png</key>
            <key type="filename">Raw/patternStripesMetal_small.png</key>
            <key type="filename">Raw/patternStripesRed_large.png</key>
            <key type="filename">Raw/patternStripesRed_small.png</key>
            <key type="filename">Raw/patternStripesShadow_large.png</key>
            <key type="filename">Raw/patternStripesShadow_small.png</key>
            <key type="filename">Raw/pattern_blueprint.png</key>
            <key type="filename">Raw/pattern_blueprintPaper.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/progress.png</key>
            <key type="filename">Raw/progress_blueBorder.png</key>
            <key type="filename">Raw/progress_greenBorder.png</key>
            <key type="filename">Raw/progress_redBorder.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,17,16,34</rect>
                <key>scale9Paddings</key>
                <rect>8,17,16,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/progress_blue.png</key>
            <key type="filename">Raw/progress_green.png</key>
            <key type="filename">Raw/progress_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,15,12,30</rect>
                <key>scale9Paddings</key>
                <rect>6,15,12,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/progress_blueSmall.png</key>
            <key type="filename">Raw/progress_greenSmall.png</key>
            <key type="filename">Raw/progress_redSmall.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,12,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,12,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/scrollbar_barPoint.png</key>
            <key type="filename">Raw/scrollbar_barRound.png</key>
            <key type="filename">Raw/scrollbar_metal.png</key>
            <key type="filename">Raw/scrollbar_metalRed.png</key>
            <key type="filename">Raw/scrollbar_stone.png</key>
            <key type="filename">Raw/scrollbar_wood.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,32,16,64</rect>
                <key>scale9Paddings</key>
                <rect>8,32,16,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Raw/scrollbar_barPointSmall.png</key>
            <key type="filename">Raw/scrollbar_barRoundSmall.png</key>
            <key type="filename">Raw/scrollbar_metalRedSmall.png</key>
            <key type="filename">Raw/scrollbar_metalSmall.png</key>
            <key type="filename">Raw/scrollbar_stoneSmall.png</key>
            <key type="filename">Raw/scrollbar_woodSmall.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,10,16,20</rect>
                <key>scale9Paddings</key>
                <rect>8,10,16,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>Raw</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
