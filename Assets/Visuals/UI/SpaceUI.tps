<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>SpaceUI.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Graphics/SpaceUI/barHorizontal_blue_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_blue_right.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_green_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_green_right.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_red_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_red_right.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_shadow_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_shadow_right.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_white_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_white_right.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_yellow_left.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_yellow_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,7,3,13</rect>
                <key>scale9Paddings</key>
                <rect>2,7,3,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/barHorizontal_blue_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_green_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_red_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_shadow_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_white_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barHorizontal_yellow_mid.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,7,8,13</rect>
                <key>scale9Paddings</key>
                <rect>4,7,8,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/barVertical_blue_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_blue_top.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_green_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_green_top.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_red_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_red_top.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_shadow_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_shadow_top.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_white_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_white_top.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_yellow_bottom.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_yellow_top.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,13,3</rect>
                <key>scale9Paddings</key>
                <rect>7,2,13,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/barVertical_blue_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_green_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_red_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_shadow_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_white_mid.png</key>
            <key type="filename">Graphics/SpaceUI/barVertical_yellow_mid.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,13,8</rect>
                <key>scale9Paddings</key>
                <rect>7,4,13,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/crossair_black.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_blue.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_red.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_white.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/crossair_blackOutline.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_blueOutline.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_redOutline.png</key>
            <key type="filename">Graphics/SpaceUI/crossair_whiteOutline.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,9,18,18</rect>
                <key>scale9Paddings</key>
                <rect>9,9,18,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/cursor_hand.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/cursor_pointer3D.png</key>
            <key type="filename">Graphics/SpaceUI/cursor_pointerFlat.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,10,12</rect>
                <key>scale9Paddings</key>
                <rect>5,6,10,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/cursor_pointer3D_shadow.png</key>
            <key type="filename">Graphics/SpaceUI/cursor_pointerFlat_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,7,10,13</rect>
                <key>scale9Paddings</key>
                <rect>5,7,10,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/dotBlue.png</key>
            <key type="filename">Graphics/SpaceUI/dotGreen.png</key>
            <key type="filename">Graphics/SpaceUI/dotRed.png</key>
            <key type="filename">Graphics/SpaceUI/dotWhite.png</key>
            <key type="filename">Graphics/SpaceUI/dotYellow.png</key>
            <key type="filename">Graphics/SpaceUI/dot_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,12,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,12,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/glassPanel.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_cornerBL.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_cornerBR.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_cornerTL.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_cornerTR.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_corners.png</key>
            <key type="filename">Graphics/SpaceUI/glassPanel_projection.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_blue.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_blueCorner.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_green.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_greenCorner.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_red.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_redCorner.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_yellow.png</key>
            <key type="filename">Graphics/SpaceUI/metalPanel_yellowCorner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/glassPanel_tab.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,28,50,56</rect>
                <key>scale9Paddings</key>
                <rect>25,28,50,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/metalPanel_plate.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,40,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,40,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Graphics/SpaceUI/squareBlue.png</key>
            <key type="filename">Graphics/SpaceUI/squareGreen.png</key>
            <key type="filename">Graphics/SpaceUI/squareRed.png</key>
            <key type="filename">Graphics/SpaceUI/squareWhite.png</key>
            <key type="filename">Graphics/SpaceUI/squareYellow.png</key>
            <key type="filename">Graphics/SpaceUI/square_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,7,9,13</rect>
                <key>scale9Paddings</key>
                <rect>5,7,9,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>Graphics</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
