﻿using Assets.GameServices;
using Assets.GameServices.PlayerData;
using Assets.GameServices.UserInterface;
using Assets.SharedValues.Boolean;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.MainMenu
{
    public class MainMenu : MonoBehaviour
    {
        private bool _Playing;
        private bool _Quit;

        [SerializeField] private int _GridSceneIndex;

        [SerializeField] private Button _PlayButton;

        [SerializeField] private BoolValueAssetRef _GamePausedAssetRef;
        private BoolValue _GamePaused;

        private void Start()
        {
            _PlayButton.interactable = true;

            GameService.Service<UserInterface>().SetConfig(UserInterface.GameMode.MAIN_MENU);
        }

        public void Play()
        {
            if (_Playing) return;
            _Playing = true;
            StartCoroutine(LoadGridScene());
        }

        private IEnumerator LoadGridScene()
        {
            //TODO - Transition screen
            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<BoolValue> loadPause = _GamePausedAssetRef.LoadAssetAsync();
            yield return loadPause;
            _GamePaused = loadPause.Result;
            _GamePaused.SetValue(false);

            yield return GameService.Service<ActiveProfile>().StartSession();

            Scene currentScene = SceneManager.GetActiveScene();
            AsyncOperation loadscene = SceneManager.LoadSceneAsync(_GridSceneIndex);
            yield return loadscene;

            AsyncOperation unloadScene = SceneManager.UnloadSceneAsync(currentScene);
            yield return unloadScene;
        }

        public void Quit()
        {
            if (_Quit) return;
            _Quit = true;
            Application.Quit();
        }
    }
}
