﻿using System;

namespace Assets.GridMode.Wave.API
{
    [Serializable]
    public class EnemyData
    {
        public int ID;
        public float SpawnTime;
        public bool Alive;
    }
}