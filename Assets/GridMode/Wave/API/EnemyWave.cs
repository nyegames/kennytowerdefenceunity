﻿using System;
using System.Collections.Generic;

namespace Assets.GridMode.Wave.API
{
    /// <summary>Contains the data of a wave that the player has to defeat</summary>
    [Serializable]
    public class EnemyWave
    {
        public List<EnemyData> Enemies;
    }
}
