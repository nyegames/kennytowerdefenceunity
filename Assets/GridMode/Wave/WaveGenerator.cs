﻿using Assets.Events;
using Assets.Events.Wave;
using Assets.GameServices;
using Assets.GameServices.UserInterface;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Float;
using Assets.SharedValues.Int32;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Wave
{
    /// <summary>
    /// Controls the Time that the Player is allowed to be in BuildMode
    /// </summary>
    public class WaveGenerator : MonoBehaviour
    {
        private bool _PreparingWave;

        [Header("Debug")]
        [SerializeField] private bool _DebugOn;
        [SerializeField] private int _EnemyCountOverride;
        [SerializeField] private int _EnemyTypeOveride;

        [Header("Wave Mode")]
        [SerializeField] private IntValueAssetRef _WaveNumberValueAssetRef;
        private IntValue _WaveNumber;
        [SerializeField] private FloatValueAssetRef _WaveDurationValueAssetRef;
        private FloatValue _WaveDurationValue;

        [SerializeField] private EnemyWave _CurrentWave;
        private bool _AllEnemiesDefeated = false;

        [Header("Events")]
        [SerializeField] private EnemyWaveEventAssetRef _EnemyWaveCreatedEventAssetRef;
        private EnemyWaveEvent _EnemyWaveCreatedEvent;

        [SerializeField] private EnemyWaveEventAssetRef _StartEnemyWaveEventAssetRef;
        private EnemyWaveEvent _StartEnemyWaveEvent;

        [SerializeField] private GameEventAssetRef _StartBuildModeAssetRef;
        private GameEvent _StartBuildModeEvent;

        private void OnEnabled()
        {
            _DebugOn = false;
        }

        private void OnValidate()
        {
            if (_DebugOn && _CurrentWave != null &&
                gameObject.activeSelf && Application.isPlaying)
            {
                StartCoroutine(GenerateNextWave());
            }
        }

        private IEnumerator Start()
        {
            GameService.Service<UserInterface>().SetConfig(UserInterface.GameMode.GAME_SESSION);

            AsyncOperationHandle<EnemyWaveEvent> loadWave = _EnemyWaveCreatedEventAssetRef.LoadAssetAsync();
            yield return loadWave;
            _EnemyWaveCreatedEvent = loadWave.Result;

            AsyncOperationHandle<IntValue> loadWaveNumber = _WaveNumberValueAssetRef.LoadAssetAsync();
            yield return loadWaveNumber;
            _WaveNumber = loadWaveNumber.Result;

            AsyncOperationHandle<GameEvent> loadBuildModeStart = _StartBuildModeAssetRef.LoadAssetAsync();
            yield return loadBuildModeStart;
            _StartBuildModeEvent = loadBuildModeStart.Result;

            AsyncOperationHandle<FloatValue> loadWaveDuration = _WaveDurationValueAssetRef.LoadAssetAsync();
            yield return loadWaveDuration;
            _WaveDurationValue = loadWaveDuration.Result;

            AsyncOperationHandle<EnemyWaveEvent> loadStartWave = _StartEnemyWaveEventAssetRef.LoadAssetAsync();
            yield return loadStartWave;
            _StartEnemyWaveEvent = loadStartWave.Result;
        }

        #region Callbacks

        public void PlayerLoaded(PlayerUnit player)
        {
            PrepareForNextWave();
        }

        public void AllEnemiesDefeated(EnemyWave wave)
        {
            _AllEnemiesDefeated = true;
        }

        /// <summary>
        /// Will generate the next wave, then triggers BuildMode = true.
        /// And starts the countdown to star the next wave
        /// </summary>
        public void PrepareForNextWave()
        {
            if (_PreparingWave) return;
            _PreparingWave = true;

            StartCoroutine(GenerateNextWave());
        }

        public void BuildModeOver()
        {
            _AllEnemiesDefeated = true;

            _PreparingWave = false;

            StartCoroutine(StartWaveMode());
        }

        #endregion

        /// <summary>
        /// Generates what wave is going to be next
        /// </summary>
        /// <returns></returns>
        private IEnumerator GenerateNextWave()
        {
            //TODO - Generate wave from data
            _CurrentWave = new EnemyWave { Enemies = new List<EnemyData>() };

            float currentTime = 0;
            int enemyCount = Random.Range(10, 20);

            if (_DebugOn && _EnemyCountOverride > 0)
            {
                enemyCount = _EnemyCountOverride;
            }

            for (int i = 0; i < enemyCount; i++)
            {
                int id = UnityEngine.Random.Range(0, 5);

                if (_DebugOn && _EnemyTypeOveride >= 0)
                {
                    id = _EnemyTypeOveride;
                }

                float nextSpawn = Random.Range(0.5f, 2f);
                currentTime += nextSpawn;

                EnemyData enemy = new EnemyData
                {
                    ID = id,
                    SpawnTime = currentTime
                };
                _CurrentWave.Enemies.Add(enemy);
            }

            _EnemyWaveCreatedEvent?.Raise(_CurrentWave);

            yield return new WaitForSeconds(0.1f);

            _StartBuildModeEvent?.Raise();
        }

        /// <summary>
        /// Start the WaveMode
        /// </summary>
        /// <returns></returns>
        private IEnumerator StartWaveMode()
        {
            yield return new WaitUntil(() => _StartBuildModeEvent != null);
            yield return new WaitUntil(() => _WaveDurationValue != null);
            yield return new WaitUntil(() => _StartEnemyWaveEvent != null);

            _WaveDurationValue.SetValue(0);

            _AllEnemiesDefeated = false;

            _StartEnemyWaveEvent?.Raise(_CurrentWave);

            while (!_AllEnemiesDefeated)
            {
                _WaveDurationValue.SetValue(_WaveDurationValue.GetValue() + Time.deltaTime);
                yield return null;
            }
        }
    }
}
