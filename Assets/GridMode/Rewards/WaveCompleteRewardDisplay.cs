﻿using System.Collections;
using Assets.Events;
using Assets.GameServices.PlayerData;
using Assets.GridMode.Rewards.Popup;
using Assets.GridMode.Units.Player;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Rewards
{
    /// <summary>
    /// Displays the WaveRewards that the player has earned from the defeated Wave, and then awards them to players profiles
    /// Once completed emits event to say rewards have been added
    /// </summary>
    public class WaveCompleteRewardDisplay : MonoBehaviour
    {
        private bool _ShowingPopups;
        [SerializeField] private AssetReferenceGameObject _RewardDisplayPopupAssetRef;

        [Header("Events")]
        [SerializeField] private GameEventAssetRef _PrepareNextWaveEventAssetRef;
        private GameEvent _PrepareNextWaveEvent;

        private PlayerUnit _Player;

        private IEnumerator Start()
        {
            AsyncOperationHandle<GameEvent> loadPrepare = _PrepareNextWaveEventAssetRef.LoadAssetAsync();
            yield return loadPrepare;
            _PrepareNextWaveEvent = loadPrepare.Result;
        }

        public void PlayerLoaded(PlayerUnit player)
        {
            _Player = player;
        }

        public void PlayerRewardsCalculated(WaveRewards rewards)
        {
            if (_ShowingPopups) return;
            _ShowingPopups = true;
            StartCoroutine(ShowPlayerRewardPopup(rewards));
        }

        private IEnumerator ShowPlayerRewardPopup(WaveRewards rewards)
        {
            //If Player dead, then you didnt complete the wave
            if (_Player.Health <= 0)
            {
                yield break;
            }

            AsyncOperationHandle<GameObject> load = _RewardDisplayPopupAssetRef.InstantiateAsync();
            yield return load;

            GameObject popupObj = load.Result;
            WaveCompletePopup popup = popupObj.GetComponent<WaveCompletePopup>();

            yield return popup.Display(rewards);

            Addressables.ReleaseInstance(popupObj);

            //The new wave can now be prepared, this means that you will enter BuildMode
            yield return new WaitUntil(() => _PrepareNextWaveEvent != null);
            _PrepareNextWaveEvent?.Raise();

            _ShowingPopups = false;
        }

    }
}
