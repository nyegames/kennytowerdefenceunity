﻿using System.Collections;
using Assets.SharedValues.Int32;
using TMPro;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Rewards.Popup
{
    public class WaveCompletePopup : MonoBehaviour
    {
        private bool _CloseButtonPressed;

        [SerializeField] private IntValueAssetRef _WaveNumberValueAssetRef;
        private IntValue _WaveNumber;

        [SerializeField] private TextMeshProUGUI _TitleText;

        public IEnumerator Display(WaveRewards rewards)
        {
            AsyncOperationHandle<IntValue> loadWaveNumber = _WaveNumberValueAssetRef.LoadAssetAsync();
            yield return loadWaveNumber;
            _WaveNumber = loadWaveNumber.Result;

            _TitleText.text = $"WAVE {_WaveNumber.GetValue()} COMPLETE";

            while (!_CloseButtonPressed)
            {
                yield return null;
            }
        }

        public void CloseButtonPressed()
        {
            _CloseButtonPressed = true;
        }
    }
}
