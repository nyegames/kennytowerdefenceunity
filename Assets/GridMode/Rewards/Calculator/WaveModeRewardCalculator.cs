﻿using Assets.Events.IntEvent;
using Assets.Events.Wave.Rewards;
using Assets.GameServices.PlayerData;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Int32;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Rewards.Calculator
{
    /// <summary>
    /// After the Wave has been defeated, this will calculate how much progression/coin the player has been rewarded
    /// </summary>
    public class WaveModeRewardCalculator : MonoBehaviour
    {
        //TODO - Emit an event detailing all the WaveRewards that have been achieved for this
        //TODO - Then the game can visualise these, and then continue with the BuildMode

        [Header("Wave Number")]
        [SerializeField] private IntValueAssetRef _WaveNumberAssetRef;
        private IntValue _WaveNumber;

        private bool _ValuesLoaded;

        [Header("Events")]
        [SerializeField] private IntEventAssetRef _PlayerEarnedExpEventAssetRef;
        private IntEvent _PlayerEarnedExpEvent;
        [SerializeField] private IntEventAssetRef _PlayerEarnedCoinEventAssetRef;
        private IntEvent _PlayerEarnedCoinEvent;

        [SerializeField] private WaveRewardsEventAssetRef _DefeatWaveRewardsEventAssetRef;
        private WaveRewardsEvent _DefeatWaveRewardsEvent;

        private bool _EventsLoaded;

        private DateTime _StartWave;

        private void Start()
        {
            StartCoroutine(LoadSharedValues());
            StartCoroutine(LoadEvents());
        }

        #region Loaders
        private IEnumerator LoadSharedValues()
        {
            AsyncOperationHandle<IntValue> loadWaveNumber = _WaveNumberAssetRef.LoadAssetAsync();
            yield return loadWaveNumber;
            _WaveNumber = loadWaveNumber.Result;

            _ValuesLoaded = true;
        }
        private IEnumerator LoadEvents()
        {
            AsyncOperationHandle<IntEvent> loadExp = _PlayerEarnedExpEventAssetRef.LoadAssetAsync();
            yield return loadExp;
            _PlayerEarnedExpEvent = loadExp.Result;

            AsyncOperationHandle<IntEvent> loadCoin = _PlayerEarnedCoinEventAssetRef.LoadAssetAsync();
            yield return loadCoin;
            _PlayerEarnedCoinEvent = loadCoin.Result;

            AsyncOperationHandle<WaveRewardsEvent> loadReward = _DefeatWaveRewardsEventAssetRef.LoadAssetAsync();
            yield return loadReward;
            _DefeatWaveRewardsEvent = loadReward.Result;

            _EventsLoaded = true;
        }
        #endregion

        private IEnumerator CalculatePlayerExpEarned(EnemyWave wave)
        {
            yield return new WaitUntil(() => _EventsLoaded);
            yield return new WaitUntil(() => _ValuesLoaded);

            ActiveProfile activeProfile = GameServices.GameService.Service<ActiveProfile>();

            TimeSpan waveDuration = DateTime.UtcNow - _StartWave;

            //How many seconds under 30 seconds did you this wave in?
            int bonusSeconds = 30 - (int)waveDuration.TotalSeconds;
            bonusSeconds = Math.Max(0, bonusSeconds);

            WaveRewards waveReward = new WaveRewards
            {
                EnemyExp = wave.Enemies.Sum(enemySpawn => GetEnemyBaseExp(enemySpawn.ID)),
                TimeBonusExp = bonusSeconds * 200,
                RandomBonusExp = UnityEngine.Random.Range(0, 150) + 10,
                EnemyCoin = wave.Enemies.Sum(s => GetEnemyCoinValue(s.ID)),
                TimeBonusCoins = bonusSeconds * 5,
                RandomBonusCoin = UnityEngine.Random.Range(0, 50) + 10,

                Duration = waveDuration,
                WaveDefeated = wave
            };

            activeProfile.AwardExp(waveReward.EnemyExp + waveReward.TimeBonusExp + waveReward.RandomBonusExp);
            activeProfile.AwardCoins(waveReward.EnemyCoin + waveReward.TimeBonusCoins + waveReward.RandomBonusCoin);
            //10% chance for 5 gems
            if (UnityEngine.Random.Range(0, 100) <= 10) activeProfile.AwardCoins(5);

            yield return activeProfile.WaveCompleted();

            _DefeatWaveRewardsEvent?.Raise(waveReward);
        }

        private int GetEnemyCoinValue(int enemyID)
        {
            //TODO - Use EnemyDatabase to retrieve a base coin value for killing this enemy
            return 5;
        }

        private int GetEnemyBaseExp(int enemySpawnID)
        {
            //TODO - Create EnemyDatabase which holds the data about enemies
            //TODO - Retrieve from the database using OperationAsync's
            return 10;
        }

        #region Callbacks

        public void OnWaveStarted(EnemyWave wave)
        {
            _StartWave = DateTime.UtcNow;
        }

        public void OnWaveDefeated(EnemyWave wave)
        {
            StartCoroutine(CalculatePlayerExpEarned(wave));
        }

        #endregion
    }
}
