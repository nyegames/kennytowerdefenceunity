﻿using System;
using Assets.GridMode.Wave.API;

namespace Assets.GridMode.Rewards
{
    [Serializable]
    public struct WaveRewards
    {
        /// <summary>How much exp earned from killing the enemies</summary>
        public int EnemyExp;

        /// <summary>Exp earned from doing it quickly</summary>
        public int TimeBonusExp;

        /// <summary>Random amount of exp earned from just finishing it</summary>
        public int RandomBonusExp;


        /// <summary>How many coins you earned killing this wave</summary>
        public int EnemyCoin;

        /// <summary>How many coins you earned from doing it quickly</summary>
        public int TimeBonusCoins;

        /// <summary>Random amount of coins given</summary>
        public int RandomBonusCoin;

        public EnemyWave WaveDefeated;

        public TimeSpan Duration;
    }
}
