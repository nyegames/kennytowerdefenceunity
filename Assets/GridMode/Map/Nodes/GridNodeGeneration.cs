﻿using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Assets.GridMode.Map.Nodes
{
    [CreateAssetMenu(menuName = "Map/Nodes/Generation")]
    public class GridNodeGeneration : ScriptableObject
    {
        //TODO - This will read from file to create the data

        [Header("Prefabs")]
        [SerializeField] private AssetReferenceGameObject[] _DefaultTiles;
        [SerializeField] private AssetReferenceGameObject[] _SpawnTiles;
        public AssetReferenceGameObject[] DefaultTiles => _DefaultTiles.ToArray();
        public AssetReferenceGameObject[] SpawnTiles => _SpawnTiles.ToArray();

        [Header("Size")]
        [SerializeField] private int _Columns = 7;
        [SerializeField] private int _Rows = 16;
        [SerializeField] private int _NodeSize = 1;

        public int Columns => _Columns;
        public int Rows => _Rows;
        public int NodeSize => _NodeSize;

        [Header("Spawn")]
        [SerializeField] private GridNodePosition[] _SpawnNodes;
        public GridNodePosition[] SpawnNodes => _SpawnNodes.ToArray();
    }
}
