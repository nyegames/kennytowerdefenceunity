﻿using System.Collections.Generic;

namespace Assets.GridMode.Map.Nodes
{
    public class GridNodeComparer : IEqualityComparer<GridNode>
    {
        #region Implementation of IEqualityComparer<in GridNode>

        public bool Equals(GridNode x, GridNode y)
        {
            if (x == null || y == null) return false;
            return x.Column == y.Column && x.Row == y.Row;
        }

        public int GetHashCode(GridNode obj)
        {
            return obj.Column.GetHashCode() ^ obj.Row.GetHashCode();
        }

        #endregion
    }
}