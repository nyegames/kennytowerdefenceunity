﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Units;
using Assets.GridMode.Units.Tower;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.GridMode.Map.Nodes
{
    [Serializable]
    public partial class GridNode : MonoBehaviour,
        IPointerDownHandler,
        IPointerUpHandler
    {
        [SerializeField] private Transform _VisualRoot;
        private Vector3 _DefaultVisualLocalPosition;

        [Header("Info")]
        [SerializeField] private int _Row;
        [SerializeField] private int _Column;
        [SerializeField] private bool _SpawnNode;
        public GridNodePosition GridPosition => new GridNodePosition(_Column, _Row);

        [Header("Ownership")]
        public GridMap Map;
        /// <summary>The unit that has been placed on this node</summary>
        public TowerUnit Tower;

        [Header("Units")]
        [SerializeField] private List<GridUnit> _Units = new List<GridUnit>();
        public List<GridUnit> Units => _Units.ToList();

        public int Row => _Row;
        public int Column => _Column;
        public bool IsSpawnNode => _SpawnNode;

        /// <summary>Is this GridNode free to use?</summary>
        public bool IsBlocked => _SpawnNode || (Tower != null && !Tower.IsDisabled);

        public event Action<GridNode> OnNodePressed;
        public event Action<GridNode> OnNodeReleased;

        public event Action<GridNode> OnTowerPlaced;

        public void LoadNode(GridNodePosition position, bool isSpawnNode, GameObject visualTile)
        {
            _SpawnNode = isSpawnNode;
            _Column = position.Column;
            _Row = position.Row;

            name = $"Node [{_Column},{_Row} ]";

            _DefaultVisualLocalPosition = _VisualRoot.localPosition;

            visualTile.transform.parent = _VisualRoot;
            visualTile.transform.localPosition = Vector3.zero;
        }

        public bool PlaceTower(TowerUnit tower)
        {
            //If you are trying to place nothing onto this, no
            if (tower == null) return false;
            //If the node is taken, then no, if you are re-dropping the same unit. Then thats fine, snap it back.
            if (Tower != null && tower != Tower) return false;

            if (_Units.Any()) return false;

            //The unit's old node, no longer has a unit on it
            if (tower.Node != null && tower.Node.Tower != null)
            {
                tower.Node.Tower = null;
            }

            //Give this node the tower
            Tower = tower;
            //Position the Tower on the node
            Tower.transform.parent = transform;
            tower.transform.localPosition = new Vector3(0, 0, 0);

            //Tell the tower, it is now on this node
            Tower.AttachToNode(this);

            OnTowerPlaced?.Invoke(this);

            return true;
        }

        public List<GridNode> GetNeighbours()
        {
            return Map.GetNeighbours(this);
        }

        private List<GridNode> GetNodesEitherSideInColumn(int col, int row, int count)
        {
            List<GridNode> nodes = new List<GridNode>();

            for (int i = 0; i <= count; i++)
            {
                //Check the node up from you
                int targetRow = row + i;
                GridNode tile = Map.GetNode(col, targetRow);
                if (tile != null) nodes.Add(tile);

                //Check the node down from you
                targetRow = row - i;
                tile = Map.GetNode(col, targetRow);
                if (tile != null) nodes.Add(tile);
            }

            return nodes.Distinct(new GridNodeComparer()).ToList();
        }

        public List<GridNode> GetRadiusOfNodes(int radius,
            bool ignoreSelf = true, bool ignoreSpawn = true, bool ignoreTower = true)
        {
            List<GridNode> targetNodes = new List<GridNode>();

            bool even = radius % 2 == 0;

            if (even)
            {
                radius = radius / 2;
                for (int i = 0; i <= radius; i++)
                {
                    targetNodes.AddRange(GetNodesEitherSideInColumn(Column - i, Row, radius));
                    targetNodes.AddRange(GetNodesEitherSideInColumn(Column + i, Row, radius));
                }
            }
            else if (radius == 1)
            {
                targetNodes = GetNeighbours();
            }
            else
            {
                int aroundTarget = 0;

                int distanceToCenter = radius - 1;
                GridNodePosition currentPosition = new GridNodePosition(Column - distanceToCenter, Row);

                //Get all nodes to the left of the target node
                for (int i = 0; i < distanceToCenter; i++)
                {
                    targetNodes.Add(Map.GetNode(currentPosition));

                    //Get the nodes above and below
                    for (int row = -aroundTarget; row <= aroundTarget; row++)
                    {
                        targetNodes.Add(Map.GetNode(currentPosition.Column, currentPosition.Row + row));
                    }

                    currentPosition = currentPosition + new GridNodePosition(1, 0);
                    aroundTarget += 1;
                }

                targetNodes.AddRange(GetNodesEitherSideInColumn(Column, Row, distanceToCenter));

                currentPosition = new GridNodePosition(Column + 1, Row);
                aroundTarget -= 1;

                //Get all nodes to the right of the target node
                for (int i = 0; i < distanceToCenter; i++)
                {
                    targetNodes.Add(Map.GetNode(currentPosition));

                    //Get the nodes above and below
                    for (int row = -aroundTarget; row <= aroundTarget; row++)
                    {
                        targetNodes.Add(Map.GetNode(currentPosition.Column, currentPosition.Row - row));
                    }

                    currentPosition = currentPosition + new GridNodePosition(1, 0);
                    aroundTarget -= 1;
                }
            }

            if (!ignoreSelf)
            {
                targetNodes.Add(this);
            }

            targetNodes = targetNodes.Where(s => s != null).ToList();
            targetNodes = targetNodes.Distinct(new GridNodeComparer()).ToList();

            if (ignoreSelf)
            {
                targetNodes.RemoveAll(s => s.Column == Column && s.Row == Row);
            }

            if (ignoreSpawn)
            {
                targetNodes.RemoveAll(s => s.IsSpawnNode);
            }

            if (ignoreTower)
            {
                targetNodes.RemoveAll(s => s.Tower != null);
            }

            return targetNodes;
        }

        public List<GridNode> GetConeOfNodes(TowerDirection direction,
            int coneWidth = 4,
            int minDistance = 0, int maxDistance = 10)
        {
            GridNode node = this;
            List<GridNode> targetNodes = new List<GridNode>();

            //Increment in the direction of the tower
            //Each increment of the direction +2 the each either side of that node for target nodes

            Vector2Int incrementDirection = new Vector2Int(0, 0);
            bool checkVertical = false;

            int distanceCount = 0;

            do
            {
                switch (direction)
                {
                    case TowerDirection.NORTH:
                        incrementDirection = new Vector2Int(0, 1);
                        checkVertical = true;
                        break;
                    case TowerDirection.EAST:
                        incrementDirection = new Vector2Int(1, 0);
                        checkVertical = false;
                        break;
                    case TowerDirection.SOUTH:
                        incrementDirection = new Vector2Int(0, -1);
                        checkVertical = true;
                        break;
                    case TowerDirection.WEST:
                        incrementDirection = new Vector2Int(-1, 0);
                        checkVertical = false;
                        break;
                }

                if (Map != null)
                {
                    node = node.Map.GetNode(node.Column + incrementDirection.x, node.Row + incrementDirection.y);
                    if (node != null)
                    {
                        if (node.IsSpawnNode)
                        {
                            break;
                        }

                        if (distanceCount >= minDistance && distanceCount <= maxDistance)
                        {
                            List<GridNode> lineNodes = new List<GridNode>();

                            int sideCount = distanceCount / coneWidth;

                            for (int i = -sideCount; i <= sideCount; i++)
                            {
                                GridNode lineNode = checkVertical ?
                                    node.Map.GetNode(node.Column + i, node.Row) :
                                    node.Map.GetNode(node.Column, node.Row + i);

                                if (lineNode != null && lineNode.Tower == null)
                                {
                                    lineNodes.Add(lineNode);
                                }
                            }

                            targetNodes.AddRange(lineNodes);
                        }
                    }
                }

                distanceCount++;

            } while (node != null);

            return targetNodes;
        }
        public void UnitEntered(GridUnit unit)
        {
            if (_Units.Contains(unit)) return;
            _Units.Add(unit);
        }

        public void UnitExited(GridUnit unit)
        {
            if (!_Units.Contains(unit)) return;
            _Units.Remove(unit);
        }

        #region Implementation of IPointerDownHandler

        public void OnPointerDown(PointerEventData eventData)
        {
            //If you have a unit on your node, then ignore being pressed, the unit should handle it
            if (Tower != null) return;
            OnNodePressed?.Invoke(this);
            _VisualRoot.localPosition = new Vector3(0, _DefaultVisualLocalPosition.y - 0.1f, 0f);
        }

        #endregion

        #region Implementation of IPointerUpHandler

        public void OnPointerUp(PointerEventData eventData)
        {
            //If you have a unit on your node, then ignore being pressed, the unit should handle it
            if (Tower != null) return;
            OnNodeReleased?.Invoke(this);
            _VisualRoot.localPosition = _DefaultVisualLocalPosition;
        }

        #endregion
    }
}
