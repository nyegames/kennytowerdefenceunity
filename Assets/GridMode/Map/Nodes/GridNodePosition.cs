﻿using System;
using Assets.GridMode.Map.Influence;

namespace Assets.GridMode.Map.Nodes
{
    [Serializable]
    public struct GridNodePosition
    {
        public int Column;
        public int Row;

        public GridNodePosition(int column, int row)
        {
            Column = column;
            Row = row;
        }

        #region Equality members

        public bool Equals(GridNodePosition other)
        {
            return Column == other.Column && Row == other.Row;
        }

        public override bool Equals(object obj)
        {
            return obj is GridNodePosition other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Column * 397) ^ Row;
            }
        }

        #endregion

        public static GridNodePosition operator +(GridNodePosition a, GridNodePosition b)
            => new GridNodePosition(a.Column + b.Column, a.Row + b.Row);

        public static GridNodePosition operator -(GridNodePosition a, GridNodePosition b)
            => new GridNodePosition(a.Column - b.Column, a.Row - b.Row);

        public static bool operator ==(GridNodePosition a, GridNodePosition b)
        {
            return a.Column == b.Column && a.Row == b.Row;
        }

        public static bool operator !=(GridNodePosition a, GridNodePosition b)
        {
            return !(a == b);
        }

        public static implicit operator GridNodePosition(InfluenceNode node)
        {
            return new GridNodePosition(node.GridNode.Column, node.GridNode.Row);
        }

        public static implicit operator GridNodePosition(GridNode node)
        {
            return new GridNodePosition(node.Column, node.Row);
        }
    }

}