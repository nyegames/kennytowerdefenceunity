﻿using Assets.Events.BoolEvent;
using Assets.Events.Map;
using Assets.Events.Map.Nodes;
using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using Assets.SharedValues.Boolean;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Map
{
    public class GridMap : MonoBehaviour
    {
        [Header("Build Mode")]
        [SerializeField] private BoolValueAssetRef _BuildModeAssetRef;
        private BoolValue _BuildMode;
        public bool BuildMode => _BuildMode.GetValue();

        [Header("Nodes")]
        [SerializeField] private Transform _NodeRoot;
        [SerializeField] private AssetReferenceGameObject _GridNodeAssetRef;
        private GameObject _GridNodePrefab;
        [SerializeField] private GridNodeGeneration _Generation;

        private List<GameObject> _DefaultTilePrefabs;
        private List<GameObject> _SpawnTilePrefabs;

        public int Columns => _Generation.Columns;
        public int Rows => _Generation.Rows;
        private int GridNodeSize => _Generation.NodeSize;

        private GridNode[,] _GridNodes;
        public GridNode[,] Nodes => _GridNodes;

        [Header("Loading")]
        [SerializeField] private GridMapEventAssetRef _MapLoadedEventAssetRef;
        private GridMapEvent _MapLoadedEvent;

        [Header("Grid Node Interaction")]
        [SerializeField] private GridNodeEventAssetRef _GridNodePressedEventAssetRef;
        private GridNodeEvent _GridNodePressedEvent;

        [SerializeField] private GridNodeEventAssetRef _GridNodeReleasedEventAssetRef;
        private GridNodeEvent _GridNodeReleasedEvent;

        [Header("Tower Interactions")]
        [SerializeField] private GridNodeEventAssetRef _TowerBuildEventAssetRef;
        private GridNodeEvent _TowerBuildEvent;

        [Header("Mode Interactions")]
        [SerializeField] private BoolEventAssetRef _BuildModeToggleEventAssetRef;
        private BoolEvent _BuildModeToggledEvent;

        [Header("Influences")]
        [SerializeField] private List<InfluenceMap> _MapInfluences = new List<InfluenceMap>();

        public InfluenceMap GetInfluenceMap(InfluenceRule influenceRule) => _MapInfluences.FirstOrDefault(s => s.Key == influenceRule.name);

        public event Action<GridMap> OnNodesUpdated;

        private IEnumerator Start()
        {
            _MapInfluences = GetComponentsInChildren<InfluenceMap>().ToList();

            yield return LoadAddressables();

            yield return LoadGrid();
        }

        private IEnumerator LoadAddressables()
        {
            //Build Mode
            AsyncOperationHandle<BoolValue> loadBuildMode = _BuildModeAssetRef.LoadAssetAsync();
            yield return loadBuildMode;
            _BuildMode = loadBuildMode.Result;
            _BuildMode.OnValueChanged += BuildModeOnOnValueChanged;
            _BuildMode.SetValue(true);

            AsyncOperationHandle<BoolEvent> loadBuild = _BuildModeToggleEventAssetRef.LoadAssetAsync();
            yield return loadBuild;
            _BuildModeToggledEvent = loadBuild.Result;

            //Map
            AsyncOperationHandle<GridMapEvent> loadMap = _MapLoadedEventAssetRef.LoadAssetAsync();
            yield return loadMap;
            _MapLoadedEvent = loadMap.Result;

            //Grid Node
            AsyncOperationHandle<GridNodeEvent> loadNodePress = _GridNodePressedEventAssetRef.LoadAssetAsync();
            yield return loadNodePress;
            _GridNodePressedEvent = loadNodePress.Result;

            AsyncOperationHandle<GridNodeEvent> loadNodeReleased = _GridNodeReleasedEventAssetRef.LoadAssetAsync();
            yield return loadNodePress;
            _GridNodeReleasedEvent = loadNodeReleased.Result;

            //Grid Tower
            AsyncOperationHandle<GridNodeEvent> loadtower = _TowerBuildEventAssetRef.LoadAssetAsync();
            yield return loadtower;
            _TowerBuildEvent = loadtower.Result;

            //Grid Node Visuals
            AsyncOperationHandle<GameObject> loadGridNodePrefab = _GridNodeAssetRef.LoadAssetAsync();
            yield return loadGridNodePrefab;
            _GridNodePrefab = loadGridNodePrefab.Result;

            _DefaultTilePrefabs = new List<GameObject>();
            foreach (AssetReferenceGameObject assetReferenceGameObject in _Generation.DefaultTiles)
            {
                AsyncOperationHandle<GameObject> loadDefaultTile = assetReferenceGameObject.LoadAssetAsync();
                yield return loadDefaultTile;
                _DefaultTilePrefabs.Add(loadDefaultTile.Result);
            }

            _SpawnTilePrefabs = new List<GameObject>();
            foreach (AssetReferenceGameObject spawnTile in _Generation.SpawnTiles)
            {
                AsyncOperationHandle<GameObject> loadSpawnTile = spawnTile.LoadAssetAsync();
                yield return loadSpawnTile;
                _SpawnTilePrefabs.Add(loadSpawnTile.Result);
            }
        }

        public IEnumerator LoadGrid()
        {
            _GridNodes = new GridNode[Columns, Rows];

            for (int column = 0; column < Columns; column++)
            {
                for (int row = 0; row < Rows; row++)
                {
                    GameObject node = Instantiate(_GridNodePrefab, _NodeRoot);
                    int x = GridNodeSize * column;
                    int y = GridNodeSize * row;
                    node.transform.position = _NodeRoot.position + new Vector3(x, 0, y);

                    GridNode gridNode = node.GetComponent<GridNode>();
                    _GridNodes[column, row] = gridNode;

                    bool isSpawn = _Generation.SpawnNodes.Any(s => s.Column == column && s.Row == row);
                    GameObject prefab = isSpawn ? _SpawnTilePrefabs[UnityEngine.Random.Range(0, _SpawnTilePrefabs.Count)] :
                        _DefaultTilePrefabs[UnityEngine.Random.Range(0, _DefaultTilePrefabs.Count)];

                    GameObject tileVisual = Instantiate(prefab);
                    gridNode.LoadNode(new GridNodePosition(column, row), isSpawn, tileVisual);

                    gridNode.Map = this;
                    gridNode.OnNodePressed += OnGridNodePressed;
                    gridNode.OnNodeReleased += OnGridNodeReleased;
                    gridNode.OnTowerPlaced += OnTowerPlaced;


                    yield return null;
                }
            }

            foreach (InfluenceMap mapInfluencesValue in _MapInfluences)
            {
                mapInfluencesValue.OnMapLoaded(this);
            }
            _MapLoadedEvent?.Raise(this);

            OnNodesUpdated?.Invoke(this);
        }

        private void BuildModeOnOnValueChanged(bool buildModeActive)
        {
            _BuildModeToggledEvent?.Raise(buildModeActive);
        }

        private void OnTowerPlaced(GridNode obj)
        {
            foreach (InfluenceMap mapInfluence in _MapInfluences)
            {
                mapInfluence.OnTowerPlaced(obj);
            }
            _TowerBuildEvent?.Raise(obj);

            OnNodesUpdated?.Invoke(this);
        }

        private void OnGridNodePressed(GridNode node)
        {
            _GridNodePressedEvent?.Raise(node);
        }

        private void OnGridNodeReleased(GridNode node)
        {
            _GridNodeReleasedEvent?.Raise(node);
        }

        public GridNode GetNode(GridNodePosition position, bool clamp = false)
        {
            return GetNode(position.Column, position.Row, clamp);
        }

        public GridNode GetNode(int column, int row, bool clamp = false)
        {
            bool colMin = column < 0;
            bool colMax = column >= Columns;

            bool rowMin = row < 0;
            bool rowMax = row >= Rows;

            if (!clamp && (rowMin || rowMax || colMin || colMax))
            {
                return null;
            }

            if (colMin) column = 0;
            if (colMax) column = Columns - 1;
            if (rowMin) row = 0;
            if (rowMax) row = Rows - 1;

            return _GridNodes?[column, row];
        }

        public List<GridNode> GetNeighbours(GridNode node)
        {
            GridMap map = node.Map;
            int row = node.Row;
            int col = node.Column;

            List<GridNode> neighbours = new List<GridNode>
            {
                map.GetNode(col,row+1),
                map.GetNode(col+1,row),
                map.GetNode(col,row-1),
                map.GetNode(col-1,row),
            };

            List<GridNode> final = new List<GridNode>();
            foreach (GridNode neighbour in neighbours)
            {
                //Neighbour is null (out of bounds) or duplicated
                if (neighbour == null || final.Any(s => s.Equals(neighbour)))
                {
                    continue;
                }
                final.Add(neighbour);
            }

            return final;
        }

        #region Callbacks

        public void OnPlayerLoaded(PlayerUnit player)
        {
            foreach (InfluenceMap mapInfluence in _MapInfluences)
            {
                mapInfluence.OnPlayerLoaded(player);
            }
        }

        public void OnPlayerMoved(PlayerUnit player)
        {
            foreach (InfluenceMap mapInfluence in _MapInfluences)
            {
                mapInfluence.OnPlayerMoved(player);
            }
        }


        public void UpdateInfluenceMaps()
        {
            foreach (InfluenceMap mapInfluence in _MapInfluences)
            {
                mapInfluence.UpdateInfluence();
            }
        }

        #endregion
    }
}
