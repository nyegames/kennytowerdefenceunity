﻿using System;
using Assets.GridMode.Map.Nodes;

namespace Assets.GridMode.Map.Pathing
{
    public class PathNode
    {
        /// <summary>Distance from starting node</summary>
        public int GCost;

        /// <summary>Distance from end node</summary>
        public int HCost;

        /// <summary></summary>
        public int FCost;

        public GridNode GridNode;

        public PathNode ParentNode;

        #region Overrides of Object

        public override string ToString() => GridNode?.ToString();

        #endregion

        public static PathNode ToPathNode(GridNode node, GridNode start, GridNode end, PathNode parent)
        {
            PathNode pathNode = new PathNode
            {
                GCost = GetDistanceCost(node, start),
                HCost = GetDistanceCost(node, end),
                ParentNode = parent,
                GridNode = node
            };
            pathNode.FCost = pathNode.GCost + pathNode.HCost;
            return pathNode;
        }

        public static int GetDistanceCost(GridNode node, GridNode target) => Math.Abs((target.Row - node.Row) + (target.Column - node.Column));
    }
}