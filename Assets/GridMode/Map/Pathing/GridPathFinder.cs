﻿using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GridMode.Map.Pathing
{
    public class GridPathFinder : MonoBehaviour
    {
        //TODO - Creates a new object to manage the creation of a path, doing it this way means you can only store 1 path at a time per component
        public GridPathHandle FindPath(GridNode start, GridNode end)
        {
            return new GridPathHandle(start, end, coroutine => StartCoroutine(coroutine));
        }
    }
}
