﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GridMode.Map.Pathing
{
    public class GridPathHandle : CustomYieldInstruction
    {
        private bool _PathFound = false;

        /// <summary>Set of nodes to be checked</summary>
        private List<PathNode> _OpenList;

        /// <summary>Nodes that have already been checked</summary>
        private List<PathNode> _ClosedList;

        /// <summary>If true, then the <see cref="Path"/> will instead be a path to navigate you to the <see cref="GridNode"/> next to the <see cref="BlockingNode"/></summary>
        [HideInInspector] public bool Blocked;

        [HideInInspector] public List<GridNode> Path;

        //TODO - This seems arbitrary, maybe have it as real time seconds, somehow?
        private static int _NodeSearchForYielding = 120;
        private static int _YieldSteps;

        #region Overrides of CustomYieldInstruction

        public override bool keepWaiting => !_PathFound;

        #endregion

        public GridPathHandle(GridNode startNode, GridNode endNode,
            Action<IEnumerator> pathingcoroutine)
        {
            pathingcoroutine?.Invoke(CalculatePath(startNode, endNode));
        }

        private IEnumerator CalculatePath(GridNode start, GridNode end)
        {
            Blocked = true;

            _OpenList = new List<PathNode> { PathNode.ToPathNode(start, start, end, null) };
            _ClosedList = new List<PathNode>();

            while (_OpenList.Any())
            {
                PathNode currentNode = _OpenList[0];
                _OpenList.RemoveAt(0);
                _ClosedList.Add(currentNode);

                if (currentNode.GridNode.GridPosition == end.GridPosition)
                {
                    List<PathNode> nodePath = new List<PathNode>();
                    do
                    {
                        nodePath.Add(currentNode);
                        currentNode = currentNode.ParentNode;
                    } while (currentNode != null);
                    nodePath.Reverse();

                    List<GridNode> fullPath = nodePath.Select(s => s.GridNode).ToList();

                    Path = fullPath;

                    Blocked = false;
                    _PathFound = true;

                    yield break;
                }

                List<GridNode> currentNeighbours = currentNode.GridNode.Map.GetNeighbours(currentNode.GridNode);

                for (int i = 0; i < currentNeighbours.Count; i++)
                {
                    GridNode neighbourNode = currentNeighbours[i];

                    //This neighbour is not available to use, ignore it being blocked if it is our end goal
                    if (neighbourNode.GridPosition != end.GridPosition && 
                        neighbourNode.IsBlocked)
                    {
                        continue;
                    }

                    PathNode closedCopy = _ClosedList.FirstOrDefault(s => s.GridNode.GridPosition == neighbourNode.GridPosition);

                    //This neighbour is already in the closed list
                    if (closedCopy != null)
                    {
                        continue;
                    }

                    PathNode neighboutPathNode = PathNode.ToPathNode(neighbourNode, start, end, currentNode);

                    PathNode openCopy = _OpenList.FirstOrDefault(s => s.GridNode.GridPosition == neighboutPathNode.GridNode.GridPosition);

                    //This neighbour hasnt been checked yet, so add it immediately to the _OpenList
                    if (openCopy == null)
                    {
                        _OpenList.Add(neighboutPathNode);
                        continue;
                    }

                    if (neighboutPathNode.FCost < currentNode.FCost)
                    {
                        _OpenList.Add(neighboutPathNode);
                    }
                }

                _YieldSteps++;
                if (_YieldSteps % _NodeSearchForYielding == 0)
                {
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }
}
