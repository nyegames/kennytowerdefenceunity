﻿using Assets.GridMode.Map.Nodes;

namespace Assets.GridMode.Map.Influence
{
    public class InfluenceNode
    {
        public GridNode GridNode;
        public int InfluenceScore;

        public InfluenceNode(GridNode node, int influence)
        {
            GridNode = node;
            InfluenceScore = influence;
        }
    }
}