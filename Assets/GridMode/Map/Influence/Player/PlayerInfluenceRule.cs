﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using UnityEngine;

namespace Assets.GridMode.Map.Influence.Player
{
    [CreateAssetMenu(menuName = "Map/Influence/Rule/Player")]
    public class PlayerInfluenceRule : InfluenceRule
    {
        private PlayerUnit _PlayerUnit;

        public override bool OnPlayerLoaded(PlayerUnit playerUnit)
        {
            _PlayerUnit = playerUnit;
            return base.OnPlayerLoaded(playerUnit);
        }

        #region Overrides of InfluenceType

        public override bool IsDesiredNode(GridNode node)
        {
            return _PlayerUnit?.Node == node;
        }

        public override bool CanUpdateInfluence()
        {
            return _PlayerUnit != null;
        }

        #endregion
    }
}
