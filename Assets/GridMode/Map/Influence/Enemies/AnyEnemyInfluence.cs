﻿using System.Linq;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Enemies;
using UnityEngine;

namespace Assets.GridMode.Map.Influence.Enemies
{
    [CreateAssetMenu(menuName = "Map/Influence/Rule/AllEnemies")]
    public class AnyEnemyInfluence : InfluenceRule
    {
        #region Overrides of InfluenceType

        public override bool IsDesiredNode(GridNode node)
        {
            return node.Units.Any(s => s is EnemyUnit);
        }

        #endregion
    }
}
