﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using UnityEngine;

namespace Assets.GridMode.Map.Influence.Towers
{
    [CreateAssetMenu(menuName = "Map/Influence/Rule/AnyTower")]
    public class AnyTowerInfluenceRule : InfluenceRule
    {
        #region Overrides of InfluenceType

        public override bool IsDesiredNode(GridNode node)
        {
            if (node.Tower != null)
            {
                if (!node.Tower.IsDisabled)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public override bool IsForbiddenNode(GridNode node)
        {
            if (node.Tower != null)
            {
                if (node.Tower.IsDisabled)
                {
                    return true;
                }
            }

            return false;
        }

        public override bool OnPlayerLoaded(PlayerUnit playerUnit)
        {
            return false;
        }

        public override bool OnPlayerMoved(PlayerUnit player)
        {
            return false;
        }

        public override bool OnTowerPlaced(GridNode gridNode)
        {
            return true;
        }

        #endregion
    }
}
