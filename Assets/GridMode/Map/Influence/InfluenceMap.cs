﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using Assets.Tools;
using UnityEngine;

namespace Assets.GridMode.Map.Influence
{
    public sealed class InfluenceMap : MonoBehaviour
    {
        private static int _InfluenceSteps;
        private const int INFLUNCE_STEP_YIELD = 150;

        [Header("Editor")]
        [SerializeField] private bool _DrawGizmos;
        [SerializeField] private Color _StrongColour = Color.green;
        [SerializeField] private Color _WeakColour = Color.clear;

        [Header("Influence")]
        [SerializeField] private InfluenceRule _InfluenceRule;
        private GridMap _Map;

        private InfluenceNode[,] _InfluenceNodes;

        private bool _InfluenceUpdating;
        private bool _UpdateAgain;

        #region Callbacks

        public void OnMapLoaded(GridMap map)
        {
            _Map = map;
            _InfluenceNodes = new InfluenceNode[_Map.Columns, _Map.Rows];
            for (int column = 0; column < _InfluenceNodes.GetLength(0); column++)
            {
                for (int row = 0; row < _InfluenceNodes.GetLength(1); row++)
                {
                    _InfluenceNodes[column, row] = new InfluenceNode(null, 0);
                }
            }

            if (_InfluenceRule.OnMapLoaded(map))
            {
                UpdateInfluence();
            }
        }

        public void OnPlayerLoaded(PlayerUnit playerUnit)
        {
            if (_InfluenceRule.OnPlayerLoaded(playerUnit))
            {
                UpdateInfluence();
            }
        }

        public void OnPlayerMoved(PlayerUnit player)
        {
            if (_InfluenceRule.OnPlayerMoved(player))
            {
                UpdateInfluence();
            }
        }

        public void OnTowerPlaced(GridNode gridNode)
        {
            if (_InfluenceRule.OnTowerPlaced(gridNode))
            {
                UpdateInfluence();
            }
        }

        #endregion

        private IEnumerator UpdateInfluencePerNode()
        {
            while (!_InfluenceRule.CanUpdateInfluence())
            {
                yield return null;
            }

            if (_InfluenceUpdating)
            {
                _UpdateAgain = true;
                yield break;
            }
            _InfluenceUpdating = true;

            List<InfluenceNode> influenceNodes = new List<InfluenceNode>();
            List<GridNode> startingNodes = new List<GridNode>();

            foreach (GridNode gridNode in _Map.Nodes)
            {
                if (_InfluenceRule.IsDesiredNode(gridNode))
                {
                    startingNodes.Add(gridNode);
                }

                influenceNodes.Add(new InfluenceNode(gridNode, 0));
            }

            int columns = _Map.Columns;
            int rows = _Map.Rows;
            List<int[,]> influencerGridSnapshot = new List<int[,]>();

            //Loop through all the influencers on this grid
            foreach (GridNode startingNode in startingNodes)
            {
                //Record the influence values that this influencer starting node will record on the map
                int[,] influenceSnapShot = new int[columns, rows];

                Tuple<int, int> startingInfluenceNode = new Tuple<int, int>(startingNode.Column, startingNode.Row);

                List<Tuple<int, int>> openList = new List<Tuple<int, int>> { startingInfluenceNode };
                List<Tuple<int, int>> closedList = new List<Tuple<int, int>>();

                while (openList.Any())
                {
                    Tuple<int, int> currentNode = openList[0];
                    openList.RemoveAt(0);
                    closedList.Add(currentNode);

                    List<GridNode> targetNeighbours = _Map.GetNode(currentNode.Item1, currentNode.Item2).GetNeighbours();

                    List<Tuple<int, int>> influenceNeighbours =
                        targetNeighbours.Select(s => new Tuple<int, int>(s.Column, s.Row)).ToList();

                    foreach (Tuple<int, int> neighbourNode in influenceNeighbours)
                    {
                        int neighourColumn = neighbourNode.Item1;
                        int neighbourRow = neighbourNode.Item2;

                        Tuple<int, int> closedCopy = closedList.FirstOrDefault(s => s.Item1 == neighbourNode.Item1 &&
                                                                        s.Item2 == neighbourNode.Item2);

                        //This has already been checked
                        if (closedCopy != null)
                        {
                            continue;
                        }

                        //This neighbour hasn't been in the open list yet
                        Tuple<int, int> openCopy = openList.FirstOrDefault(s => s.Item1 == neighbourNode.Item1 &&
                                                                                    s.Item2 == neighbourNode.Item2);

                        if (openCopy == null)
                        {
                            int currentInfluence = influenceSnapShot[currentNode.Item1, currentNode.Item2];

                            if (_InfluenceRule.IsForbiddenNode(_Map.GetNode(currentNode.Item1, currentNode.Item2)))
                            {
                                //Ignore
                            }
                            else
                            {
                                influenceSnapShot[neighourColumn, neighbourRow] = currentInfluence + 1;
                                openList.Add(neighbourNode);
                            }
                        }
                    }

                    _InfluenceSteps++;
                    if (_InfluenceSteps > 0 && _InfluenceSteps % INFLUNCE_STEP_YIELD == 0)
                    {
                        yield return new WaitForEndOfFrame();
                    }

                }//openList empty

                influencerGridSnapshot.Add(influenceSnapShot);
            }

            if (startingNodes.Any())
            {
                //combine all influence snapshots into one
                int[,] combinedInfluence = new int[columns, rows];

                foreach (int[,] snapshot in influencerGridSnapshot)
                {
                    for (int column = 0; column < snapshot.GetLength(0); column++)
                    {
                        for (int row = 0; row < snapshot.GetLength(1); row++)
                        {
                            combinedInfluence[column, row] += snapshot[column, row];
                        }
                    }
                }

                //Then divide the result of each influence by how many influencer nodes there are
                for (int column = 0; column < combinedInfluence.GetLength(0); column++)
                {
                    for (int row = 0; row < combinedInfluence.GetLength(1); row++)
                    {
                        int combinedValue = combinedInfluence[column, row];
                        int nodeInfluenceValue = combinedValue / startingNodes.Count;

                        _InfluenceNodes[column, row].GridNode = _Map.GetNode(column, row);
                        _InfluenceNodes[column, row].InfluenceScore = nodeInfluenceValue;
                    }
                }
            }
            else
            {
                //Then divide the result of each influence by how many influencer nodes there are
                for (int column = 0; column < _InfluenceNodes.GetLength(0); column++)
                {
                    for (int row = 0; row < _InfluenceNodes.GetLength(1); row++)
                    {
                        _InfluenceNodes[column, row].GridNode = _Map.GetNode(column, row);
                        _InfluenceNodes[column, row].InfluenceScore = -1;
                    }
                }
            }

            if (_UpdateAgain)
            {
                _UpdateAgain = false;
                StartCoroutine(UpdateInfluencePerNode());
            }

            _UpdateAgain = false;

            _InfluenceUpdating = false;
        }

        public string Key => _InfluenceRule.name;

        public void UpdateInfluence()
        {
            if (_InfluenceUpdating)
            {
                _UpdateAgain = true;
                return;
            }
            StartCoroutine(UpdateInfluencePerNode());
        }

        public InfluenceNode GetNode(int column, int row)
        {
            if (column < 0 || row < 0) return null;
            if (column >= _InfluenceNodes.GetLength(0)) return null;
            if (row >= _InfluenceNodes.GetLength(1)) return null;

            return _InfluenceNodes[column, row];
        }

        public List<InfluenceNode> GetNeighbours(InfluenceNode node) => GetNeighbours(node.GridNode);

        public List<InfluenceNode> GetNeighbours(GridNode node)
        {
            List<GridNode> neighbors = node.GetNeighbours();
            List<InfluenceNode> influnceNeighbours = new List<InfluenceNode>();

            foreach (GridNode neighbor in neighbors)
            {
                InfluenceNode influ = GetNode(neighbor.Column, neighbor.Row);
                if (influ == null) continue;
                influnceNeighbours.Add(influ);
            }

            return influnceNeighbours;
        }

        #region Editor

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (!_DrawGizmos) return;
            if (_InfluenceNodes == null) return;

            InfluenceNode[] influenceNodes = _InfluenceNodes.Cast<InfluenceNode>().ToArray();
            int max = influenceNodes.Max(s => s.InfluenceScore);
            int min = influenceNodes.Min(s => s.InfluenceScore);

            foreach (InfluenceNode influenceNode in _InfluenceNodes)
            {
                if (influenceNode.InfluenceScore < 0) continue;

                GridNode node = influenceNode.GridNode;
                DrawString($"{influenceNode.InfluenceScore}", node.transform.position + new Vector3(0, 0.1f, 0));

                Color color = Color.Lerp(_StrongColour, _WeakColour, influenceNode.InfluenceScore.Normalise(min, max));
                Gizmos.color = color;
                float t = 0.98f;
                Vector3 nodeSize = new Vector3(t, 0.2f, t);
                Gizmos.DrawCube(node.transform.position + new Vector3(0, -nodeSize.y / 2f + 0.05f, 0), nodeSize);
            }
        }

        public static void DrawString(string text, Vector3 worldPos, Color? colour = null)
        {
            UnityEditor.Handles.BeginGUI();

            Color restoreColor = GUI.color;

            if (colour.HasValue) GUI.color = colour.Value;
            UnityEditor.SceneView view = UnityEditor.SceneView.currentDrawingSceneView;
            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                UnityEditor.Handles.EndGUI();
                return;
            }

            Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
            GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
        }

#endif
        #endregion
    }
}
