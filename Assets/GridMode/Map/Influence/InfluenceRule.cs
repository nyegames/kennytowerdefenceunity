﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using UnityEngine;

namespace Assets.GridMode.Map.Influence
{
    public abstract class InfluenceRule : ScriptableObject
    {
        //name - The identification of this influence type

        public abstract bool IsDesiredNode(GridNode node);

        public virtual bool IsForbiddenNode(GridNode node)
        {
            //Node is forbidden if there is a tower on it, that is not disabled
            return node.Tower != null && !node.Tower.IsDisabled;
        }

        public virtual bool CanUpdateInfluence()
        {
            return true;
        }

        public virtual bool OnMapLoaded(GridMap map)
        {
            return true;
        }

        public virtual bool OnPlayerLoaded(PlayerUnit playerUnit)
        {
            return true;
        }

        public virtual bool OnPlayerMoved(PlayerUnit player)
        {
            return true;
        }

        public virtual bool OnTowerPlaced(GridNode gridNode)
        {
            return true;
        }
    }
}
