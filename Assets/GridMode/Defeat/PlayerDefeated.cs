﻿using Assets.GameServices;
using Assets.GameServices.PlayerData;
using Assets.GridMode.Defeat.Popup;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Boolean;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

namespace Assets.GridMode.Defeat
{
    /// <summary>
    /// Show the GameOverPopup when the Player dies
    /// </summary>
    public class PlayerDefeated : MonoBehaviour
    {
        [Header("Popup")]
        [SerializeField] private AssetReferenceGameObject _GameOverPopupAssetRef;

        [Header("Game Pause")]
        [SerializeField] private BoolValueAssetRef _GamePauseValueAssetRef;
        private BoolValue _GamePauseValue;

        private EnemyWave _Wave;

        private bool _ShowingGameOver;

        private bool _ResourcedLoaded;
        private PlayerUnit _Player;

        private IEnumerator Start()
        {
            AsyncOperationHandle<BoolValue> loadGamePause = _GamePauseValueAssetRef.LoadAssetAsync();
            yield return loadGamePause;
            _GamePauseValue = loadGamePause.Result;

            _ResourcedLoaded = true;
        }

        #region Callbacks

        public void EnemyWaveStarted(EnemyWave wave)
        {
            _Wave = wave;
        }

        public void PlayerHasBeenDefeated(PlayerUnit player)
        {
            if (_ShowingGameOver) return;
            _ShowingGameOver = true;
            StartCoroutine(ShowGameOverPopup(player));
        }

        #endregion

        private IEnumerator ShowGameOverPopup(PlayerUnit player)
        {
            yield return new WaitUntil(() => _ResourcedLoaded);
            _GamePauseValue.SetValue(true);

            AsyncOperationHandle<GameObject> loadPopup = _GameOverPopupAssetRef.InstantiateAsync();
            yield return loadPopup;

            GameObject obj = loadPopup.Result;
            GameOverPopup popup = obj.GetComponent<GameOverPopup>();
            yield return popup.Display(_Wave);

            if (popup.RetryTriggered)
            {
                player.Revive();

                _GamePauseValue.SetValue(false);
            }

            Addressables.ReleaseInstance(obj);

            _ShowingGameOver = false;

            if (!popup.RetryTriggered)
            {
                yield return GameService.Service<ActiveProfile>().SessionEnded();

                //TODO - Create transition screen back to MainMenu
                Scene myScene = SceneManager.GetActiveScene();
                AsyncOperation loadMainMenu = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
                yield return loadMainMenu;

                _GamePauseValue.SetValue(false);
                AsyncOperation unload = SceneManager.UnloadSceneAsync(myScene);
                yield return unload;
            }
        }
    }
}
