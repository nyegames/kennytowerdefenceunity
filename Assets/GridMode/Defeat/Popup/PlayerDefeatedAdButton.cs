﻿using Assets.Events.BoolEvent;
using Assets.SharedValues.Int32;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace Assets.GridMode.Defeat.Popup
{
    [RequireComponent(typeof(Button))]
    public class PlayerDefeatedAdButton : MonoBehaviour,
        IUnityAdsListener
    {
        [SerializeField] private IntValueAssetRef _WaveNumberAssetRef;
        private IntValue _WaveNumber;

#if UNITY_ANDROID
        private String _GameID = "3620584";
#else
        private String _GameID = "";
#endif

        [SerializeField] private bool _TestMode = true;

        private string _PlayerDefeatedPlacementID = "PlayerDefeated";

        private Button _Button;

        private bool _AdvertLoaded;
        [SerializeField] private int _AdvertAvailableEveryWave = 5;

        public BoolUnityEvent PlayerDefeatedAdvertCompleted;

        private IEnumerator Start()
        {
            _Button = GetComponent<Button>();

            // Set interactivity to be dependent on the Placement’s status:
            _Button.interactable = Advertisement.IsReady(_PlayerDefeatedPlacementID);

            // Map the ShowRewardedVideo function to the button’s click listener:
            if (_Button) _Button.onClick.AddListener(ShowRewardedVideo);

            // Initialize the Ads listener and service:
            Advertisement.AddListener(this);
            Advertisement.Initialize(_GameID, _TestMode);

            AsyncOperationHandle<IntValue> loadWave = _WaveNumberAssetRef.LoadAssetAsync();
            yield return loadWave;
            _WaveNumber = loadWave.Result;

            UpdateButtonReadyState();
        }

        private void ShowRewardedVideo()
        {
            Advertisement.Show(_PlayerDefeatedPlacementID);
        }

        private void UpdateButtonReadyState()
        {
            if (!_AdvertLoaded || _WaveNumber == null)
            {
                _Button.interactable = false;
                return;
            }

            int waveNumber = _WaveNumber.GetValue();
            _Button.interactable = waveNumber == 0 || waveNumber % _AdvertAvailableEveryWave == 0;
        }

        #region Implementation of IUnityAdsListener

        public void OnUnityAdsReady(string placementId)
        {
            // If the ready Placement is rewarded, activate the button: 
            if (placementId == _PlayerDefeatedPlacementID)
            {
                _AdvertLoaded = true;
                UpdateButtonReadyState();
            }
        }

        public void OnUnityAdsDidError(string message) { }

        public void OnUnityAdsDidStart(string placementId) { }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (placementId == _PlayerDefeatedPlacementID)
            {
                PlayerDefeatedAdvertCompleted?.Invoke(showResult == ShowResult.Finished);
            }
        }

        #endregion
    }
}
