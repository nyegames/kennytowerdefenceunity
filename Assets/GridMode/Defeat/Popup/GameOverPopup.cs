﻿using Assets.GridMode.Wave.API;
using System.Collections;
using UnityEngine;

namespace Assets.GridMode.Defeat.Popup
{
    public class GameOverPopup : MonoBehaviour
    {
        private bool _ClosePopupNow;

        public bool RetryTriggered { get; private set; }

        public IEnumerator Display(EnemyWave wave)
        {
            yield return new WaitUntil(() => _ClosePopupNow);
        }

        public void RetryButtonPressed(bool advertRewarded)
        {
            RetryTriggered = advertRewarded;
            _ClosePopupNow = true;
        }

        public void CloseButtonPressed()
        {
            _ClosePopupNow = true;
        }
    }
}
