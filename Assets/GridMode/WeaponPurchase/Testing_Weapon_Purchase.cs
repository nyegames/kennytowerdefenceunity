﻿using System.Collections;
using Assets.Events.TowerWeapon;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.WeaponPurchase
{
    public class Testing_Weapon_Purchase : MonoBehaviour
    {
        [SerializeField] private AssetReferenceGameObject _WeaponPurchasePopupAssetRef;

        [SerializeField] private TowerWeaponEventAssetRef _FakeEquipTowerEventAssetRef;
        private TowerWeaponEvent _FakeEquipEvent;

        private IEnumerator Start()
        {
            AsyncOperationHandle<TowerWeaponEvent> loadEvent = _FakeEquipTowerEventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _FakeEquipEvent = loadEvent.Result;

            AsyncOperationHandle<GameObject> loadPopup = _WeaponPurchasePopupAssetRef.InstantiateAsync();
            yield return loadPopup;
            GameObject obj = loadPopup.Result;
            WeaponPurchasePopup purchaseWeapon = obj.GetComponent<WeaponPurchasePopup>();

            yield return purchaseWeapon.Display(null);
        }

        public void PurchaseWeapon(TowerWeaponType weapon, TowerUnit tower)
        {
            _FakeEquipEvent?.Raise(weapon, tower);
        }
    }
}
