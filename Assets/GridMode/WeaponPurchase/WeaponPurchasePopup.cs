﻿using System.Collections;
using Assets.Events.TowerWeapon;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.WeaponPurchase
{
    public class WeaponPurchasePopup : MonoBehaviour
    {
        [SerializeField] private bool _Displaying;
        [SerializeField] private bool _WeaponPurchased;

        [SerializeField] private TowerWeaponType _WeaponType;
        private TowerUnit _Tower;

        [Header("Events")]
        [SerializeField] private TowerWeaponEventAssetRef _PurchaseTowerWeaponEventAssetRef;
        private TowerWeaponEvent _PurchaseTowerWeaponEvent;

        public IEnumerator Display(TowerUnit tower)
        {
            _WeaponPurchased = false;

            AsyncOperationHandle<TowerWeaponEvent> loadPurchaseEvent = _PurchaseTowerWeaponEventAssetRef.LoadAssetAsync();
            yield return loadPurchaseEvent;
            _PurchaseTowerWeaponEvent = loadPurchaseEvent.Result;

            _Tower = tower;
            _Displaying = true;

            while (_Displaying)
            {
                if (_WeaponPurchased)
                {
                    break;
                }

                yield return null;
            }

            //TODO - Exit animation

            Destroy(gameObject);
        }

        public void ClosePopup()
        {
            _Displaying = false;
        }

        public void PurchaseWeapon(TowerWeaponType weapon, TowerUnit tower)
        {
            //TODO - Require WeaponPurchaseFailedEvent to know if it hasnt succeeded
            _WeaponType = weapon;
            _PurchaseTowerWeaponEvent?.Raise(_WeaponType, _Tower);
        }

        public void WeaponEquip(TowerWeaponType weapon, TowerUnit tower)
        {
            if (weapon.name == _WeaponType.name &&
                _Tower.Node.Column == tower.Node.Column &&
                _Tower.Node.Row == tower.Node.Row)
            {
                _WeaponPurchased = true;
            }
        }
    }
}
