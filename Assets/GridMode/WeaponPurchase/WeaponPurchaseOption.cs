﻿using Assets.Events.TowerWeapon;
using Assets.GridMode.Units.Weapons;
using UnityEngine;

namespace Assets.GridMode.WeaponPurchase
{
    public class WeaponPurchaseOption : MonoBehaviour
    {
        [SerializeField] private TowerWeaponType _WeaponType;

        [SerializeField] private TowerWeaponUnityEvent _PurchaseWeapon;

        public void Purchase()
        {
            _PurchaseWeapon?.Invoke(_WeaponType, null);
        }
    }
}
