﻿using Assets.Events.Tower;
using Assets.GameServices;
using Assets.GameServices.PlayerData;
using Assets.GameServices.PlayerData.API;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Assets.GridMode.Control.Towers
{
    public class TowerBuilder : MonoBehaviour
    {
        [SerializeField] private AssetLabelReference _TowerLabel;
        [SerializeField] private AssetLabelReference _WeaponLabel;

        [SerializeField] private GameObject[] _TowerPrefabs;
        [SerializeField] private GameObject[] _WeaponPrefabs;

        [Header("Events")]
        [SerializeField] private TowerUnitEventAssetRef _TowerBuiltEventAssetRef;
        private TowerUnitEvent _TowerBuiltEvent;

        [SerializeField] private TowerUnitEventAssetRef _TowerRemovedEventAssetRef;
        private TowerUnitEvent _TowerRemovedEvent;

        private IEnumerator Start()
        {
            AsyncOperationHandle<IList<IResourceLocation>> findTowers = Addressables.LoadResourceLocationsAsync(_TowerLabel);
            yield return findTowers;
            AsyncOperationHandle<IList<GameObject>> loadTowers = Addressables.LoadAssetsAsync<GameObject>(findTowers.Result, towerCB => { });
            yield return loadTowers;
            _TowerPrefabs = loadTowers.Result.ToArray();

            AsyncOperationHandle<IList<IResourceLocation>> findWeapons = Addressables.LoadResourceLocationsAsync(_WeaponLabel);
            yield return findWeapons;
            AsyncOperationHandle<IList<GameObject>> loadWeapons = Addressables.LoadAssetsAsync<GameObject>(findWeapons.Result, weaponCB => { });
            yield return loadWeapons;
            _WeaponPrefabs = loadWeapons.Result.ToArray();

            AsyncOperationHandle<TowerUnitEvent> loadTowerRemoved = _TowerRemovedEventAssetRef.LoadAssetAsync();
            yield return loadTowerRemoved;
            _TowerRemovedEvent = loadTowerRemoved.Result;

            AsyncOperationHandle<TowerUnitEvent> loadTowerBuilt = _TowerBuiltEventAssetRef.LoadAssetAsync();
            yield return loadTowerBuilt;
            _TowerBuiltEvent = loadTowerBuilt.Result;
        }

        public void BuildTowerBase(GridNode node)
        {
            //TODO - Make error message GameService that will show little Toast messages on the screen

            if (!node.Map.BuildMode) return;
            if (node.IsSpawnNode) return;
            //Cannot create a Unit on a node that already has a unit
            if (node.Tower != null) return;
            //If there are any units on this square you cannot place it here
            if (node.Units?.Any() ?? false) return;

            GameObject towerBaseObj = _TowerPrefabs[0];
            TowerUnit towerUnitPrefab = towerBaseObj.GetComponent<TowerUnit>();

            ActiveProfile activeProfile = GameService.Service<ActiveProfile>();
            PlayerProfileSession? session = activeProfile?.GetActiveProfile().Session;

            if (session == null || session.Value.TowerPoints < towerUnitPrefab.BuildCost)
            {
                //TODO - Show error message for not able to afford
                return;
            }

            if (!activeProfile.RemoveTowerPoints(towerUnitPrefab.BuildCost))
            {
                //TODO - Show error
                return;
            }

            GameObject towerObj = Instantiate(towerBaseObj, node.transform);
            TowerUnit gridTower = towerObj.GetComponent<TowerUnit>();

            node.PlaceTower(gridTower);

            _TowerBuiltEvent?.Raise(gridTower);
        }

        public void PurchaseTowerWeapon(TowerWeaponType weaponType, TowerUnit tower)
        {
            if (tower == null) return;
            if (!tower.Node.Map.BuildMode) return;
            if (tower.Node.IsSpawnNode) return;
            if (tower.Weapon != null) return;



            //TODO - check if player can afford
            foreach (GameObject weaponPrefab in _WeaponPrefabs)
            {
                TowerWeapon weapon = weaponPrefab.GetComponent<TowerWeapon>();
                if (weaponType.name == weapon.WeaponType.name)
                {
                    int weaponCost = weapon.BuildCost;
                    ActiveProfile activeProfile = GameService.Service<ActiveProfile>();
                    if (!activeProfile.RemoveTowerPoints(weaponCost))
                    {
                        //TODO - Show error message saying you cant afford this
                        return;
                    }

                    GameObject weaponObj = Instantiate(weaponPrefab, tower.WeaponSlot);
                    tower.SetWeapon(weaponObj);
                    return;
                }
            }
        }

        /// <summary>
        /// Destroy a tower that has been requested by the Player</summary>
        /// <param name="tower"></param>
        public void SellTower(TowerUnit tower)
        {
            if (tower == null) return;

            int towerCost = tower.BuildCost;

            //TODO - Calculate value of weapon
            if (tower.Weapon != null)
            {
                towerCost += tower.Weapon.BuildCost;
            }

            ActiveProfile activeProfile = GameService.Service<ActiveProfile>();
            activeProfile.AddTowerPoints(towerCost);

            tower.Node.Tower = null;

            _TowerRemovedEvent?.Raise(tower);

            Destroy(tower.gameObject);
        }

        public void PowerUpTower(TowerUnit tower)
        {
            //Cant power up a tower already powered
            if (tower == null || tower.Weapon == null || tower.Weapon.IsPowerActive) return;

            //TODO - Calculate if the player has the Power currency to do this

            tower.Weapon.SetPowerActive(true);
        }
    }
}
