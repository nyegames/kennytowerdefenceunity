﻿using Assets.Events.Map.Nodes;
using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Control.User
{
    public class UserPlayerMover : MonoBehaviour
    {
        [Header("Events")]
        [SerializeField] private GridNodeEventAssetRef _PlayerMovEventAssetRef;
        private GridNodeEvent _PlayerMoveEvent;

        private IEnumerator Start()
        {
            AsyncOperationHandle<GridNodeEvent> loadPlayerMove = _PlayerMovEventAssetRef.LoadAssetAsync();
            yield return loadPlayerMove;
            _PlayerMoveEvent = loadPlayerMove.Result;
        }

        public void UserCommandDetected(UserCommand command)
        {
            if (command.UserSelectionTriggered) return;
            if (command.Node == null) return;
            _PlayerMoveEvent?.Raise(command.Node);
        }
    }
}
