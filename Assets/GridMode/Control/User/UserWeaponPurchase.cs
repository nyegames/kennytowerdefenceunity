﻿using Assets.GridMode.Units.Tower;
using Assets.GridMode.WeaponPurchase;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Control.User
{
    public class UserWeaponPurchase : MonoBehaviour
    {
        [Header("Popups")]
        [SerializeField] private AssetReferenceGameObject _WeaponPurchasePopupAssetRef;
        private bool _ShowingWeaponPurchase;
        
        #region Popups

        public void ShowWeaponPurchasePopup(TowerUnit tower)
        {
            if (_ShowingWeaponPurchase) return;
            _ShowingWeaponPurchase = true;
            StartCoroutine(ShowingWeaponPurchasePopup(tower));
        }

        private IEnumerator ShowingWeaponPurchasePopup(TowerUnit tower)
        {
            AsyncOperationHandle<GameObject> load = _WeaponPurchasePopupAssetRef.InstantiateAsync();
            yield return load;
            GameObject obj = load.Result;

            WeaponPurchasePopup popup = obj.GetComponent<WeaponPurchasePopup>();
            yield return popup.Display(tower);

            _ShowingWeaponPurchase = false;
        }

        #endregion
    }
}
