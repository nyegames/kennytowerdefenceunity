﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.GridMode.Control.User
{
    public class UserTowerPreview : MonoBehaviour
    {
        [Header("Target Preview")]
        [SerializeField] private Transform _PreviewRoot;
        [SerializeField] private GameObject _TargetNodePreviewPrefab;
        private List<GameObject> _TargetNodePreviews;
        private TowerUnit _PreviewTower;

        private void Start()
        {
            _TargetNodePreviews = new List<GameObject>();
        }

        #region Callbacks

        public void TowerPressed(TowerUnit tower)
        {
            if (tower.Weapon == null) return;

            bool previouslySelected = tower == _PreviewTower;
            _PreviewTower = tower;
            //Active the preview on a newly selected tower
            PreviewTargetArea(!previouslySelected);
        }

        #endregion

        private void PreviewTargetArea(bool active)
        {
            foreach (GameObject targetNodePreview in _TargetNodePreviews)
            {
                targetNodePreview.SetActive(false);
            }

            if (active)
            {
                List<GridNode> targetNodes = _PreviewTower?.Weapon?.GetTargetNodes();
                if (targetNodes == null) return;

                for (int i = 0; i < targetNodes.Count; i++)
                {
                    GridNode targetNode = targetNodes[i];

                    if (i >= _TargetNodePreviews.Count)
                    {
                        _TargetNodePreviews.Add(Instantiate(_TargetNodePreviewPrefab, _PreviewRoot));
                    }

                    GameObject preview = _TargetNodePreviews[i];
                    preview.transform.position = targetNode.transform.position;
                    preview.SetActive(true);
                }
            }
            else
            {
                _PreviewTower = null;
            }
        }
    }
}
