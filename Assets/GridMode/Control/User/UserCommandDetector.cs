﻿using Assets.Events.User.Command;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using Assets.SharedValues.Boolean;
using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Control.User
{
    /// <summary>
    /// Detects what type of command the user is wanting to perform
    /// </summary>
    public class UserCommandDetector : MonoBehaviour
    {
        [Header("Build Mode")]
        [SerializeField] private BoolValueAssetRef _BuildModeAssetRef;
        private BoolValue _BuildModeValue;

        [Header("Trigger")]
        [Tooltip("How long after a GridNode is pressed before")]
        [SerializeField] private float _SecondsBeforeClickInvoked = 0.4f;
        [Tooltip("How long the current node has been pressed for")]
        [SerializeField] private float _TimeUserSelectionHeld = 0f;

        /// <summary>If true then the player has held the grid node too long to move the player there</summary>
        [SerializeField] private bool _UserSelectionThresholdReached;

        [Header("Events")]
        [SerializeField] private UserCommandEventAssetRef _CommandDetectedEventAssetRef;
        private UserCommandEvent _CommandDetectedEvent;

        private bool _UserSelectedSomething;

        private GridNode _TargetNode;
        private TowerUnit _TargetTower;

        private Vector3 _LastPressedPosition;

        private IEnumerator Start()
        {
            AsyncOperationHandle<BoolValue> loadBuildMode = _BuildModeAssetRef.LoadAssetAsync();
            yield return loadBuildMode;
            _BuildModeValue = loadBuildMode.Result;

            AsyncOperationHandle<UserCommandEvent> loadCommand = _CommandDetectedEventAssetRef.LoadAssetAsync();
            yield return loadCommand;
            _CommandDetectedEvent = loadCommand.Result;
        }

        #region User Interactions

        public void GridNodePressed(GridNode node)
        {
            UserSelectionPressed();

            _TargetTower = null;

            _TargetNode = node;
            _LastPressedPosition = node.transform.position;
        }

        public void GridNodeReleased(GridNode node)
        {
            if (!_UserSelectionThresholdReached)
            {
                TriggerUserCommand();
            }

            UserSelectionReleased();
        }

        public void TowerPressed(TowerUnit tower)
        {
            UserSelectionPressed();

            _TargetNode = tower.Node;
            _TargetTower = tower;

            _LastPressedPosition = tower.transform.position;

            TriggerUserCommand();
        }

        public void TowerReleased(TowerUnit tower)
        {
            UserSelectionReleased();
        }

        #endregion

        private void UserSelectionPressed()
        {
            //TODO - Event to hide all context menus
            //HideContextMenu();

            _UserSelectedSomething = true;

            _TimeUserSelectionHeld = 0;
            _UserSelectionThresholdReached = false;
        }

        private void UserSelectionReleased()
        {
            _UserSelectedSomething = false;
            _UserSelectionThresholdReached = false;
        }

        private void Update()
        {
            if (!_UserSelectedSomething) return;

            _TimeUserSelectionHeld += Time.deltaTime;

            //If the hold threshold has been reached then trigger the context menu
            if (!_UserSelectionThresholdReached && _TimeUserSelectionHeld >= _SecondsBeforeClickInvoked)
            {
                _UserSelectionThresholdReached = true;
                _TimeUserSelectionHeld = 0f;
                _UserSelectedSomething = false;

                TriggerUserCommand();
            }
        }

        private void TriggerUserCommand()
        {
            UserCommand userCommand = new UserCommand
            {
                PressedPosition = _LastPressedPosition,
                Node = _TargetNode,
                Tower = _TargetTower,
                UserSelectionTriggered = _UserSelectionThresholdReached
            };
            _CommandDetectedEvent?.Raise(userCommand);
        }
    }
}
