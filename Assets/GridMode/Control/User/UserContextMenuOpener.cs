﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using Assets.SharedValues.Boolean;
using System.Collections;
using Assets.GameServices.UserInterface.Session.Context;
using Assets.GameServices.UserInterface.Session.Context.Buttons;
using Assets.GameServices.UserInterface.Session.Context.Options;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Control.User
{
    /// <summary>
    /// Contains the controls the user (the human playing the game) can use to interact with the game.
    ///
    /// Press a Node
    /// </summary>
    public class UserContextMenuOpener : MonoBehaviour
    {
        [Header("Build Mode")]
        [SerializeField] private BoolValueAssetRef _BuildModeAssetRef;
        private BoolValue _BuildModeValue;

        [Header("Showing")]
        [SerializeField] private bool _MenuShowing;
        [SerializeField] private float _TimeMenuAvailable = 2f;
        [SerializeField] private float _MenuShowingTime = 0;

        [Space(10)]
        [Tooltip("ContextMenu for a Tower, during WaveMode")]
        [SerializeField] private ContextMenu _WaveTowerMenu;

        //-------------------

        private GridNode _TargetNode;
        private TowerUnit _TargetTower;

        private Vector3 _LastPressedPosition;

        private ContextButton[] _ContextButtons;

        private IEnumerator Start()
        {
            _ContextButtons = GetComponentsInChildren<ContextButton>();
            foreach (ContextButton contextButton in _ContextButtons)
            {
                contextButton.OnClicked += ContextButtonOnOnClicked;
            }

            AsyncOperationHandle<BoolValue> loadBuildMode = _BuildModeAssetRef.LoadAssetAsync();
            yield return loadBuildMode;
            _BuildModeValue = loadBuildMode.Result;
        }

        #region User Interactions

        public void UserCommand(UserCommand command)
        {
            if (!command.UserSelectionTriggered)
            {
                HideContextMenu();
                return;
            }

            _TargetNode = command.Node;
            _TargetTower = command.Tower;
            _LastPressedPosition = command.PressedPosition;

            _MenuShowingTime = 0f;
            _MenuShowing = true;
            StartCoroutine(ShowContextMenu());
        }

        #endregion

        private void Update()
        {
            if (_MenuShowing)
            {
                _MenuShowingTime += Time.deltaTime;
                if (_MenuShowingTime >= _TimeMenuAvailable)
                {
                    HideContextMenu();
                }
            }
        }

        /*private void OnDrawGizmos()
        {
            if (Camera.main == null) return;
            Vector3 pos = Camera.main.WorldToScreenPoint(_LastPressedPosition);
            Gizmos.DrawWireSphere(pos, 20f);
        }*/

        private IEnumerator ShowContextMenu()
        {
            ContextData contextData = new ContextData()
            {
                Node = _TargetNode,
                BuildMode = _BuildModeValue.GetValue()
            };

            _MenuShowing = false;

            foreach (ContextButton contextButton in _ContextButtons)
            {
                if (contextButton.Option.IsAvailable(contextData))
                {
                    _MenuShowing = true;
                    contextButton.gameObject.SetActive(true);
                    yield return contextButton.ShowButton();
                }
                else
                {
                    contextButton.gameObject.SetActive(false);
                }
            }
        }

        private void ContextButtonOnOnClicked(ContextButton contextButton)
        {
            StartCoroutine(TriggerContextMenuOption(contextButton.Option, _TargetNode));
            HideContextMenu();
        }

        private IEnumerator TriggerContextMenuOption(ContextOption contextOption, GridNode node)
        {
            yield return contextOption.Trigger(node);
        }

        private void HideContextMenu()
        {
            _TargetNode = null;
            _TargetTower = null;

            _MenuShowingTime = 0;
            _MenuShowing = false;

            foreach (ContextButton contextButton in _ContextButtons)
            {
                contextButton.HideButton();
            }
        }

    }
}
