﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using System;
using UnityEngine;

namespace Assets.GridMode.Control.User
{
    [Serializable]
    public class UserCommand
    {
        public Vector3 PressedPosition;

        public GridNode Node;
        public TowerUnit Tower;

        public bool UserSelectionTriggered;
    }
}
