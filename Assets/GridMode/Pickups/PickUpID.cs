﻿using UnityEngine;

namespace Assets.GridMode.Pickups
{
    [CreateAssetMenu(menuName = "PickUps/ID")]
    public class PickUpID : ScriptableObject
    {

    }
}
