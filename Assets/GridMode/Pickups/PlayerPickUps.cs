﻿using Assets.GridMode.Map;
using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Pickups.PowerUp;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Boolean;
using Assets.Tools;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Assets.GridMode.Pickups
{
    public class PlayerPickUps : MonoBehaviour
    {
        [Header("Game Pause")]
        [SerializeField] private BoolValueAssetRef _GamePausedAssetRef;
        private BoolValue _GamePaused;

        [Header("Pick Up")]
        [SerializeField] private AssetLabelReference _PickUpLabelRef;
        private List<GameObject> _PickUpPrefabs;

        [Header("Spawn")]
        [SerializeField] private InfluenceRule _EnemyInfluence;
        [SerializeField] private float _SpawnRate = 20f;
        [Tooltip("If 0, then picks a random node closest to the player, if 1 then picks random node closest to enemies.")]
        [SerializeField] private float _PercentTowardsEnemies = 0.1f;
        [Tooltip("How long each second will cause the spawn to be closer to the enemies")]
        [SerializeField] private float _EnemyPercentSpawnCoefficient = 0.002f;

        private Dictionary<GridNodePosition, NodePickUp> _PickUpInstances = new Dictionary<GridNodePosition, NodePickUp>();

        private bool _WaveDefeated;

        private GridMap _Map;
        private InfluenceMap _PlayerInfluenceMap;
        private InfluenceMap _EnemyInfluenceMap;

        private EnemyWave _CurrentWave;
        private PlayerUnit _Player;

        private void Start()
        {
            StartCoroutine(LoadResources());
        }

        private IEnumerator LoadResources()
        {
            AsyncOperationHandle<IList<IResourceLocation>> findPickups = Addressables.LoadResourceLocationsAsync(_PickUpLabelRef);
            yield return findPickups;
            AsyncOperationHandle<IList<GameObject>> loadPicksup = Addressables.LoadAssetsAsync<GameObject>(findPickups.Result, s => { });
            yield return loadPicksup;
            _PickUpPrefabs = loadPicksup.Result.ToList();

            AsyncOperationHandle<BoolValue> loadPause = _GamePausedAssetRef.LoadAssetAsync();
            yield return loadPause;
            _GamePaused = loadPause.Result;
        }

        private IEnumerator SpawnPowersUpDuringWave()
        {
            while (_PickUpPrefabs == null)
            {
                yield return null;
            }

            foreach (NodePickUp powerUpPickup in _PickUpInstances.Values)
            {
                Destroy(powerUpPickup.gameObject);
            }
            _PickUpInstances.Clear();

            float elapsedTime = 0f;
            float spawnTime = 0f;

            while (!_WaveDefeated)
            {
                elapsedTime += Time.deltaTime;
                spawnTime += Time.deltaTime;

                if (spawnTime >= _SpawnRate)
                {
                    spawnTime = 0;
                    yield return SpawnPowerUp(elapsedTime);
                }

                yield return null;
            }

            yield return CleanUpPreviousWavePickUps();
        }

        private IEnumerator SpawnPowerUp(float elapsedTime)
        {
            //Get all the nodes, and find their EnemyInfluence Value, and PlayerInfluence Value.
            GridNode[] availableNodes = _Map.Nodes.Cast<GridNode>().Where(s => s.Tower == null && !s.IsSpawnNode).ToArray();

            //Find the min/max value of the enemy influence
            InfluenceNode[] enemyInfluenceNodes = availableNodes
                //Dont pick a node that already has a power up on it
                .Where(s => !_PickUpInstances.ContainsKey(new GridNodePosition(s.Column, s.Row)))
                .Select(s => _EnemyInfluenceMap.GetNode(s.Column, s.Row))
                //Order by largest first, which means furthest from enemies at start
                .OrderByDescending(s => s.InfluenceScore).ToArray();

            int minEnemyInfluence = enemyInfluenceNodes.Min(s => s.InfluenceScore);
            int maxEnemyInfluence = enemyInfluenceNodes.Max(s => s.InfluenceScore);

            //Nodes of same influence grouped together
            IGrouping<int, InfluenceNode>[] groupedEnemyInfluenceNodes = enemyInfluenceNodes
                .GroupBy(s => s.InfluenceScore)
                .OrderByDescending(s => s.Key)
                .ToArray();

            //0 -> 1 value, 1 being highest influence node of enemies, 0 being lowest
            float targetRatioToEnemy = _PercentTowardsEnemies + (elapsedTime * _EnemyPercentSpawnCoefficient);
            if (targetRatioToEnemy < 0) targetRatioToEnemy = 0;
            else if (targetRatioToEnemy > 1) targetRatioToEnemy = 1f;

            //Flip the range of 0 -> 1 to match the inverted nature of the influence, where higher influence is actually closer to the target
            targetRatioToEnemy = 1 - targetRatioToEnemy;

            //Normalise the 0 -> 1 ratio range into the min/max range of the influence.
            float targetEnemyInfluence = targetRatioToEnemy.Normalise(0, 1, minEnemyInfluence, maxEnemyInfluence);

            GridNode targetNode;

            if (targetEnemyInfluence >= maxEnemyInfluence)
            {
                targetNode = enemyInfluenceNodes.Last().GridNode;
            }
            else
            {
                //Loop through the ordered grouped influence nodes, first one that meets goes below the threshold, is the target. OR, the last one (the closest to enemies)
                InfluenceNode[] targetGroup = groupedEnemyInfluenceNodes.FirstOrDefault(s => s.Key <= targetEnemyInfluence)?.ToArray();

                //Get a random node from the group of influence nodes which are all at the desired distance to the enemies. OR, just get the last one (on top of the enemies)
                targetNode = targetGroup == null ?
                    enemyInfluenceNodes.Last().GridNode :
                    targetGroup[Random.Range(0, targetGroup.Length)].GridNode;
            }

            //Wait here while the game is paused
            yield return new WaitUntil(() => !_GamePaused.GetValue());

            GridNodePosition gridNodePosition = new GridNodePosition(targetNode.Column, targetNode.Row);
            if (!_PickUpInstances.TryGetValue(gridNodePosition, out NodePickUp pickUp))
            {
                GameObject prefab = _PickUpPrefabs.First();

                Vector3 pos = targetNode.transform.position;
                GameObject instance = Instantiate(prefab, pos, Quaternion.identity);
                PowerUpPickup powerUpPickup = instance.GetComponent<PowerUpPickup>();
                powerUpPickup.GridPosition = gridNodePosition;
                _PickUpInstances.Add(gridNodePosition, powerUpPickup);
            }
        }

        private IEnumerator PickupPowerup(PlayerUnit player, NodePickUp powerup)
        {
            yield return powerup.PickUp(player);
        }

        private IEnumerator CleanUpPreviousWavePickUps()
        {
            foreach (NodePickUp nodePickUp in _PickUpInstances.Values)
            {
                yield return nodePickUp.Despawn();
            }
            _PickUpInstances.Clear();
        }

        #region Callbacks

        public void OnMapLoaded(GridMap map)
        {
            _Map = map;
            _EnemyInfluenceMap = _Map.GetInfluenceMap(_EnemyInfluence);
        }

        public void OnWaveStarted(EnemyWave wave)
        {
            _CurrentWave = wave;
            _WaveDefeated = false;
            StartCoroutine(SpawnPowersUpDuringWave());
        }

        public void OnPlayerMoved(PlayerUnit player)
        {
            if (_WaveDefeated) return;

            _Player = player;
            GridNodePosition gridNodePos = new GridNodePosition(player.Node.Column, player.Node.Row);
            if (_PickUpInstances.TryGetValue(gridNodePos, out NodePickUp pickup))
            {
                _PickUpInstances.Remove(gridNodePos);
                StartCoroutine(PickupPowerup(player, pickup));
            }
        }

        public void OnWaveDefeated(EnemyWave wave)
        {
            _WaveDefeated = true;
        }

        #endregion
    }
}
