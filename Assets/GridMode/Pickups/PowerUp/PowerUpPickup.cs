﻿using System.Collections;
using Assets.Events.PickUps;
using Assets.GridMode.Units;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Pickups.PowerUp
{
    public class PowerUpPickup : NodePickUp
    {
        private bool _Dead;

        #region Overrides of NodePickUp

        public override IEnumerator PickUp(GridUnit unit)
        {
            if (_Dead) yield break;
            Node = unit.Node;

            AsyncOperationHandle<PickUpEvent> loadPickup = PickedUpEventAssetRef.LoadAssetAsync();
            yield return loadPickup;
            PickedUpEvent = loadPickup.Result;

            PickedUpEvent?.Raise(this);

            _Dead = true;
            Destroy(gameObject);
        }

        public override IEnumerator Despawn()
        {
            if (_Dead) yield break;
            _Dead = true;
            Destroy(gameObject);
        }

        #endregion
    }
}
