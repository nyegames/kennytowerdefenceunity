﻿using System.Collections;
using Assets.Events.PickUps;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units;
using UnityEngine;

namespace Assets.GridMode.Pickups
{
    public abstract class NodePickUp : MonoBehaviour
    {
        [Header("ID")]
        [SerializeField] private PickUpID _PickupID;
        public PickUpID ID => _PickupID;

        [Header("Events")]
        [SerializeField] protected PickUpEventAssetRef PickedUpEventAssetRef;
        protected PickUpEvent PickedUpEvent;

        public GridNodePosition GridPosition { get; set; }

        public GridNode Node { get; protected set; }

        public abstract IEnumerator PickUp(GridUnit unit);

        public abstract IEnumerator Despawn();
    }
}
