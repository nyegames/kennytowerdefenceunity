﻿using Assets.Events.Tower;
using Assets.Events.TowerWeapon;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Weapons;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units.Tower
{
    /// <summary>Detects and allocates itself onto a GridNode</summary>
    public class TowerUnit : GridUnit,
        IPointerDownHandler,
        IPointerUpHandler,

        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler
    {
        [Header("Weapon")]
        [SerializeField] private Transform _WeaponSlot;
        private GameObject _WeaponObj;
        [SerializeField] private TowerWeapon _Weapon;

        [Header("Building")]
        public bool IsDraggable = true;

        [SerializeField] private int _BuildCost;
        public int BuildCost => _BuildCost;

        [Header("Settings")]
        [SerializeField] private bool _IsDisabled;
        public bool IsDisabled => _IsDisabled;
        [SerializeField] private float _TowerDisableDuration = 5f;

        [Header("Visuals")]
        [SerializeField] private MeshRenderer _TowerMesh;

        [Header("Events")]
        [SerializeField] private TowerUnitEventAssetRef _TowerPressedEventAssetRef;
        private TowerUnitEvent _TowerPressedEvent;

        [SerializeField] private TowerUnitEventAssetRef _TowerReleasedEventAssetRef;
        private TowerUnitEvent _TowerReleasedEvent;

        [SerializeField] private TowerUnitEventAssetRef _TowerDisabledEventAssetRef;
        private TowerUnitEvent _TowerDisabledEvent;

        [SerializeField] private TowerWeaponEventAssetRef _TowerWeaponEquipAssetRef;
        private TowerWeaponEvent _TowerWeaponEquip;

        //Dragging
        private Vector3 _DragOffset;

        public TowerWeapon Weapon => _Weapon;
        public Transform WeaponSlot => _WeaponSlot;

        public event Action<TowerUnit> OnGridNodeSet;

        #region Overrides of GridUnit

        protected override void OnStart()
        {
            base.OnStart();
            StartCoroutine(LoadResources());
        }

        #endregion

        private IEnumerator LoadResources()
        {
            AsyncOperationHandle<TowerUnitEvent> loadtowerPress = _TowerPressedEventAssetRef.LoadAssetAsync();
            yield return loadtowerPress;
            _TowerPressedEvent = loadtowerPress.Result;

            AsyncOperationHandle<TowerUnitEvent> loadtowerReleased = _TowerReleasedEventAssetRef.LoadAssetAsync();
            yield return loadtowerReleased;
            _TowerReleasedEvent = loadtowerReleased.Result;

            AsyncOperationHandle<TowerUnitEvent> loadTowerDis = _TowerDisabledEventAssetRef.LoadAssetAsync();
            yield return loadTowerDis;
            _TowerDisabledEvent = loadTowerDis.Result;

            AsyncOperationHandle<TowerWeaponEvent> loadWeaponEquip = _TowerWeaponEquipAssetRef.LoadAssetAsync();
            yield return loadWeaponEquip;
            _TowerWeaponEquip = loadWeaponEquip.Result;
        }

        public void AttachToNode(GridNode node)
        {
            _Node = node;
            OnGridNodeSet?.Invoke(this);
        }

        #region Overrides of GridUnit

        protected override void OnGamePaused(bool paused)
        {
            base.OnGamePaused(paused);
            if (_Weapon != null)
            {
                _Weapon.GamePaused(paused);
            }
        }

        #endregion

        /// <summary>If a tower is </summary>
        /// <param name="disabled"></param>
        public void SetDisabled(bool disabled)
        {
            _IsDisabled = disabled;

            Collider col = GetComponent<Collider>();
            if (col != null)
            {
                col.enabled = !disabled;
            }

            _TowerMesh.enabled = !disabled;

            if (_Weapon != null)
            {
                _Weapon.SetDisabled(disabled);
            }

            _TowerDisabledEvent?.Raise(this);

            if (!disabled) return;
            StopCoroutine("EnableTowerAfter");
            StartCoroutine("EnableTowerAfter", _TowerDisableDuration);
        }

        private IEnumerator EnableTowerAfter(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            SetDisabled(false);
        }

        public void SetWeapon(GameObject weaponObj)
        {
            _WeaponObj = weaponObj;
            _Weapon = _WeaponObj.GetComponent<TowerWeapon>();
            _Weapon.SetTower(this);

            _TowerWeaponEquip?.Raise(_Weapon.WeaponType, this);
        }

        public void RotateStartingDirection()
        {
            if (Weapon == null) return;
            int valueCount = Enum.GetValues(typeof(TowerDirection)).Length;
            int nextDirection = (int)Weapon.FacingDirection + 1;
            int cappedDirection = nextDirection % valueCount;
            TowerDirection d = (TowerDirection)cappedDirection;
            Weapon.SetFacingDirection(d);
        }

        #region Implementation of IPointerDownHandler

        public void OnPointerDown(PointerEventData eventData)
        {
            _TowerPressedEvent?.Raise(this);
        }

        #endregion

        #region Implementation of IPointerUpHandler

        public void OnPointerUp(PointerEventData eventData)
        {
            _TowerReleasedEvent?.Raise(this);
        }

        #endregion

        #region Dragging Tower

        private bool CanDrag
        {
            get
            {
                bool buildMode = Node?.Map?.BuildMode ?? true;
                return buildMode && IsDraggable;
            }
        }

        #region Implementation of IBeginDragHandler

        public void OnBeginDrag(PointerEventData eventData)
        {
            return;
            if (!CanDrag) return;

            _DragOffset = transform.position - eventData.pointerCurrentRaycast.worldPosition;
            _DragOffset = new Vector3(_DragOffset.x, transform.position.y, _DragOffset.z);
        }

        #endregion

        #region Implementation of IDragHandler

        public void OnDrag(PointerEventData eventData)
        {
            return;
            if (!CanDrag) return;

            Vector3 pos = eventData.pointerCurrentRaycast.worldPosition;
            transform.position = _DragOffset + new Vector3(pos.x, transform.position.y, pos.z);
        }

        #endregion

        #region Implementation of IEndDragHandler

        public void OnEndDrag(PointerEventData eventData)
        {
            return;
            if (!CanDrag) return;

            Vector3 origin = eventData.pointerCurrentRaycast.worldPosition + new Vector3(0, -10, 0);
            Vector3 direction = new Vector3(0, 1, 0);

            RaycastHit[] result = Physics.RaycastAll(origin, direction, 50, ~0);
            GridNode dropNode = result.Select(s => s.transform.gameObject.GetComponent<GridNode>())
                .FirstOrDefault(s => s != null);

            bool dropOnTargetSuccess = dropNode != null && dropNode.PlaceTower(this);

            //If no valid drop target
            if (!dropOnTargetSuccess)
            {
                //Snap back to current node
                Node.PlaceTower(this);
            }
        }

        #endregion

        #endregion
    }
}
