﻿namespace Assets.GridMode.Units.Tower
{
    public enum TowerDirection
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}