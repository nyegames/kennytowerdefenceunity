﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Factions
{
    [CreateAssetMenu(menuName = "FactionID")]
    public class FactionID : ScriptableObject
    {
        [SerializeField] private List<FactionID> _FriendlyTowards;
        [SerializeField] private List<FactionID> _AggressiveTowards;

        public bool IsAggressiveTowards(FactionID faction)
        {
            return _AggressiveTowards.Any(s => s.name == faction.name);
        }

        public bool IsFriendlyTowards(FactionID faction)
        {
            return _FriendlyTowards.Any(s => s.name == faction.name);
        }
    }
}
