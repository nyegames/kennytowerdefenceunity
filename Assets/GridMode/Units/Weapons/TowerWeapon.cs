﻿using Assets.GridMode.Map;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Weapons
{
    public abstract class TowerWeapon : MonoBehaviour
    {
        [Header("Tower")]
        [SerializeField] protected TowerUnit Tower;
        [SerializeField] private TowerWeaponType _WeaponType;
        public TowerWeaponType WeaponType => _WeaponType;

        [Header("Build")]
        [SerializeField] private int _BuildCost;
        public int BuildCost => _BuildCost;

        [Header("Position")]
        [SerializeField] protected TowerDirection CurrentDirection;
        public TowerDirection FacingDirection => CurrentDirection;

        [Header("Power")]
        [SerializeField] protected bool PowerActive;
        public bool IsPowerActive => PowerActive;

        [Header("Active")]
        [SerializeField] protected bool WeaponDisabled = false;
        public bool IsDisabled => WeaponDisabled;


        private MeshRenderer[] _Visuals;

        public abstract List<GridNode> GetTargetNodes();

        protected virtual void Start()
        {
            if (_Visuals == null || !_Visuals.Any())
            {
                _Visuals = GetComponentsInChildren<MeshRenderer>();
            }
            OnStart();
        }

        protected virtual void OnStart()
        {
            SetFacingDirection(CurrentDirection);
        }

        public virtual void SetTower(TowerUnit tower)
        {
            Tower = tower;
            Tower.OnGridNodeSet += TowerOnOnGridNodeSet;
            Tower.Node.Map.OnNodesUpdated += MapOnOnNodesUpdated;
        }

        protected virtual void TowerOnOnGridNodeSet(TowerUnit obj)
        {

        }

        protected virtual void MapOnOnNodesUpdated(GridMap obj)
        {

        }

        public virtual void SetFacingDirection(TowerDirection direction)
        {
            switch (direction)
            {
                case TowerDirection.NORTH:
                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                    break;
                case TowerDirection.EAST:
                    transform.localRotation = Quaternion.Euler(0, 90, 0);
                    break;
                case TowerDirection.SOUTH:
                    transform.localRotation = Quaternion.Euler(0, 180, 0);
                    break;
                case TowerDirection.WEST:
                    transform.localRotation = Quaternion.Euler(0, -90, 0);
                    break;
            }

            CurrentDirection = direction;
        }

        public virtual void GamePaused(bool paused)
        {

        }

        public virtual void SetPowerActive(bool powerActive)
        {
            PowerActive = powerActive;
        }

        public virtual void SetDisabled(bool disabled)
        {
            WeaponDisabled = disabled;
            //Stop being able to press this while disabled
            Collider col = GetComponent<Collider>();
            if (col != null)
            {
                col.enabled = !disabled;
            }

            foreach (MeshRenderer meshRenderer in _Visuals)
            {
                meshRenderer.enabled = !disabled;
            }
        }

        protected virtual void OnUpdate(float deltaTime)
        {

        }

        private void Update()
        {
            OnUpdate(Time.deltaTime);
        }
    }
}
