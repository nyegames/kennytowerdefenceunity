﻿using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Enemies;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Weapons.Blaster
{
    /// <summary>
    /// Blaster weapon is an overheat weapon, it will instantly snap to the first target in its area.
    /// After shooting for X seconds it will overheat and be unable to shoot for Y seconds.
    ///
    /// Used for lots of little units.
    ///
    /// POWER ACTIVATE:
    /// When its power is active, it no longer can overheat for Z seconds 
    /// </summary>
    public class BlasterTowerWeapon : TowerWeapon
    {
        [Header("Tracking")]
        [Range(1, 5)]
        [SerializeField] private int _TargetRadius = 1;
        //TODO - Use this to work out the enemy closest to the player, and therefore biggest threat.
        [SerializeField] private InfluenceRule _PlayerInfluence;

        [Header("Attack")]
        [Range(0.1f, 20)]
        [Tooltip("How many times a target can get hit per second")]
        [SerializeField] private float _ShotsPerSecond = 10;
        [Range(0, 10)]
        [SerializeField] private int _DamagePerShot = 1;
        [SerializeField] private float _TimeShooting = 0f;
        [SerializeField] private float _TimeSinceLastShot = 0;

        [Header("Overheat")]
        [Tooltip("Time it takes to overheat")]
        [Range(0.1f, 10f)]
        [SerializeField] private float _TimeBeforeOverheated = 2f;
        [SerializeField] private bool _Overheated = false;
        [Range(0, 10f)]
        [SerializeField] private float _OverheatRecoveryDuration = 1f;

        [Header("Power")]
        [SerializeField] private float _PowerDuration = 10f;
        [SerializeField] private float _PowerRemaining = 0;

        [Header("Effects")]
        [SerializeField] private ParticleSystem _ChargingParticles;
        [SerializeField] private ParticleSystem _LaserOnHitParticles;

        [Header("Editor")]
        [SerializeField] private Color _TargetNodeColour = Color.cyan;

        #region Overrides of TowerWeaponBase

        public override List<GridNode> GetTargetNodes()
        {
            if (Tower == null || Tower.Node == null) return new List<GridNode>();
            return Tower.Node.GetRadiusOfNodes(_TargetRadius, true);
        }

        public override void SetPowerActive(bool powerActive)
        {
            base.SetPowerActive(powerActive);
            if (PowerActive)
            {
                _PowerRemaining = _PowerDuration;
                _Overheated = false;
            }
            else
            {
                _PowerRemaining = 0;
            }
        }

        #endregion

        private EnemyUnit GetTarget()
        {
            EnemyUnit[] enemies = GetTargetNodes().SelectMany(s => s.Units).OfType<EnemyUnit>().ToArray();
            bool targetsInRange = enemies.Any(s => s != null);
            if (targetsInRange)
            {
                return enemies.FirstOrDefault();
            }
            return null;
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            if (WeaponDisabled) return;

            if (Tower.Paused) return;

            EnemyUnit target = GetTarget();
            if (target == null) return;

            if (!_Overheated)
            {
                transform.LookAt(target.transform);
            }

            //Dont do any shooting if you have overheated
            if (PowerActive || !_Overheated)
            {
                if (PowerActive)
                {
                    _PowerRemaining -= deltaTime;
                    if (_PowerRemaining <= 0)
                    {
                        SetPowerActive(false);
                    }
                }

                _TimeShooting += deltaTime;
                //Cannot overheat if you are Powered up
                if (!PowerActive && _TimeShooting >= _TimeBeforeOverheated)
                {
                    //You have overheated, stop shooting
                    _Overheated = true;
                    StartCoroutine(Overheated(_OverheatRecoveryDuration));
                }
                else
                {
                    _TimeSinceLastShot += deltaTime;
                    if (_TimeSinceLastShot > _ShotsPerSecond)
                    {
                        _TimeSinceLastShot = 0;
                        target.TakeDamage(Tower, _DamagePerShot);
                        if (_LaserOnHitParticles != null)
                        {
                            _LaserOnHitParticles.transform.position = target.transform.position;
                            _LaserOnHitParticles.gameObject.SetActive(true);
                            _LaserOnHitParticles.Play();
                        }
                    }
                }
            }
            else
            {
                //Start to cool off your time shooting,
                //so if you improve the overheat recovery time, you will go back into overheat faster
                _TimeShooting -= deltaTime;
                if (_TimeShooting < 0) _TimeShooting = 0;
            }
        }

        private IEnumerator Overheated(float overheatDuration)
        {
            if (_ChargingParticles != null)
            {
                _ChargingParticles.gameObject.SetActive(true);
                _ChargingParticles?.Play();
            }

            yield return new WaitForSeconds(overheatDuration);

            if (_ChargingParticles != null)
            {
                //_ChargingParticles.gameObject.SetActive(false);
                _ChargingParticles?.Stop();
            }

            _Overheated = false;
        }

        private void OnDestroy()
        {
            if (Tower != null)
            {
                Tower.OnGridNodeSet -= TowerOnOnGridNodeSet;

                if (Tower.Node.Map != null)
                {
                    Tower.Node.Map.OnNodesUpdated -= MapOnOnNodesUpdated;
                }
            }
        }

        #region Editor

        private void OnValidate()
        {
            SetFacingDirection(CurrentDirection);
            SetPowerActive(PowerActive);
        }

        private void OnDrawGizmos()
        {
            if (GetTargetNodes() == null) return;
            EnemyUnit target = GetTarget();

            for (int i = 0; i < GetTargetNodes().Count; i++)
            {
                GridNode targetNode = GetTargetNodes()[i];
                Gizmos.color = target != null && target.Node == targetNode ? Color.red : _TargetNodeColour;
                float size = 0.95f;
                Gizmos.DrawWireCube(targetNode.transform.position, new Vector3(size, size * 0.85f, size));
            }
        }

        #endregion
    }
}
