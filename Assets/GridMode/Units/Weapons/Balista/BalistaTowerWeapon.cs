﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Enemies;
using Assets.GridMode.Units.Tower;
using UnityEngine;

namespace Assets.GridMode.Units.Weapons.Balista
{
    /// <summary>
    /// Balista will shoot the first enemy in a linear path along X nodes, in the StartingDirection it is facing
    ///
    /// POWER:
    /// Balista will shoot Y shots that pierce ALL enemies along the linear path in the StartingDirection they are facing, until that path meets another Tower.
    /// 
    /// </summary>
    public class BalistaTowerWeapon : TowerWeapon
    {
        [Header("Tracking")]
        [Tooltip("How many nodes can be damaged in the direction of the weapon")]
        [Range(0, 20)]
        [SerializeField] private int _DamageNodeRange = 6;

        [Header("Attack")]
        [SerializeField] private int _DamagePerShot = 10;
        [Tooltip("How long it takes the balista to charge a shot")]
        [SerializeField] private float _ChargeTime = 0.05f;
        [SerializeField] private float _ChargeTimeLeft;

        [Space(5)]
        [Tooltip("How long it takes the arrow to move across 1 node once fired")]
        [SerializeField] private float _ArrowNodeSpeed = 0.2f;
        [Tooltip("True when the arrow is moving across the target nodes, cannot fire again until this is false")]
        [SerializeField] private bool _ArrowFiring;

        [Header("Bolt")]
        [SerializeField] private Transform _BoltSlot;

        [Header("Power")]
        [SerializeField] private int _PowerfulShotsAllowed = 3;
        [SerializeField] private int _PowerShotsRemaining = 0;

        [Header("Editor")]
        [SerializeField] private Color _TargetNodeColour = Color.red;

        #region Overrides of TowerWeaponBase

        public override List<GridNode> GetTargetNodes()
        {
            if (Tower == null || Tower.Node == null) return new List<GridNode>();
            GridNode node = Tower.Node;
            List<GridNode> targetNodes = new List<GridNode>();

            do
            {
                switch (CurrentDirection)
                {
                    case TowerDirection.NORTH:
                        node = node.Map.GetNode(node.Column, node.Row + 1);
                        break;
                    case TowerDirection.EAST:
                        node = node.Map.GetNode(node.Column + 1, node.Row);
                        break;
                    case TowerDirection.SOUTH:
                        node = node.Map.GetNode(node.Column, node.Row - 1);
                        break;
                    case TowerDirection.WEST:
                        node = node.Map.GetNode(node.Column - 1, node.Row);
                        break;
                }

                if (node != null)
                {
                    if (node.Tower != null)
                    {
                        break;
                    }

                    if (node.IsSpawnNode)
                    {
                        break;
                    }

                    targetNodes.Add(node);
                }

                if (!PowerActive && targetNodes.Count >= _DamageNodeRange)
                {
                    break;
                }
            } while (node != null);

            return targetNodes;
        }

        protected override void OnStart()
        {
            base.OnStart();
            _ChargeTimeLeft = _ChargeTime;
        }

        public override void SetTower(TowerUnit tower)
        {
            base.SetTower(tower);
            TowerOnOnGridNodeSet(Tower);
            MapOnOnNodesUpdated(Tower.Node.Map);
        }

        public override void SetPowerActive(bool powerActive)
        {
            base.SetPowerActive(powerActive);
            _PowerShotsRemaining = _PowerfulShotsAllowed;
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            if (WeaponDisabled) return;

            List<GridNode> targetNodes = GetTargetNodes();

            if (targetNodes == null || _ArrowFiring)
            {
                return;
            }

            //Do you have any enemies in your target areas?
            EnemyUnit[] enemies = targetNodes.SelectMany(s => s.Units).OfType<EnemyUnit>().ToArray();
            bool targetsInRange = enemies.Any(s => s != null);

            if (targetsInRange)
            {
                _ChargeTimeLeft += deltaTime;
                if (_ChargeTimeLeft >= _ChargeTime)
                {
                    _ChargeTimeLeft = 0;
                    _ArrowFiring = true;

                    StartCoroutine(FireArrow());
                }
            }
            else
            {
                //Cool down the charge time back to 0 when targets are no longer in range
                _ChargeTimeLeft -= deltaTime;
                if (_ChargeTimeLeft <= 0) _ChargeTimeLeft = 0;
            }
        }

        #endregion

        private IEnumerator FireArrow()
        {
            Transform arrowTransform = _BoltSlot.GetChild(0);
            if (arrowTransform == null)
            {
                _ArrowFiring = false;
                yield break;
            }

            GridNode[] targetNodes = GetTargetNodes().ToArray();

            foreach (GridNode targetNode in targetNodes)
            {
                float elapsedTime = 0f;
                Vector3 startPosition = arrowTransform.position;
                startPosition = new Vector3(startPosition.x, arrowTransform.position.y, startPosition.z);
                Vector3 targetPosition = targetNode.transform.position;
                targetPosition = new Vector3(targetPosition.x, arrowTransform.position.y, targetPosition.z);

                while (elapsedTime < _ArrowNodeSpeed)
                {
                    arrowTransform.position = Vector3.Lerp(startPosition, targetPosition, (elapsedTime / _ArrowNodeSpeed));
                    elapsedTime += Time.deltaTime;

                    // Yield here

                    yield return new WaitUntil(() => !Tower.Paused);
                }

                yield return new WaitUntil(() => !Tower.Paused);

                arrowTransform.position = targetPosition;
                DamageEverythingInNode(targetNode);
            }

            arrowTransform.position = _BoltSlot.transform.position;

            if (PowerActive)
            {
                _PowerShotsRemaining--;
                if (_PowerShotsRemaining <= 0)
                {
                    PowerActive = false;
                }
            }

            _ArrowFiring = false;
        }

        private void DamageEverythingInNode(GridNode targetNode)
        {
            foreach (GridUnit targetNodeUnit in targetNode.Units)
            {
                if (Tower.Faction.IsAggressiveTowards(targetNodeUnit.Faction))
                {
                    targetNodeUnit.TakeDamage(Tower, _DamagePerShot);
                    //TODO - Do an effect for damage on the target
                }
            }
        }

        private void OnDestroy()
        {
            if (Tower != null)
            {
                Tower.OnGridNodeSet -= TowerOnOnGridNodeSet;

                if (Tower.Node.Map != null)
                {
                    Tower.Node.Map.OnNodesUpdated -= MapOnOnNodesUpdated;
                }
            }
        }

        #region Editor

        private void OnValidate()
        {
            SetFacingDirection(CurrentDirection);
        }

        private void OnDrawGizmos()
        {
            if (GetTargetNodes() == null) return;

            foreach (GridNode targetNode in GetTargetNodes())
            {
                Gizmos.color = _TargetNodeColour;
                float size = 0.95f;
                Gizmos.DrawWireCube(targetNode.transform.position, new Vector3(size, size * 0.85f, size));
            }

            if (GetTargetNodes().Count > 2)
            {
                Vector3 distanceBetweenOneNode = GetTargetNodes()[1].transform.position - GetTargetNodes()[0].transform.position;

                Vector3 firstPos = GetTargetNodes()[0].transform.position - (distanceBetweenOneNode * 0.5f);
                Vector3 lastPos = GetTargetNodes().Last().transform.position + (distanceBetweenOneNode * 0.5f);

                firstPos = new Vector3(firstPos.x, firstPos.y + 0.1f, firstPos.z);
                lastPos = new Vector3(lastPos.x, lastPos.y + 0.1f, lastPos.z);

                Gizmos.DrawLine(firstPos, lastPos);
            }
        }

        #endregion
    }
}
