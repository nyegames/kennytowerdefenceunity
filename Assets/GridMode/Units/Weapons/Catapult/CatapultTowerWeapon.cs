﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Enemies;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Weapons.Catapult
{
    public class CatapultTowerWeapon : TowerWeapon
    {
        [Header("Tracking")]
        [SerializeField] private Transform _Root;
        [Tooltip("Distance away from tower before targets get counted")]
        [Range(0, 20)]
        [SerializeField] private int _MinNodeDistance;
        [Tooltip("Distance away from the MinNodeDistance as the area that is counted as targets")]
        [Range(1, 21)]
        [SerializeField] private int _MaxNodeDistance;
        [Range(1, 5)]
        [Tooltip("How narrow the target cone is for this cannon, larger number smaller cone")]
        [SerializeField] private int _TargetConeWidth = 4;

        [Header("Attack")]
        [SerializeField] private int _DamagePerShot = 10;
        [Tooltip("How long it takes the Catapult to charge a shot")]
        [SerializeField] private float _ChargeTime = 0.05f;
        [Tooltip("How long it takes the Projectile to move across a node")]
        [Range(0.01f, 1)]
        [SerializeField] private float _ProjectileNodeSpeed = 0.15f;
        [Range(1, 5)]
        [Tooltip("AoE around the target node that gets damaged")]
        [SerializeField] private int _AreaOfEffectNodes = 1;

        private int GetAreaOfEffectNodeRadius() => PowerActive ? (int)(_AreaOfEffectNodes * _PowerRadiusMultiplier) : _AreaOfEffectNodes;

        [SerializeField] private float _ChargeTimeLeft;
        [Tooltip("True when the Projectile is moving across the target nodes, cannot fire again until this is false")]
        [SerializeField] private bool _ProjectileFiring;

        [Header("Projectile")]
        [SerializeField] private Transform _ProjectileSlot;

        [Header("Animation")]
        [SerializeField] private Transform _AnimationRoot;
        [SerializeField] private float _DefaultRotation = 90f;
        [SerializeField] private float _FireRotationTarget = 47f;

        [Header("Power")]
        [Tooltip("Larger AoE of radius when powered")]
        [SerializeField] private float _PowerRadiusMultiplier = 2f;
        [Tooltip("How many powerful shots you get before losing Power")]
        [SerializeField] private int _PowefulShotAmmo = 1;
        [Tooltip("How many powerful shots are left to fire")]
        [SerializeField] private int _PowerShotsRemaining = 0;

        [Header("Editor")]
        [SerializeField] private Color _TargetNodeColour = Color.white;
        [SerializeField] private Color _DamageNodeColour = Color.green;
        private GridNode _TargetLandingNode;
        private TweenerCore<Vector3, Vector3, VectorOptions> _WeaponTween;

        #region Overrides of TowerWeaponBase

        public override List<GridNode> GetTargetNodes()
        {
            if (Tower == null || Tower.Node == null) return new List<GridNode>();
            return Tower.Node.GetConeOfNodes(CurrentDirection, _TargetConeWidth, _MinNodeDistance, _MaxNodeDistance);
        }

        public override void SetPowerActive(bool powerActive)
        {
            base.SetPowerActive(powerActive);
            _PowerShotsRemaining = powerActive ? _PowefulShotAmmo : 0;
        }

        #region Overrides of TowerWeapon

        public override void GamePaused(bool paused)
        {
            base.GamePaused(paused);
            if (_WeaponTween != null)
            {
                if (paused) _WeaponTween.Pause();
                else _WeaponTween.Play();
            }
        }

        #endregion

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            if (WeaponDisabled) return;

            List<GridNode> targetNodes = GetTargetNodes();
            if (targetNodes == null || _ProjectileFiring) return;

            EnemyUnit[] enemies = targetNodes.SelectMany(s => s.Units).OfType<EnemyUnit>().ToArray();
            bool targetsInRange = enemies.Any(s => s != null);

            if (targetsInRange)
            {
                _ChargeTimeLeft += deltaTime;
                if (_ChargeTimeLeft >= 0)
                {
                    _ChargeTimeLeft = 0;
                    _ProjectileFiring = true;

                    StartCoroutine(FireProjectile());
                }
            }
            else
            {
                //Cool down the charge time back to 0 when targets are no longer in range
                _ChargeTimeLeft -= deltaTime;
                if (_ChargeTimeLeft <= 0) _ChargeTimeLeft = 0;
            }
        }

        #endregion

        private IEnumerator FireProjectile()
        {
            Transform projectileTransform = _ProjectileSlot.GetChild(0);
            if (projectileTransform == null)
            {
                _ProjectileFiring = false;
                yield break;
            }

            if (Tower == null || Tower.Node == null) yield break;

            Vector2 myNode = new Vector2(Tower.Node.Column, Tower.Node.Row);

            //Order by furthest from this tower, weak when up close
            GridNode[] distanceNodes = GetTargetNodes().OrderByDescending(s => Vector2.Distance(new Vector2(s.Column, s.Row), myNode))
                .ToArray();

            //TODO - first with an enemy inside the target area?
            //First with an enemy on it
            _TargetLandingNode = distanceNodes.FirstOrDefault(s =>
            {
                return s.Units.Any(u => u.TryGetComponent(out EnemyUnit b) && !b.Dead);
            });


            if (_TargetLandingNode == null)
            {
                _ProjectileFiring = false;
                yield break;
            }

            //TODO - Rotate the canon slowly
            _Root.LookAt(_TargetLandingNode.transform, Vector3.up);

            float distanceFromNode = Vector2.Distance(new Vector2(Tower.Node.Column, Tower.Node.Row), new Vector2(_TargetLandingNode.Column, _TargetLandingNode.Row));
            float totalTime = _ProjectileNodeSpeed * distanceFromNode;

            //TODO - Make this curve, not change direction mid air

            _WeaponTween = projectileTransform.transform.DOMove(_TargetLandingNode.transform.position, totalTime);
            yield return _WeaponTween.WaitForCompletion();

            //Wait here while the game is paused
            yield return new WaitUntil(() => !Tower.Paused);

            projectileTransform.position = _TargetLandingNode.transform.position;

            int areaOfEffectNodeRadius = GetAreaOfEffectNodeRadius();
            List<GridNode> aoeNodes = _TargetLandingNode.GetRadiusOfNodes(areaOfEffectNodeRadius, false);
            foreach (GridNode targetNode in aoeNodes)
            {
                foreach (GridUnit targetNodeUnit in targetNode.Units)
                {
                    if (Tower.Faction.IsAggressiveTowards(targetNodeUnit.Faction))
                    {
                        targetNodeUnit.TakeDamage(Tower, _DamagePerShot);
                        //TODO - Do an effect for damage on the target
                    }
                }
            }

            if (PowerActive)
            {
                _PowerShotsRemaining--;
                if (_PowerShotsRemaining < 0)
                {
                    SetPowerActive(false);
                }
            }

            projectileTransform.position = _ProjectileSlot.transform.position;
            _ProjectileFiring = false;

            _TargetLandingNode = null;
        }

        private void OnDestroy()
        {
            if (Tower != null)
            {
                Tower.OnGridNodeSet -= TowerOnOnGridNodeSet;

                if (Tower.Node.Map != null)
                {
                    Tower.Node.Map.OnNodesUpdated -= MapOnOnNodesUpdated;
                }
            }
        }

        #region Editor

        private void OnValidate()
        {
            if (_MaxNodeDistance < _MinNodeDistance)
            {
                _MaxNodeDistance = _MinNodeDistance + 1;
            }
            SetFacingDirection(CurrentDirection);
        }

        private void OnDrawGizmos()
        {
            if (GetTargetNodes() == null) return;

            List<GridNode> damageNodes = new List<GridNode>();
            if (_TargetLandingNode != null)
            {
                damageNodes = _TargetLandingNode.GetRadiusOfNodes(GetAreaOfEffectNodeRadius(), false);
            }

            foreach (GridNode targetNode in GetTargetNodes())
            {
                if (damageNodes.Contains(targetNode)) continue;
                Gizmos.color = _TargetNodeColour;

                float size = 0.95f;
                Gizmos.DrawWireCube(targetNode.transform.position, new Vector3(size, size * 0.85f, size));
            }

            foreach (GridNode damageNode in damageNodes)
            {
                Gizmos.color = _DamageNodeColour;

                float size = 0.95f;
                Gizmos.DrawWireCube(damageNode.transform.position, new Vector3(size, size * 0.85f, size));
            }
        }

        #endregion
    }
}
