﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Enemies;
using UnityEngine;

namespace Assets.GridMode.Units.Weapons.Cannon
{
    /// <summary>
    ///  Normal: Single target damage to a single enemy
    ///
    ///  Power: 1 overpowerful x100 damage shot to a single target
    /// </summary>
    public class CannonTowerWeapon : TowerWeapon
    {
        [Header("Tracking")]
        [SerializeField] private Transform _Root;
        [Tooltip("Distance away from tower before targets get counted")]
        [Range(0, 20)]
        [SerializeField] private int _MinNodeDistance;
        [Tooltip("Distance away from the MinNodeDistance as the area that is counted as targets")]
        [Range(1, 21)]
        [SerializeField] private int _MaxNodeDistance;
        [Range(1, 5)]
        [Tooltip("How narrow the target cone is for this cannon, larger number smaller cone")]
        [SerializeField] private int _TargetConeWidth = 4;

        [Header("Attack")]
        [SerializeField] private int _DamagePerShot = 10;
        [Tooltip("How long it takes the Canon to charge a shot")]
        [SerializeField] private float _ChargeTime = 0.05f;
        [Tooltip("How long it takes the Projectile to move across a node")]
        [Range(0.01f, 1)]
        [SerializeField] private float _ProjectileNodeSpeed = 0.15f;

        [SerializeField] private float _ChargeTimeLeft;
        [Tooltip("True when the Projectile is moving across the target nodes, cannot fire again until this is false")]
        [SerializeField] private bool _ProjectileFiring;

        [Header("Canon Ball")]
        [SerializeField] private Transform _ProjectileSlot;

        [Header("Animation")]
        [SerializeField] private float _DefaultRotation = 90f;
        [SerializeField] private float _FireRotationTarget = 47f;

        [Header("Power")]
        [Tooltip("Extra damage to apply her powerful shot")]
        [SerializeField] private float _PowerDamageMultiplier = 100.0f;
        [Tooltip("How many powerful shots you get before losing Power")]
        [SerializeField] private int _PowefulShotAmmo = 1;
        [Tooltip("How many powerful shots are left to fire")]
        [SerializeField] private int _PowerShotsRemaining = 0;

        [Header("Editor")]
        [SerializeField] private Color _TargetNodeColour = Color.yellow;

        #region Overrides of TowerWeaponBase

        /// <summary>
        /// Find nodes in the direction of the starting direction.
        /// Move in the direction, and increment how many perpendicular neighbours you search for each time you move in the direction.
        /// Dont 
        ///      []
        ///   [] [] []
        /// [] []  [] []
        /// Start recording the GridNodes when you are within the <see cref="_MinNodeDistance"/> and <see cref="_MaxNodeDistance"/>
        /// </summary>
        public override List<GridNode> GetTargetNodes()
        {
            if (Tower == null || Tower.Node == null) return new List<GridNode>();
            return Tower.Node.GetConeOfNodes(CurrentDirection, _TargetConeWidth, _MinNodeDistance,
                _MaxNodeDistance);
        }

        public override void SetPowerActive(bool powerActive)
        {
            base.SetPowerActive(powerActive);
            _PowerShotsRemaining = powerActive ? _PowefulShotAmmo : 0;
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            if (WeaponDisabled) return;
            if (GetTargetNodes() == null || _ProjectileFiring) return;

            EnemyUnit[] enemies = GetTargetNodes().SelectMany(s => s.Units).OfType<EnemyUnit>().ToArray();
            bool targetsInRange = enemies.Any(s => s != null);

            if (targetsInRange)
            {
                _ChargeTimeLeft += deltaTime;
                if (_ChargeTimeLeft >= 0)
                {
                    _ChargeTimeLeft = 0;
                    _ProjectileFiring = true;

                    StartCoroutine(FireProjectile());
                }
            }
            else
            {
                //Cool down the charge time back to 0 when targets are no longer in range
                _ChargeTimeLeft -= deltaTime;
                if (_ChargeTimeLeft <= 0) _ChargeTimeLeft = 0;
            }
        }

        #endregion

        private IEnumerator FireProjectile()
        {
            Transform projectileTransform = _ProjectileSlot.GetChild(0);
            if (projectileTransform == null)
            {
                _ProjectileFiring = false;
                yield break;
            }

            Vector2 myNode = new Vector2(Tower.Node.Column, Tower.Node.Row);

            GridNode[] distanceNodes = GetTargetNodes()
                .OrderByDescending(s =>
                    Vector2.Distance(new Vector2(s.Column, s.Row), myNode))
                .ToArray();

            GridUnit targetUnit = distanceNodes.SelectMany(s => s.Units).FirstOrDefault(s => s.TryGetComponent(out EnemyUnit e) && !e.Dead);
            if (targetUnit == null)
            {
                _ProjectileFiring = false;
                yield break;
            }

            //TODO - Rotate the canon slowly
            _Root.LookAt(targetUnit.transform, Vector3.up);

            GridNode targetNode = targetUnit.Node;
            Vector3 startPosition = _Root.position;

            //TODO - Make this curve, not change direction mid air

            float elapsedTime = 0f;
            float distanceFromNode = Vector2.Distance(myNode, new Vector2(targetNode.Column, targetNode.Row));
            float totalTime = _ProjectileNodeSpeed * distanceFromNode;

            Vector3 lastKnownPosition = targetUnit.transform.position;

            while (elapsedTime < totalTime)
            {
                if (targetUnit != null && targetUnit.isActiveAndEnabled)
                {
                    lastKnownPosition = targetUnit.transform.position;
                }

                projectileTransform.transform.position = Vector3.Lerp(startPosition, lastKnownPosition,
                    elapsedTime / totalTime);
                elapsedTime += Time.deltaTime;
                
                yield return new WaitUntil(() => !Tower.Paused);
            }

            yield return new WaitUntil(() => !Tower.Paused);

            if (targetUnit != null && targetUnit.isActiveAndEnabled)
            {
                projectileTransform.transform.position = targetUnit.transform.position;
                int damagePerShot = PowerActive ? (int)(_DamagePerShot * _PowerDamageMultiplier) : _DamagePerShot;

                projectileTransform.position = targetUnit.transform.position;

                if (!targetUnit.Dead)
                {
                    targetUnit.TakeDamage(Tower, damagePerShot);
                }

                if (PowerActive)
                {
                    _PowerShotsRemaining--;
                    if (_PowerShotsRemaining < 0)
                    {
                        SetPowerActive(false);
                    }
                }
            }

            projectileTransform.position = _ProjectileSlot.transform.position;
            _ProjectileFiring = false;
        }

        private void OnDestroy()
        {
            if (Tower != null)
            {
                Tower.OnGridNodeSet -= TowerOnOnGridNodeSet;

                if (Tower.Node.Map != null)
                {
                    Tower.Node.Map.OnNodesUpdated -= MapOnOnNodesUpdated;
                }
            }
        }

        #region Editor

        private void OnValidate()
        {
            if (_MaxNodeDistance < _MinNodeDistance)
            {
                _MaxNodeDistance = _MinNodeDistance + 1;
            }
            SetFacingDirection(CurrentDirection);
        }

        private void OnDrawGizmos()
        {
            if (GetTargetNodes() == null) return;

            foreach (GridNode targetNode in GetTargetNodes())
            {
                Gizmos.color = _TargetNodeColour;
                float size = 0.95f;
                Gizmos.DrawWireCube(targetNode.transform.position, new Vector3(size, size * 0.85f, size));
            }
        }

        #endregion
    }
}
