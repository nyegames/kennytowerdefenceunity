﻿using Assets.Events.Enemies;
using Assets.Events.Wave;
using Assets.GameServices.PlayerData;
using Assets.GridMode.Map;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Map.Pathing;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Boolean;
using Assets.SharedValues.Float;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using Random = UnityEngine.Random;

namespace Assets.GridMode.Units.Enemies.Spawn
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private AssetLabelReference _EnemyLabel;
        private List<GameObject> _EnemyPrefabs;
        private bool _EnemiesLoading = false;
        private bool _EnemiesLoaded = false;
        private bool _WaveEnemiesCreated = false;

        private GridMap _Map;
        private GridNode[] _SpawnNodes;

        [Header("PathToPlayer")]
        [SerializeField] private GridPathFinder _PathFinder;

        [Header("Wave")]
        [SerializeField] private FloatValueAssetRef _WaveDurationValueAssetRef;
        private FloatValue _WaveDurationValue;
        [SerializeField] private EnemyWave _CurrentWave;
        [SerializeField] private List<GameObject> _WaveEnemies;
        private int _DeadEnemyCount = 0;

        [Header("Game Pause")]
        [SerializeField] private BoolValueAssetRef _GamePauseValueAssetRef;
        private BoolValue _GamePauseValue;

        [Header("Events")]
        [SerializeField] private EnemyEventAssetRef _EnemySpawnedEventAssetRef;
        private EnemyEvent _EnemySpawnedEvent;

        [SerializeField] private EnemyEventAssetRef _EnemyDiedEventAssetRef;
        private EnemyEvent _EnemyDiedEvent;

        [SerializeField] private EnemyWaveEventAssetRef _WaveCompletedEventAssetRef;
        private EnemyWaveEvent _WaveCompletedEvent;

        private PlayerUnit _Player;

        public void OnMapLoaded(GridMap map)
        {
            _Map = map;
            //TODO - Make into coroutine if grid is large
            GridNode[] gridNodes = _Map.Nodes.Cast<GridNode>().ToArray();
            _SpawnNodes = gridNodes.Where(s => s.IsSpawnNode).ToArray();

            //If enemies are already loading or they have already been loaded, then dont try again
            if (_EnemiesLoaded || _EnemiesLoading) return;
            StartCoroutine(LoadResources());
        }

        private IEnumerator LoadResources()
        {
            _EnemiesLoading = true;
            _EnemiesLoaded = false;

            AsyncOperationHandle<IList<IResourceLocation>> enemiesResources = Addressables.LoadResourceLocationsAsync(_EnemyLabel);
            yield return enemiesResources;
            AsyncOperationHandle<IList<GameObject>> enemyPrefabs = Addressables.LoadAssetsAsync<GameObject>(enemiesResources.Result, s => { });
            yield return enemyPrefabs;

            _EnemyPrefabs = enemyPrefabs.Result.ToList();
            _EnemiesLoaded = true;
            _EnemiesLoading = false;

            AsyncOperationHandle<EnemyEvent> loadSpawnEvent = _EnemySpawnedEventAssetRef.LoadAssetAsync();
            yield return loadSpawnEvent;
            _EnemySpawnedEvent = loadSpawnEvent.Result;

            AsyncOperationHandle<EnemyEvent> loadDiedEvent = _EnemyDiedEventAssetRef.LoadAssetAsync();
            yield return loadDiedEvent;
            _EnemyDiedEvent = loadDiedEvent.Result;

            AsyncOperationHandle<EnemyWaveEvent> loadWaveCompleted = _WaveCompletedEventAssetRef.LoadAssetAsync();
            yield return loadWaveCompleted;
            _WaveCompletedEvent = loadWaveCompleted.Result;

            AsyncOperationHandle<BoolValue> loadPause = _GamePauseValueAssetRef.LoadAssetAsync();
            yield return loadPause;
            _GamePauseValue = loadPause.Result;
            _GamePauseValue.OnValueChanged += GamePaused;

            AsyncOperationHandle<FloatValue> loadWaveDuration = _WaveDurationValueAssetRef.LoadAssetAsync();
            yield return loadWaveDuration;
            _WaveDurationValue = loadWaveDuration.Result;
        }

        public void GamePaused(bool paused)
        {

        }

        #region Callbacks

        public void OnPlayerCreated(PlayerUnit player)
        {
            _Player = player;
        }

        public void CreateNewWave(EnemyWave wave)
        {
            _CurrentWave = wave;
            StopCoroutine("CreateEnemies");
            StartCoroutine("CreateEnemies");
        }

        public void StartEnemyWave(EnemyWave wave)
        {
            StartCoroutine(SpawnEnemies());
        }

        #endregion

        private IEnumerator CreateEnemies()
        {
            _WaveEnemiesCreated = false;
            yield return new WaitUntil(() => _EnemiesLoaded);

            _DeadEnemyCount = 0;

            //TODO - Pool the enemies
            if (_WaveEnemies == null) _WaveEnemies = new List<GameObject>();
            foreach (GameObject enemy in _WaveEnemies) Destroy(enemy);
            _WaveEnemies.Clear();

            List<EnemyData> enemies = _CurrentWave.Enemies.ToList();
            //Sort in order of spawn
            enemies = enemies.OrderBy(s => s.SpawnTime).ToList();

            foreach (EnemyData enemySpawn in enemies)
            {
                //Grab the prefab of the enemy with the matching ID
                EnemyData data = enemySpawn;
                GameObject enemyPrefab = _EnemyPrefabs.FirstOrDefault(s =>
                {
                    EnemyUnit eb = s.GetComponent<EnemyUnit>();
                    if (eb == null) return false;
                    return eb.EnemyID == data.ID;
                });

                //If no matching, just pick first
                if (enemyPrefab == null) enemyPrefab = _EnemyPrefabs[0];

                Vector3 offscreen = new Vector3(1000, 1000, 1000);
                GameObject enemyObj = Instantiate(enemyPrefab, offscreen, Quaternion.identity);
                _WaveEnemies.Add(enemyObj);

                enemyObj.SetActive(false);

                //Maybe helps with any stuttering from making all these enemies
                //Wont be needing as much if pooling helps
                yield return null;
            }

            _WaveEnemiesCreated = true;
        }

        private IEnumerator SpawnEnemies()
        {
            yield return new WaitUntil(() => _WaveEnemiesCreated);

            float elapsedTime = _WaveDurationValue.GetValue();
            int spawnIndex = 0;

            List<EnemyData> enemies = _CurrentWave.Enemies.ToList();
            //Sort by smallest spawn time at the end, so you can reverse loop and remove them as they spawn from the end
            enemies = enemies.OrderByDescending(s => s.SpawnTime).ToList();

            while (enemies.Any())
            {
                for (int i = enemies.Count - 1; i >= 0; i--)
                {
                    EnemyData enemyData = enemies[i];
                    if (elapsedTime >= enemyData.SpawnTime)
                    {
                        //TODO - Determine spawn location better?
                        GridNode spawnNode = _SpawnNodes[Random.Range(0, _SpawnNodes.Length)];

                        GameObject enemyObj = _WaveEnemies[spawnIndex++];
                        enemyObj.SetActive(true);

                        EnemyUnit enemy = enemyObj.GetComponent<EnemyUnit>();
                        enemy.Spawner = this;
                        enemy.transform.position = spawnNode.transform.position;
                        enemy.EnterNode(spawnNode);
                        enemy.SetTargetPlayer(_Player);

                        _EnemySpawnedEvent?.Raise(enemy);

                        enemies.RemoveAt(i);
                    }
                    else
                    {
                        //Once you reach an enemy that cant be spawned, the others are sorted so cant be spawned either
                        break;
                    }
                }

                elapsedTime = _WaveDurationValue.GetValue();

                //Wait here if the game is paused
                yield return new WaitUntil(() => !_GamePauseValue.GetValue());
            }

            //Wait for all enemies to be deactivated
            while (_DeadEnemyCount != _WaveEnemies.Count)
            {
                yield return null;
            }

            _WaveCompletedEvent?.Raise(_CurrentWave);

            //TODO - Pool Enemies?
            foreach (GameObject waveEnemy in _WaveEnemies)
            {
                Destroy(waveEnemy);
            }
            _WaveEnemies.Clear();
        }

        public void EnemyDied(EnemyUnit enemy)
        {
            _DeadEnemyCount++;
            enemy.gameObject.SetActive(false);
        }
    }
}
