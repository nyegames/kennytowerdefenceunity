﻿using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Enemies.Intelligence
{
    public class EnemyInfluenceBrain : EnemyBrainBase
    {
        protected bool FollowingPath;

        [Header("Knowledge")]
        [SerializeField] private InfluenceRule _PlayerInfluence;

        protected InfluenceMap PlayerInfluenceMap;

        protected GridNode TargetNode;
        private TweenerCore<Vector3, Vector3, VectorOptions> _MovementTween;

        protected override void OnStart()
        {
            base.OnStart();
            StartCoroutine(TrackPlayer());
        }

        #region Overrides of EnemyIntelligenceBase

        public override void SetTargetPlayer(PlayerUnit player)
        {
            base.SetTargetPlayer(player);
            TargetNode = player.Node;
            PlayerInfluenceMap = TargetNode?.Map?.GetInfluenceMap(_PlayerInfluence);
            FollowingPath = true;
        }

        #endregion

        public override void StopMoving()
        {
            base.StopMoving();
            FollowingPath = false;
            StopAllCoroutines();
        }

        protected virtual InfluenceNode GetNextNode()
        {
            InfluenceNode nextNode = null;

            List<InfluenceNode> neighbours = PlayerInfluenceMap?.GetNeighbours(Enemy.Node).Where(s => s.GridNode.Tower == null || s.GridNode.Tower.IsDisabled).ToList();
            //nextNode = neighbours?.OrderBy(s => s.InfluenceScore).FirstOrDefault();
            IGrouping<int, InfluenceNode>[] influnceGroups = neighbours?.GroupBy(s => s.InfluenceScore).OrderBy(s => s.Key).ToArray();

            if (influnceGroups != null && influnceGroups.Any())
            {
                InfluenceNode[] highestGroup = influnceGroups[0].ToArray();
                nextNode = highestGroup[UnityEngine.Random.Range(0, highestGroup.Length)];
            }

            //If no node, or you reached ON or NEAR the target node, then you have finished the path
            if (nextNode == null ||
                nextNode.GridNode == TargetNode ||
                neighbours.Any(s => s.GridNode == TargetNode))
            {
                return null;
            }

            return nextNode;
        }

        #region Overrides of EnemyBrainBase

        public override void SetGamePaused(bool paused)
        {
            base.SetGamePaused(paused);

            if (_MovementTween != null)
            {
                if (paused) _MovementTween.Pause();
                else _MovementTween.Play();
            }
        }

        #endregion

        protected IEnumerator TrackPlayer()
        {
            do
            {
                InfluenceNode nextNode;
                do
                {
                    nextNode = GetNextNode();

                    if (nextNode == null || !FollowingPath)
                    {
                        break;
                    }

                    Vector3 targetPosition = nextNode.GridNode.transform.position;
                    _MovementTween = transform.DOMove(targetPosition, _NodeTravelSeconds);
                    yield return _MovementTween.WaitForCompletion();

                    //Stay here if the game is paused
                    yield return new WaitUntil(() => !Enemy.Paused);

                    // Make sure we got there
                    transform.position = targetPosition;
                    Enemy.EnterNode(nextNode.GridNode);
                    RaiseMovedToNodeEvent();

                } while (true);

                //Stay here if the game is paused
                yield return new WaitUntil(() => !Enemy.Paused);

                FollowingPath = false;
                RaiseEndOfPathEvent();

                yield return new WaitUntil(() => FollowingPath);

            } while (!Dead);
        }
    }
}
