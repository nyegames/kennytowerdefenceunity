﻿using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Map.Pathing;
using Assets.GridMode.Units.Enemies.Specialist;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Units.Tower;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units.Enemies.Intelligence
{
    /// <summary>
    /// Specialist will look for a vital Tower, path to it, and Disable it.
    ///
    /// It can do this X times, before then it will just path towards player like a regular unit
    /// </summary>
    public class EnemySpecialistBrain : EnemyInfluenceBrain
    {
        [Header("Disabling")]
        [SerializeField] private int _DisableTowerCount = 3;
        [SerializeField] private int _CurrentTowersDisabled = 0;

        [SerializeField] private float _DisableChannelTime = 1f;

        [Header("Pathing")]
        [SerializeField] private GridPathFinder _PathFinder;
        private bool _UseInfluence;

        private TweenerCore<Vector3, Vector3, VectorOptions> _MovementTween;

        [Header("Tower Targets")]
        [SerializeField] private SpecialistTargetAssetRef _SpecialistTargetsAssetRef;
        private SpecialistTargets _SpecialistTargets;
        private bool _SkipTower;
        private TowerUnit _TargetTower;

        #region Overrides of EnemyInfluenceBrain

        protected override void OnStart()
        {
            //Dont call the base in order to not track the player and follow influence
            //base.OnStart();

            Enemy.OnEnemyDied += EnemyOnOnEnemyDied;
        }

        private void EnemyOnOnEnemyDied(EnemyUnit obj)
        {
            Enemy.OnEnemyDied -= EnemyOnOnEnemyDied;
            if (_SpecialistTargets != null && _TargetTower != null && _TargetTower.Node != null)
            {
                _SpecialistTargets.Remove(_TargetTower.Node);
            }
            _TargetTower = null;
        }

        #endregion

        #region Overrides of EnemyBrainBase

        public override void SetTargetPlayer(PlayerUnit player)
        {
            base.SetTargetPlayer(player);
            if (_UseInfluence) return;
            StartCoroutine(TrackAndMoveToVitalTower());
        }

        private IEnumerator TrackAndMoveToVitalTower()
        {
            if (_SpecialistTargets == null)
            {
                AsyncOperationHandle<SpecialistTargets> loadTargets = _SpecialistTargetsAssetRef.LoadAssetAsync();
                yield return loadTargets;
                _SpecialistTargets = loadTargets.Result;
            }

            _TargetTower = GetVitalTower();

            if (_TargetTower != null)
            {
                if (_SpecialistTargets.Add(_TargetTower.Node))
                {
                    yield return MoveToTower(_TargetTower);

                    //Stay here if the game is paused
                    yield return new WaitUntil(() => !Enemy.Paused);

                    if (!_SkipTower)
                    {
                        List<GridNode> neighbours = CurrentNode.GetNeighbours();
                        if (neighbours.Any(s => s.GridPosition == _TargetTower.Node.GridPosition))
                        {
                            yield return DisableTower(_TargetTower);

                            yield return new WaitForSeconds(0.1f);

                            _CurrentTowersDisabled++;
                        }
                    }
                }
                _SpecialistTargets.Remove(_TargetTower.Node);

                _SkipTower = false;
            }

            //Stay here if the game is paused
            yield return new WaitUntil(() => !Enemy.Paused);

            //If there are no more towers, or you have disabled your assigned amount, then continue as a normal influence moving unit
            if (_TargetTower == null || _CurrentTowersDisabled > _DisableTowerCount)
            {
                _UseInfluence = true;
                StartCoroutine(TrackPlayer());
            }
            else
            {
                StartCoroutine(TrackAndMoveToVitalTower());
            }
        }

        #region Overrides of EnemyInfluenceBrain

        public override void SetGamePaused(bool paused)
        {
            base.SetGamePaused(paused);

            if (_MovementTween != null)
            {
                if (paused) _MovementTween.Pause();
                else _MovementTween.Play();
            }
        }

        #endregion

        #endregion

        private TowerUnit GetVitalTower()
        {
            //TODO - Do not target a tower that has no free nodes around it
            //TODO - A Free node is a node without any towers on it, disabled or otherwise.

            //Foreach Tower check a Radius of 2 around the Tower.
            //The Tower with the largest difference in PlayerInfluence in those nodes could be considered a vital Tower.
            //(Because it will be causing pathing to go around it quite heavily

            GridNode[] towerNodes = CurrentNode.Map.Nodes.Cast<GridNode>().Where(s => s.Tower != null && !s.Tower.IsDisabled).ToArray();
            InfluenceNode[] towerInfluenceNodes = towerNodes.Select(s => PlayerInfluenceMap.GetNode(s.Column, s.Row)).ToArray();

            List<Tuple<InfluenceNode, int>> towerValues = towerInfluenceNodes.Select(influNode =>
            {
                List<GridNode> radiusNodes = influNode.GridNode.GetRadiusOfNodes(2, false, ignoreTower: true);
                InfluenceNode[] radiusInflunceNodes = radiusNodes.Select(s => PlayerInfluenceMap.GetNode(s.Column, s.Row)).ToArray();

                int influenceMin = radiusInflunceNodes.Min(s => s.InfluenceScore);
                int influenceMax = radiusInflunceNodes.Max(s => s.InfluenceScore);

                int value = Math.Abs(influenceMin - influenceMax);

                return new Tuple<InfluenceNode, int>(influNode, value);
            }).ToList();

            //Order by largest range of player influence around tower first
            towerValues = towerValues.OrderByDescending(s => s.Item2).ToList();

            //This Tower has the largest difference in PlayerInfluence around its location, meaning it is vital
            return towerValues.FirstOrDefault(s =>
            {
                //First one that is not already a specialist target
                return !_SpecialistTargets.Contains(s.Item1);
            })?.Item1?.GridNode?.Tower;
        }

        private IEnumerator MoveToTower(TowerUnit tower)
        {
            //TODO - Only move to a tower on a cell next to it, that does not have a tower on it. Disabled or otherwise.
            //TODO - Grab the neighbours of the tower, and get the one closest to your location, that does not have a tower on it.

            GridPathHandle pathHandle = _PathFinder.FindPath(CurrentNode, tower.Node);
            yield return pathHandle;

            foreach (GridNode gridNode in pathHandle.Path)
            {
                //If there is a tower on the node you are moving to, that is not disabled, then you have arrived.
                if (gridNode.Tower != null && !gridNode.Tower.IsDisabled) break;

                Vector3 targetPosition = gridNode.transform.position;
                _MovementTween = transform.DOMove(targetPosition, _NodeTravelSeconds);
                yield return _MovementTween.WaitForCompletion();

                //Stay here if the game is paused
                yield return new WaitUntil(() => !Enemy.Paused);

                // Make sure we got there
                transform.position = targetPosition;
                Enemy.EnterNode(gridNode);

                //TODO - Have a shared resource for all specialists
                //TODO - Addressable ScriptableObject which stores all the current targeted Towers, so they can avoid targeting the same ones
                if (tower.IsDisabled)
                {
                    _SkipTower = true;
                    yield break;
                }
            }
        }

        private IEnumerator DisableTower(TowerUnit tower)
        {
            yield return new WaitForSeconds(_DisableChannelTime);
            tower.SetDisabled(true);
        }
    }
}
