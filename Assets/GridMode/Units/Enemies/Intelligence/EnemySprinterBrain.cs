﻿using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GridMode.Units.Enemies.Intelligence
{
    public class EnemySprinterBrain : EnemyInfluenceBrain
    {
        private float _DefaultNodeSpeed;

        [Header("Sprint")]
        [Tooltip("How fast to travel between nodes when they are in a straight line")]
        [SerializeField] private float _SprintNodeTravelSeconds = 0.3f;
        [Tooltip("How many nodes need to be together to start sprinting")]
        [SerializeField] private int _NodeRequiredForLine = 3;
        [SerializeField] private bool _Sprinting;
        private GridNode _PreviousNode;

        private List<InfluenceNode> _NextNodes;

        #region Overrides of EnemyBrainBase

        #region Overrides of EnemyInfluenceBrain

        protected override void OnStart()
        {
            base.OnStart();
            _DefaultNodeSpeed = _NodeTravelSeconds;
        }

        #endregion

        protected override void OnArrivedAtNode(GridNode currentNode)
        {
            base.OnArrivedAtNode(currentNode);

            _NextNodes = new List<InfluenceNode>();
            InfluenceNode curentInfluence = PlayerInfluenceMap.GetNode(currentNode.Column, currentNode.Row);
            if (curentInfluence == null) return;

            //Check if the next X nodes are in the same column
            for (int i = 0; i < _NodeRequiredForLine; i++)
            {
                InfluenceNode[] neighbours = PlayerInfluenceMap.GetNeighbours(curentInfluence.GridNode).Where(s => s.GridNode.Tower == null).ToArray();
                neighbours = neighbours.OrderBy(s => s.InfluenceScore).ToArray();
                InfluenceNode next = neighbours.FirstOrDefault();
                _NextNodes.Add(next);

                curentInfluence = next;
            }
            _NextNodes = _NextNodes.Where(s => s != null).ToList();
            InfluenceNode nextNode = _NextNodes.FirstOrDefault();

            //If you are already sprinting, then you sprint until the node you landed on is not in a line from your previous one
            if (_Sprinting && _PreviousNode != null && nextNode != null)
            {
                //If you are moving along the same column, but the next next is not in the same column. Stop sprinting
                if (_PreviousNode.Column == CurrentNode.Column &&
                    CurrentNode.Column != nextNode.GridNode.Column)
                {
                    _Sprinting = false;
                    _NodeTravelSeconds = _DefaultNodeSpeed;
                }
                //If you are moving along the same row, but you wont next node, stop sprinting
                else if (_PreviousNode.Row == CurrentNode.Row &&
                    CurrentNode.Row != nextNode.GridNode.Row)
                {
                    _Sprinting = false;
                    _NodeTravelSeconds = _DefaultNodeSpeed;
                }

            }
            else
            {
                int colCount = 0;
                int rowCount = 0;
                curentInfluence = PlayerInfluenceMap.GetNode(currentNode.Column, currentNode.Row);

                foreach (InfluenceNode influenceNode in _NextNodes)
                {
                    if (curentInfluence.GridNode.Column == influenceNode.GridNode.Column) colCount++;
                    else colCount = 0;

                    if (curentInfluence.GridNode.Row == influenceNode.GridNode.Row) rowCount++;
                    else rowCount = 0;
                }

                //If the next nodes are in a line within the bounds, then you can sprint
                if (colCount >= _NodeRequiredForLine || rowCount >= _NodeRequiredForLine)
                {
                    _NodeTravelSeconds = _SprintNodeTravelSeconds;
                    _Sprinting = true;
                }
            }

            _PreviousNode = currentNode;
        }

        #region Overrides of EnemyInfluenceBrain

        protected override InfluenceNode GetNextNode()
        {
            if (_NextNodes == null) return base.GetNextNode();

            if (CurrentNode.GetNeighbours().Any(s => s == TargetNode))
            {
                return null;
            }

            return _NextNodes?.FirstOrDefault();
        }

        #endregion

        #endregion
    }
}
