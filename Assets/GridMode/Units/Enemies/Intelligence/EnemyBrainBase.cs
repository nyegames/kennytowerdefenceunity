﻿using System;
using System.Collections;
using Assets.Events.Enemies;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Player;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units.Enemies.Intelligence
{
    public abstract class EnemyBrainBase : MonoBehaviour
    {
        protected bool Dead { get; private set; } = false;

        protected EnemyUnit Enemy;

        protected PlayerUnit Player;

        /// <summary>How many seconds it take to move across 1 node</summary>
        [Header("Stats")]
        [SerializeField] protected float _NodeTravelSeconds = 0.25f;

        public event Action<EnemyBrainBase> OnMovedToNode;
        public event Action<EnemyBrainBase> OnReachedEndOfPath;

        public GridNode CurrentNode => Enemy.Node;

        [Header("Events")]
        [SerializeField] private EnemyEventAssetRef _EnemyMovedEnemyEventAssetRef;
        private EnemyEvent _EnemyMovedEvent;

        protected void RaiseMovedToNodeEvent()
        {
            OnMovedToNode?.Invoke(this);
            _EnemyMovedEvent?.Raise(Enemy);
        }

        protected void RaiseEndOfPathEvent() => OnReachedEndOfPath?.Invoke(this);

        private void Awake()
        {
            Enemy = GetComponent<EnemyUnit>();
            OnAwake();
        }

        protected virtual void OnAwake()
        {

        }

        private void Start()
        {
            Enemy = GetComponent<EnemyUnit>();
            OnMovedToNode += MovedToNode;
            OnReachedEndOfPath += MoverOnOnReachedEndOfPath;
            StartCoroutine(LoadEvents());
            OnStart();
        }

        protected virtual IEnumerator LoadEvents()
        {
            AsyncOperationHandle<EnemyEvent> loadMoveEvent = _EnemyMovedEnemyEventAssetRef.LoadAssetAsync();
            yield return loadMoveEvent;
            _EnemyMovedEvent = loadMoveEvent.Result;
        }

        protected virtual void OnStart()
        {

        }

        public virtual void SetTargetPlayer(PlayerUnit player)
        {
            Player = player;
        }

        public void PlayerMoved(PlayerUnit player)
        {
            if (CloseEnoughToTarget(Enemy.Node, Player.Node))
            {
                SelfDestruct();
            }
            else
            {
                OnPlayerMoved(player);
            }
        }

        protected virtual void OnPlayerMoved(PlayerUnit player)
        {

        }

        private void MovedToNode(EnemyBrainBase enemy)
        {
            if (CloseEnoughToTarget(Enemy.Node, Player.Node))
            {
                SelfDestruct();
            }
            else
            {
                OnArrivedAtNode(Enemy.Node);
            }
        }

        protected virtual void OnArrivedAtNode(GridNode currentNode)
        {

        }

        protected void MoverOnOnReachedEndOfPath(EnemyBrainBase enemy)
        {
            if (Dead) return;

            if (CloseEnoughToTarget(Enemy.Node, Player.Node))
            {
                SelfDestruct();
            }
            else
            {
                OnCompletedPath();
            }
        }

        protected virtual void OnCompletedPath()
        {
            SetTargetPlayer(Player);
        }

        public virtual void StopMoving()
        {

        }

        protected virtual void SelfDestruct()
        {
            if (Dead) return;
            Dead = true;
            Enemy.ExplodeUnit();
            StopMoving();
        }

        private static bool CloseEnoughToTarget(GridNode myNode, GridNode targetNode)
        {
            int distanceFromTarget = Math.Abs(myNode.Column - targetNode.Column) + Math.Abs(myNode.Row - targetNode.Row);
            return distanceFromTarget <= 1;
        }

        public virtual void SetGamePaused(bool paused)
        {
            
        }
    }
}
