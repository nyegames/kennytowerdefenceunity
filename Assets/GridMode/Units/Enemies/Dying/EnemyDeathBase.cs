﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Assets.GridMode.Units.Enemies.Dying
{
    public class EnemyDeathBase : MonoBehaviour
    {
        private EnemyUnit _Enemy;

        [Header("Attack")]
        [SerializeField] private int _Damage;
        [SerializeField] private int _DamageRadius = 1;

        [Header("Effects")]
        [SerializeField] private AssetLabelReference _OnDeathLabel;
        private List<GameObject> _OnDeathPrefabs;
        [SerializeField] private AssetLabelReference _EnemyLabel;

        [Header("Visuals")]
        [SerializeField] private List<GameObject> _VisualObjects;

        // Start is called before the first frame update
        private IEnumerator Start()
        {
            _Enemy = GetComponent<EnemyUnit>();

            object[] labels = { _OnDeathLabel, _EnemyLabel };
            AsyncOperationHandle<IList<IResourceLocation>> findDeathAssets = Addressables.LoadResourceLocationsAsync(labels, Addressables.MergeMode.Intersection);
            yield return findDeathAssets;

            AsyncOperationHandle<IList<GameObject>> loadPrefabs = Addressables.LoadAssetsAsync<GameObject>(findDeathAssets.Result, null);
            yield return loadPrefabs;

            _OnDeathPrefabs = loadPrefabs.Result.ToList();

            if (_VisualObjects == null || !_VisualObjects.Any())
            {
                _VisualObjects = new List<GameObject>();
                foreach (Transform child in transform)
                {
                    _VisualObjects.Add(child.gameObject);
                }
            }
        }

        public IEnumerator ExplodeUnit(bool isAttacking)
        {
            yield return new WaitUntil(() => !_Enemy.Paused);

            //If you are dying but not performing your attack while doing so, then just immediately exit
            //TODO - Perform a different animation on death with no attack
            if (!isAttacking) yield break;

            yield return new WaitUntil(() => _OnDeathPrefabs != null);

            GameObject prefab = _OnDeathPrefabs[UnityEngine.Random.Range(0, _OnDeathPrefabs.Count - 1)];
            GameObject deathObj = Instantiate(prefab, transform.position, Quaternion.identity);

            //TODO - Death Animations
            foreach (GameObject visualObject in _VisualObjects)
            {
                visualObject.SetActive(false);
            }

            List<GridNode> neighbours = _Enemy.Node.GetRadiusOfNodes(_DamageRadius, false);
            foreach (GridNode neighbour in neighbours)
            {
                TowerUnit tower = neighbour.Tower;
                if (tower != null)
                {
                    if (_Enemy.Faction.IsAggressiveTowards(tower.Faction))
                    {
                        tower.TakeDamage(_Enemy, _Damage);
                    }
                }

                if (neighbour.Units != null)
                {
                    foreach (GridUnit nodeUnit in neighbour.Units)
                    {
                        if (_Enemy.Faction.IsAggressiveTowards(nodeUnit.Faction))
                        {
                            nodeUnit.TakeDamage(_Enemy, _Damage);
                        }
                    }
                }
            }

        }

        public virtual void SetGamePaused(bool paused)
        {

        }
    }
}
