﻿using System;
using UnityEngine.AddressableAssets;

namespace Assets.GridMode.Units.Enemies.Specialist
{
    [Serializable]
    public class SpecialistTargetAssetRef : AssetReferenceT<SpecialistTargets>
    {
        public SpecialistTargetAssetRef(string guid) : base(guid)
        {
        }
    }
}
