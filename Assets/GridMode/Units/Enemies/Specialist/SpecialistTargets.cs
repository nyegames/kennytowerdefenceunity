﻿using System.Collections.Generic;
using Assets.GridMode.Map.Influence;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GridMode.Units.Enemies.Specialist
{
    [CreateAssetMenu(menuName = "Units/Enemy/SpecialistTargets")]
    public class SpecialistTargets : ScriptableObject
    {
        [SerializeField] List<GridNodePosition> _TowerTargets;

        public bool Add(GridNodePosition nodePos)
        {
            if (_TowerTargets == null) _TowerTargets = new List<GridNodePosition>();
            if (_TowerTargets.Contains(nodePos)) return false;
            _TowerTargets.Add(nodePos);
            return true;
        }

        public bool Contains(GridNodePosition nodePos)
        {
            return _TowerTargets != null && _TowerTargets.Contains(nodePos);
        }

        public void Remove(GridNodePosition nodePos)
        {
            if (_TowerTargets.Contains(nodePos))
            {
                _TowerTargets.Remove(nodePos);
            }
        }

        public bool Contains(InfluenceNode node)
        {
            if (node == null) return false;
            return Contains(new GridNodePosition
            {
                Column = node.GridNode.Column,
                Row = node.GridNode.Row
            });
        }
    }
}
