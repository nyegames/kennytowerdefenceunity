﻿using System;
using System.Collections;
using Assets.Events.Enemies;
using Assets.GameServices.UserInterface.Session.Damage;
using Assets.GridMode.Units.Enemies.Dying;
using Assets.GridMode.Units.Enemies.Intelligence;
using Assets.GridMode.Units.Enemies.Spawn;
using Assets.GridMode.Units.Player;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units.Enemies
{
    public class EnemyUnit : GridUnit
    {
        public EnemySpawner Spawner;

        [Header("Enemy")]
        [SerializeField] protected int _EnemyID;
        public int EnemyID => _EnemyID;

        protected PlayerUnit TargetPlayer;

        protected EnemyBrainBase Brain;

        protected EnemyDeathBase DeathBase;

        private bool _AttackingOnDeath = false;

        [Header("Events")]
        [SerializeField] private EnemyEventAssetRef _EnemyDiedEventAssetRef;
        private EnemyEvent _EnemyDiedEvent;

        public event Action<EnemyUnit> OnEnemyDied;

        public virtual void SetTargetPlayer(PlayerUnit player)
        {
            TargetPlayer = player;
            Brain.SetTargetPlayer(player);
        }

        #region Overrides of GridUnit

        protected override void OnAwake()
        {
            base.OnAwake();
            Brain = GetComponent<EnemyBrainBase>();
            DeathBase = GetComponent<EnemyDeathBase>();
        }

        protected override void OnStart()
        {
            base.OnStart();
            StartCoroutine(LoadEvents());
        }

        private IEnumerator LoadEvents()
        {
            AsyncOperationHandle<EnemyEvent> loadDied = _EnemyDiedEventAssetRef.LoadAssetAsync();
            yield return loadDied;
            _EnemyDiedEvent = loadDied.Result;
        }

        protected override void OnGamePaused(bool paused)
        {
            base.OnGamePaused(paused);
            Brain.SetGamePaused(paused);
            DeathBase.SetGamePaused(paused);
        }

        protected override void OnTakenDamage(DamageHandle damage)
        {
            base.OnTakenDamage(damage);
            if (Health > 0) return;
            if (Dead) return;
            KillUnit();
        }

        protected override void OnUnitKilled()
        {
            base.OnUnitKilled();
            StartCoroutine(PlayDeathProcess(_AttackingOnDeath));
        }

        #endregion

        public void ExplodeUnit()
        {
            if (Dead) return;
            _AttackingOnDeath = true;
            KillUnit();
        }

        private IEnumerator PlayDeathProcess(bool attacking)
        {
            yield return DeathBase.ExplodeUnit(attacking);

            yield return new WaitUntil(() => _EnemyDiedEvent != null);

            OnEnemyDied?.Invoke(this);
            _EnemyDiedEvent?.Raise(this);

            Node.UnitExited(this);

            Spawner.EnemyDied(this);
        }
    }
}
