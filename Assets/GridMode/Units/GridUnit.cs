﻿using Assets.Events.Damage;
using Assets.Events.Map.GridUnit;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Factions;
using Assets.SharedValues.Boolean;
using System.Collections;
using Assets.GameServices.UserInterface.Session.Damage;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units
{
    public class GridUnit : MonoBehaviour
    {
        [Header("Information")]
        [SerializeField] private FactionID _Faction;
        public FactionID Faction => _Faction;

        [SerializeField] private int _MaxHealth;
        public int MaxHealth => _MaxHealth;
        public int Health;

        [SerializeField] protected bool _Dead;
        public bool Dead => _Dead;

        [SerializeField] protected GridNode _Node;
        public GridNode Node => _Node;

        [Header("Game Pause")]
        [SerializeField] private BoolValueAssetRef _GamePauseValueAssetRef;
        private BoolValue _GamePaused;
        public bool Paused => _GamePaused?.GetValue() ?? false;

        [Header("Unit Events")]
        [SerializeField] private DamageEventAssetRef _OnUnitDamagedEventAssetRef;
        private DamageEvent _OnUnitDamagedEvent;

        [SerializeField] private GridUnitEventAssetRef _OnUnitDiedEventAssetRef;
        private GridUnitEvent _OnUnitDiedEvent;

        protected bool ResourcesLoaded;

        protected void Awake()
        {
            Health = _MaxHealth;
            OnAwake();
        }

        protected virtual void OnAwake()
        {

        }

        private void Start()
        {
            StartCoroutine(LoadResources());
            OnStart();
        }

        protected virtual void OnStart()
        {

        }

        private IEnumerator LoadResources()
        {
            AsyncOperationHandle<GridUnitEvent> loadDeathEvent = _OnUnitDiedEventAssetRef.LoadAssetAsync();
            yield return loadDeathEvent;
            _OnUnitDiedEvent = loadDeathEvent.Result;

            AsyncOperationHandle<DamageEvent> loadDamaged = _OnUnitDamagedEventAssetRef.LoadAssetAsync();
            yield return loadDamaged;
            _OnUnitDamagedEvent = loadDamaged.Result;

            AsyncOperationHandle<BoolValue> loadPaused = _GamePauseValueAssetRef.LoadAssetAsync();
            yield return loadPaused;
            _GamePaused = loadPaused.Result;
            _GamePaused.OnValueChanged += s =>
            {
                OnGamePaused(s);
            };

            yield return OnLoadResources();

            ResourcesLoaded = true;
        }

        protected virtual IEnumerator OnLoadResources()
        {
            yield break;
        }

        private void OnDestroy()
        {
            if (_GamePaused != null)
            {
                _GamePaused.OnValueChanged -= OnGamePaused;
            }

            OnDestroyed();
        }

        protected virtual void OnDestroyed()
        {

        }

        protected virtual void OnGamePaused(bool paused)
        {

        }

        public void EnterNode(GridNode node)
        {
            _Node?.UnitExited(this);
            _Node = node;
            _Node.UnitEntered(this);
        }

        public void TakeDamage(GridUnit attacker, int damage)
        {
            Health -= damage;
            if (Health < 0) Health = 0;
            DamageHandle damageHandle = new DamageHandle
            {
                Attacker = attacker,
                Damage = damage,
                Target = this
            };
            StartCoroutine(DamagingUnit(damageHandle));
        }

        private IEnumerator DamagingUnit(DamageHandle damage)
        {
            yield return new WaitUntil(() => _OnUnitDamagedEvent != null);

            _OnUnitDamagedEvent?.Raise(damage);

            OnTakenDamage(damage);
        }

        protected virtual void OnTakenDamage(DamageHandle damage)
        {

        }

        public void KillUnit()
        {
            if (_Dead) return;
            _Dead = true;
            StartCoroutine(KillingUnit());
        }

        private IEnumerator KillingUnit()
        {
            yield return new WaitUntil(() => _OnUnitDiedEvent != null);
            _OnUnitDiedEvent?.Raise(this);
            OnUnitKilled();
        }

        protected virtual void OnUnitKilled()
        {

        }
    }
}
