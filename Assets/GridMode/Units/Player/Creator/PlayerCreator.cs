﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Events.Players;
using Assets.GridMode.Map;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Assets.GridMode.Units.Player.Creator
{
    public class PlayerCreator : MonoBehaviour
    {
        private GridMap _Map;

        [SerializeField] private AssetLabelReference _PlayerLabel;
        private GameObject[] _PlayerPrefabs;

        private PlayerUnit _Player;

        [Header("Spawn")]
        [SerializeField] private GridNodePosition _SpawnNode;

        [Header("Events")]
        [SerializeField] private PlayerEventAssetRef _PlayerLoadedAssetRef;
        private PlayerEvent _PlayerLoadedEvent;

        [SerializeField] private PlayerEventAssetRef _PlayerMovedEventAssetRef;
        private PlayerEvent _PlayerMovedEvent;

        public void OnMapLoaded(GridMap map)
        {
            _Map = map;
            StartCoroutine(LoadPlayer());
        }

        private IEnumerator LoadPlayer()
        {
            if (_PlayerMovedEvent == null)
            {
                AsyncOperationHandle<PlayerEvent> loadPlayerLoad = _PlayerLoadedAssetRef.LoadAssetAsync();
                yield return loadPlayerLoad;
                _PlayerLoadedEvent = loadPlayerLoad.Result;

                AsyncOperationHandle<PlayerEvent> loadPlayerMove = _PlayerMovedEventAssetRef.LoadAssetAsync();
                yield return loadPlayerMove;
                _PlayerMovedEvent = loadPlayerMove.Result;
            }

            AsyncOperationHandle<IList<IResourceLocation>> labelLoad = Addressables.LoadResourceLocationsAsync(_PlayerLabel);
            yield return labelLoad;

            AsyncOperationHandle<IList<GameObject>> loadPlayers = Addressables.LoadAssetsAsync<GameObject>(labelLoad.Result, playerAssets => { });
            yield return loadPlayers;

            _PlayerPrefabs = loadPlayers.Result.ToArray();

            GameObject player = GameObject.Instantiate(_PlayerPrefabs[0]);

            GridNode node = _Map.GetNode(_SpawnNode.Column, _SpawnNode.Row);
            player.transform.position = node.transform.position;
            PlayerUnit playerUnit = player.GetComponent<PlayerUnit>();
            playerUnit.EnterNode(node);

            PlayerPathFinderMover playerPathFinderMover = player.GetComponent<PlayerPathFinderMover>();
            playerPathFinderMover.OnPlayerMovedNode += PlayerMovedToNode;

            _Player = playerUnit;

            _PlayerLoadedEvent?.Raise(_Player);
        }

        private void PlayerMovedToNode(PlayerUnit player)
        {
            _PlayerMovedEvent?.Raise(player);
        }
    }
}
