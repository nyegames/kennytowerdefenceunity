﻿using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Map.Pathing;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GridMode.Units.Player
{
    public class PlayerPathFinderMover : MonoBehaviour
    {
        private bool _PathFinding;

        [SerializeField] private GridPathFinder _PathFinder;
        [SerializeField] private PlayerUnit _Player;

        private GridNode _BufferedNode;

        [Header("Stats")]
        [SerializeField] private float _NodeTravelSeconds;

        private TweenerCore<Vector3, Vector3, VectorOptions> _MovingTween;

        public event Action<PlayerUnit> OnPlayerMovedNode;

        public event Action<PlayerPathFinderMover> OnMovedToNode;
        public event Action<PlayerPathFinderMover> OnReachedEndOfPath;

        private void Awake()
        {
            _Player = GetComponent<PlayerUnit>();
            _PathFinder = GetComponent<GridPathFinder>();
        }

        public void GamePaused(bool paused)
        {
            if (_MovingTween != null)
            {
                if (paused) _MovingTween.Pause();
                else _MovingTween.Play();
            }
        }

        public void SetTargetNode(GridNode targetNode)
        {
            if (_PathFinding)
            {
                _BufferedNode = targetNode;
                return;
            }

            _PathFinding = true;
            StartCoroutine(MovingToNode(targetNode));
        }

        private IEnumerator MovingToNode(GridNode targetNode)
        {
            GridPathHandle pathHandle = _PathFinder.FindPath(_Player.Node, targetNode);
            yield return pathHandle;

            List<GridNode> path = pathHandle.Path;
            while (path.FirstOrDefault() == _Player.Node)
            {
                path.RemoveAt(0);
            }

            foreach (GridNode currentNode in path)
            {
                if (targetNode == null || targetNode.Tower != null ||
                    _BufferedNode != null)
                {
                    //Reached end of path
                    break;
                }

                yield return MoveToNextNode(currentNode);
            }

            //Stay here until no longer paused
            yield return new WaitUntil(() => !_Player.Paused);

            if (_BufferedNode != null)
            {
                GridNode nextNode = _BufferedNode;
                _BufferedNode = null;

                StartCoroutine(MovingToNode(nextNode));
            }
            else
            {
                _PathFinding = false;
                OnReachedEndOfPath?.Invoke(this);
            }
        }

        private IEnumerator MoveToNextNode(GridNode targetNode)
        {
            Vector3 targetPosition = targetNode.transform.position;
            _MovingTween = transform.DOMove(targetPosition, _NodeTravelSeconds);
            yield return _MovingTween.WaitForCompletion();
            _MovingTween = null;

            // Make sure we got there
            transform.position = targetPosition;
            _Player.EnterNode(targetNode);

            OnMovedToNode?.Invoke(this);
            OnPlayerMovedNode?.Invoke(_Player);
        }
    }
}
