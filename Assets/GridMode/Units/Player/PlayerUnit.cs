﻿using Assets.Events.Players;
using System.Collections;
using Assets.GameServices.UserInterface.Session.Damage;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.Units.Player
{
    public class PlayerUnit : GridUnit
    {
        [Header("Events")]
        [SerializeField] private PlayerEventAssetRef _PlayerDefeatedEventAssetRef;
        private PlayerEvent _PlayerDefeatedEvent;

        [SerializeField] private PlayerEventAssetRef _PlayerRevivedEventAssetRef;
        private PlayerEvent _PlayerRevivedEvent;

        [Space(10)]
        [SerializeField] private PlayerPathFinderMover _Mover;

        private bool _Defeated;

        public void Revive()
        {
            Health = MaxHealth;
            _Dead = false;
            _Defeated = false;

            _PlayerRevivedEvent?.Raise(this);
        }

        #region Overrides of GridUnit

        protected override IEnumerator OnLoadResources()
        {
            AsyncOperationHandle<PlayerEvent> loadDefeated = _PlayerDefeatedEventAssetRef.LoadAssetAsync();
            yield return loadDefeated;
            _PlayerDefeatedEvent = loadDefeated.Result;

            AsyncOperationHandle<PlayerEvent> loadRevive = _PlayerRevivedEventAssetRef.LoadAssetAsync();
            yield return loadRevive;
            _PlayerRevivedEvent = loadRevive.Result;
        }

        protected override void OnGamePaused(bool paused)
        {
            base.OnGamePaused(paused);
            _Mover.GamePaused(paused);
        }

        protected override void OnTakenDamage(DamageHandle damage)
        {
            base.OnTakenDamage(damage);

            if (Health <= 0 && !_Defeated)
            {
                _Defeated = true;
                _PlayerDefeatedEvent?.Raise(this);
            }
        }

        #endregion
    }
}
