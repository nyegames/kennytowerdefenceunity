﻿using Assets.Events;
using Assets.SharedValues.Boolean;
using Assets.SharedValues.Float;
using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GridMode.BuildMode
{
    public class BuildModeTimer : MonoBehaviour
    {
        [SerializeField] private BoolValueAssetRef _BuildModeValueAssetRef;
        private BoolValue _BuildModeValue;
        [SerializeField] private FloatValueAssetRef _BuildModeTimeValueAssetRef;
        private FloatValue _BuildModeValueTime;

        [Range(1, 240)]
        [SerializeField] private float _BuildModeDuration = 25;
        [SerializeField] private float _BuildModeTimeLeft = 0;

        [Header("Events")]
        [SerializeField] private GameEventAssetRef _BuildModeCompletedAssetRef;
        private GameEvent _BuildModeCompletedEvent;

        // Start is called before the first frame update
        private IEnumerator Start()
        {
            AsyncOperationHandle<BoolValue> loadBuildMode = _BuildModeValueAssetRef.LoadAssetAsync();
            yield return loadBuildMode;
            _BuildModeValue = loadBuildMode.Result;

            AsyncOperationHandle<FloatValue> loadBuildTime = _BuildModeTimeValueAssetRef.LoadAssetAsync();
            yield return loadBuildTime;
            _BuildModeValueTime = loadBuildTime.Result;

            AsyncOperationHandle<GameEvent> loadBuildCompleted = _BuildModeCompletedAssetRef.LoadAssetAsync();
            yield return loadBuildCompleted;
            _BuildModeCompletedEvent = loadBuildCompleted.Result;

            _BuildModeValueTime.SetValue(_BuildModeDuration);
        }

        #region Callbacks

        public void BuildModeStarted()
        {
            StopCoroutine("StartBuildMode");
            StartCoroutine("StartBuildMode");
        }

        #endregion

        private IEnumerator StartBuildMode()
        {
            yield return new WaitUntil(() => _BuildModeValueTime != null);

            _BuildModeValue.SetValue(true);
            _BuildModeTimeLeft = _BuildModeDuration;

            while (_BuildModeTimeLeft > 0)
            {
                yield return null;

                //If build mode has been changed (usually through editor) then ignore countdown
                if (_BuildModeValueTime.GetValue() <= 0 || !_BuildModeValue.GetValue())
                {
                    break;
                }

                _BuildModeTimeLeft -= Time.deltaTime;
                _BuildModeValueTime.SetValue(_BuildModeTimeLeft);
            }

            _BuildModeTimeLeft = 0f;

            _BuildModeValueTime.SetValue(0);
            _BuildModeValue.SetValue(false);

            _BuildModeCompletedEvent?.Raise();

            _BuildModeValueTime.SetValue(_BuildModeDuration);
        }
    }
}
