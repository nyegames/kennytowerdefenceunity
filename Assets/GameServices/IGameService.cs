﻿using System.Collections;

namespace Assets.GameServices
{
    public interface IGameService
    {
        IEnumerator LoadService();

        void ServiceLoaded();
    }
}
