﻿using Assets.GridMode.Units;
using Assets.GridMode.Units.Enemies;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Wave.API;
using Assets.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.HealthBar
{
    public class HealthBarDisplayer : MonoBehaviour
    {
        private Camera _Camera;

        private class HealthBarUnit
        {
            public Bar.HealthBar HealthBar;
            public GridUnit Unit;
        }

        private List<HealthBarUnit> _HealthBarUnits = new List<HealthBarUnit>();

        [Header("Bars")]
        [SerializeField] private AssetReferenceGameObject _HealthBarAssetRef;
        private GameObject _HealthBarPrefab;

        [Header("Visual")]
        [SerializeField] private Vector2 _BarOffset;
        [Range(0f, 1f)]
        [SerializeField] private float _ShowThreshold = 0.9f;

        private IEnumerator Start()
        {
            _Camera = Camera.main;

            AsyncOperationHandle<GameObject> loadHealthbar = _HealthBarAssetRef.LoadAssetAsync();
            yield return loadHealthbar;
            _HealthBarPrefab = loadHealthbar.Result;
        }

        private IEnumerator CreateHealthBar(GridUnit unit)
        {
            yield return new WaitUntil(() => _HealthBarPrefab != null);

            GameObject obj = Instantiate(_HealthBarPrefab,
                new Vector3(10000, 10000, 10000),
                Quaternion.identity,
                transform);

            _HealthBarUnits.Add(new HealthBarUnit
            {
                Unit = unit,
                HealthBar = obj.GetComponent<Bar.HealthBar>()
            });
        }

        private void Update()
        {
            foreach (HealthBarUnit healthBarUnit in _HealthBarUnits)
            {
                if (healthBarUnit.Unit == null || !healthBarUnit.Unit.isActiveAndEnabled)
                {
                    healthBarUnit.HealthBar.gameObject.SetActive(false);
                    continue;
                }

                float ht = healthBarUnit.Unit.Health.Normalise(0, healthBarUnit.Unit.MaxHealth);
                bool showBar = ht <= _ShowThreshold;

                healthBarUnit.HealthBar.gameObject.SetActive(showBar);

                if (!showBar) continue;

                Vector2 originalSize = new Vector2(1080, 1920);
                Vector2 size = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);

                float width = Normaliser.Normalise(_BarOffset.x, 0, originalSize.x, 0, size.x);
                float height = Normaliser.Normalise(_BarOffset.y, 0, originalSize.y, 0, size.y);
                Vector2 offset = new Vector2(width, height);

                Vector3 pos = Camera.main.WorldToScreenPoint(healthBarUnit.Unit.transform.position + new Vector3(offset.x, offset.y, 0));

                healthBarUnit.HealthBar.transform.position = pos;
                healthBarUnit.HealthBar.SetHealth(healthBarUnit.Unit.Health, healthBarUnit.Unit.MaxHealth);
            }
        }

        #region Callbacks

        public void PlayerLoaded(PlayerUnit player)
        {
            StartCoroutine(CreateHealthBar(player));
        }

        public void EnemyCreated(EnemyUnit enemy)
        {
            StartCoroutine(CreateHealthBar(enemy));
        }

        public void TowerBuilt(TowerUnit tower)
        {
            StartCoroutine(CreateHealthBar(tower));
        }

        public void EnemyWaveDefeated(EnemyWave wave)
        {
            //TODO - Destroy all health bars on enemies
            for (int i = _HealthBarUnits.Count - 1; i >= 0; i--)
            {
                HealthBarUnit healthBarUnit = _HealthBarUnits[i];
                if (healthBarUnit.Unit == null || healthBarUnit.Unit.Health <= 0)
                {
                    Destroy(healthBarUnit.HealthBar.gameObject);
                    _HealthBarUnits.RemoveAt(i);
                }
            }
        }

        #endregion
    }
}
