﻿using Assets.Tools;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.GameServices.UserInterface.Session.HealthBar.Bar
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Image _HealthImage;

        public void SetHealth(int health, int totalHealth)
        {
            _HealthImage.fillAmount = health.Normalise(0, totalHealth);
        }
    }
}
