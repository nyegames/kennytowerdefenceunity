﻿using System.Collections;
using Assets.Events.Tower;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.Context.Options.SellTower
{
    [CreateAssetMenu(menuName = "Context/Options/SellTower")]
    public class SellTowerOption : ContextOption
    {
        [SerializeField] private TowerUnitEventAssetRef _SellTowerEventAssetRef;
        private TowerUnitEvent _SellTowerEvent;

        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            if (!contextData.BuildMode) return false;
            return contextData.Node.Tower != null;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            if (_SellTowerEvent == null)
            {
                AsyncOperationHandle<TowerUnitEvent> load = _SellTowerEventAssetRef.LoadAssetAsync();
                yield return load;
                _SellTowerEvent = load.Result;
            }

            if (node.Tower == null) yield break;
            _SellTowerEvent?.Raise(node.Tower);
        }

        #endregion
    }
}
