﻿using System.Collections;
using Assets.Events.Map.Nodes;
using Assets.GameServices.PlayerData;
using Assets.GameServices.PlayerData.API;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.Context.Options.BuildTower
{
    [CreateAssetMenu(menuName = "Context/Options/BuildTower")]
    public class BuildTowerOption : ContextOption
    {
        [SerializeField] private GridNodeEventAssetRef _RequestTowerBuildEventAssetRef;
        private GridNodeEvent _RequestTowerBuildEvent;

        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            if (!contextData.BuildMode) return false;
            if (contextData.Node.Tower != null) return false;

            PlayerProfile? playerProfile = GameService.Service<ActiveProfile>()?.GetActiveProfile();
            if (playerProfile == null) return true;

            //TODO - Create a TowerDatabase ScriptableObject/AssetRef which holds all the information about Towers, including build price.
            int towerPointsRequiredToBuild = 0;

            if (playerProfile.Value.Session.TowerPoints < towerPointsRequiredToBuild)
            {
                return false;
            }

            return true;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            if (_RequestTowerBuildEvent == null)
            {
                AsyncOperationHandle<GridNodeEvent> load = _RequestTowerBuildEventAssetRef.LoadAssetAsync();
                yield return load;
                _RequestTowerBuildEvent = load.Result;
            }

            _RequestTowerBuildEvent?.Raise(node);
        }

        #endregion
    }
}
