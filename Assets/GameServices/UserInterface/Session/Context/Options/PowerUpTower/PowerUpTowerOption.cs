﻿using System.Collections;
using Assets.Events.Tower;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.Context.Options.PowerUpTower
{
    [CreateAssetMenu(menuName = "Context/Options/PowerUpTower")]
    public class PowerUpTowerOption : ContextOption
    {
        [SerializeField] private TowerUnitEventAssetRef _PowerUpTowerEventAssetRef;
        private TowerUnitEvent _PowerGridTowerEvent;

        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            if (contextData.BuildMode) return false;
            if (contextData.Node.Tower == null) return false;
            return true;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            if (_PowerGridTowerEvent == null)
            {
                AsyncOperationHandle<TowerUnitEvent> load = _PowerUpTowerEventAssetRef.LoadAssetAsync();
                yield return load;
                _PowerGridTowerEvent = load.Result;
            }

            if (node.Tower == null) yield break;
            _PowerGridTowerEvent?.Raise(node.Tower);
        }

        #endregion
    }
}
