﻿using System.Collections;
using Assets.Events.Tower;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.Context.Options.TowerInfo
{
    [CreateAssetMenu(menuName = "Context/Options/WeaponPurchase")]
    public class WeaponPurchaseOption : ContextOption
    {
        [SerializeField] private TowerUnitEventAssetRef _OpenTowerInfoEventAssetRef;
        private TowerUnitEvent _OpenTowerInfoEvent;

        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            if (!contextData.BuildMode) return false;
            return contextData.Node.Tower != null;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            if (_OpenTowerInfoEvent == null)
            {
                AsyncOperationHandle<TowerUnitEvent> load = _OpenTowerInfoEventAssetRef.LoadAssetAsync();
                yield return load;
                _OpenTowerInfoEvent = load.Result;
            }

            if (node.Tower == null) yield break;
            _OpenTowerInfoEvent?.Raise(node.Tower);
        }

        #endregion
    }
}
