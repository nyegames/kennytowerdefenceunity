﻿using System.Collections;
using Assets.GameServices.UserInterface.Session.Context.Buttons;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Session.Context.Options
{
    /// <summary>
    /// Inside a <see cref="ContextMenu"/> there are the prefabs <see cref="ContextButton"/> which contain this as an identifying piece of information to say what
    /// they do inside the menu.
    /// </summary>
    public abstract class ContextOption : ScriptableObject
    {
        [SerializeField] protected Sprite Icon;
        public Sprite OptionIcon => Icon;

        public abstract bool IsAvailable(ContextData contextData);

        public abstract IEnumerator Trigger(GridNode node);
    }
}
