﻿using System.Collections;
using Assets.GridMode.Map.Nodes;
using Assets.GridMode.Units.Tower;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Session.Context.Options.DisableTower
{
    [CreateAssetMenu(menuName = "Context/Options/DisableTower")]
    public class DisableTowerOption : ContextOption
    {
        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            return contextData.Node.Tower != null;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            TowerUnit tower = node.Tower;
            if (tower == null) yield break;

            tower.SetDisabled(true);
        }

        #endregion
    }
}
