﻿using System.Collections;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Session.Context.Options.RotateTower
{
    [CreateAssetMenu(menuName = "Context/Options/RotateTower")]
    public class RotateTowerOption : ContextOption
    {
        #region Overrides of ContextOption

        public override bool IsAvailable(ContextData contextData)
        {
            return contextData.Node.Tower != null;
        }

        public override IEnumerator Trigger(GridNode node)
        {
            if (node.Tower == null || node.Tower.IsDisabled) yield break;

            node.Tower.RotateStartingDirection();
        }

        #endregion
    }
}
