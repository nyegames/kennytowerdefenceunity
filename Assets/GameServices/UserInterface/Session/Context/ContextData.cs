﻿using System;
using Assets.GridMode.Map.Nodes;

namespace Assets.GameServices.UserInterface.Session.Context
{
    [Serializable]
    public struct ContextData
    {
        public bool BuildMode;

        public GridNode Node;
    }
}
