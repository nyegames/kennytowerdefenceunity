﻿using System;
using System.Collections;
using Assets.GameServices.UserInterface.Session.Context.Options;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.GameServices.UserInterface.Session.Context.Buttons
{
    /// <summary>
    /// A ContextButton is the Prefab of the button that will be added into a ContextMenu when required.
    /// </summary>
    public class ContextButton : MonoBehaviour
    {
        [SerializeField] private ContextOption _ContextOption;
        public ContextOption Option => _ContextOption;

        [SerializeField] private Image _Background;
        [SerializeField] private Image _Icon;

        [SerializeField] private Button _Button;

        public event Action<ContextButton> OnClicked;

        private void Awake()
        {
            _Background.enabled = false;
            _Background.raycastTarget = false;

            _Icon.enabled = false;
            _Icon.raycastTarget = false;
            _Icon.sprite = _ContextOption.OptionIcon;

            _Button.interactable = false;
        }

        private void Start()
        {
            _Button.onClick.AddListener(() => OnClicked?.Invoke(this));
        }

        public IEnumerator ShowButton()
        {
            bool value = true;

            _Background.enabled = value;
            _Background.raycastTarget = value;

            _Icon.enabled = value;
            _Icon.raycastTarget = value;

            _Button.interactable = value;
            yield break;
        }

        public void HideButton()
        {
            bool value = false;

            _Background.enabled = value;
            _Background.raycastTarget = value;

            _Icon.enabled = value;
            _Icon.raycastTarget = value;

            _Button.interactable = value;
        }
    }
}
