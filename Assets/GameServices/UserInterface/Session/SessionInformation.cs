﻿using System;
using System.Collections;
using System.Globalization;
using Assets.GameServices.PlayerData.API;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Float;
using Assets.SharedValues.Int32;
using TMPro;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Session
{
    /// <summary>
    /// - Current Wave Number
    /// - Current Time fighting this Wave
    /// - Stored experience from this GameSession (rewarded when you complete the session via death or completion of X waves)
    /// </summary>
    public class SessionInformation : MonoBehaviour
    {
        private bool _Loaded;

        [Header("Wave Info")]
        [SerializeField] private FloatValueAssetRef _WaveDurationValueAssetRef;
        private FloatValue _WaveDuration;
        [SerializeField] private CanvasGroup _WaveTimerGroup;
        [SerializeField] private TextMeshProUGUI _WaveDurationText;
        [Space(5)]
        [SerializeField] private IntValueAssetRef _WaveNumberValueAssetRef;
        private IntValue _WaveNumber;
        [SerializeField] private TextMeshProUGUI _WaveNumberText;

        [Header("Energy")]
        [SerializeField] private TextMeshProUGUI _TowerEnergyText;


        private IEnumerator Start()
        {
            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<FloatValue> loadDuration = _WaveDurationValueAssetRef.LoadAssetAsync();
            yield return loadDuration;
            _WaveDuration = loadDuration.Result;

            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<IntValue> loadWaveNumber = _WaveNumberValueAssetRef.LoadAssetAsync();
            yield return loadWaveNumber;
            _WaveNumber = loadWaveNumber.Result;

            _WaveTimerGroup.alpha = 0f;

            _Loaded = true;
        }

        private void Update()
        {
            if (!_Loaded) return;
            _WaveDurationText.text = $"{TimeSpan.FromSeconds(_WaveDuration.GetValue()):mm\\:ss}";
            _WaveNumberText.text = _WaveNumber.GetValue().ToString(CultureInfo.InvariantCulture);
        }

        public void PlayerProfileUpdated(PlayerProfile profile)
        {
            _TowerEnergyText.text = profile.Session.TowerPoints.ToString(CultureInfo.InvariantCulture);
            _WaveNumberText.text = profile.Session.WaveNumber.ToString(CultureInfo.InvariantCulture);
        }

        public void BuildModeStarted()
        {
            _WaveTimerGroup.alpha = 0f;
        }

        public void WaveStarted(EnemyWave wave)
        {
            _WaveTimerGroup.alpha = 1f;
        }
    }
}
