﻿using System.Collections.Generic;
using System.Linq;
using Assets.GridMode.Wave.API;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.GameServices.UserInterface.Session.Wave
{
    /// <summary>
    /// During BuildMode will display the next EnemyWave information as icons for each unique type of enemy in the next wave
    /// </summary>
    public class WaveInformation : MonoBehaviour
    {
        [Header("Icon")]
        [SerializeField] private GameObject _EnemyWaveIconPrefab;

        [Header("Layout")]
        [SerializeField] private RectTransform _GroupTransform;
        private Image _BackgroundImage;

        private bool _BuildMode = false;
        private EnemyWave _Wave;

        private List<GameObject> _Icons = new List<GameObject>();

        private void Start()
        {
            _BackgroundImage = GetComponent<Image>();
        }

        public void BuildModeStarted()
        {
            _BuildMode = true;
        }

        public void NextWaveGenerated(EnemyWave wave)
        {
            _Wave = wave;

            _BackgroundImage.enabled = true;

            foreach (GameObject icon in _Icons) Destroy(icon);
            _Icons.Clear();

            foreach (IGrouping<int, EnemyData> waveEnemy in _Wave.Enemies.GroupBy(s => s.ID))
            {
                GameObject iconObj = Instantiate(_EnemyWaveIconPrefab, _GroupTransform);
                _Icons.Add(iconObj);

                if (iconObj.TryGetComponent(out EnemyWaveIcon icon))
                {
                    icon.SetEnemyCount(waveEnemy.Count());
                    //TODO - Enemy database with scriptable objects where each enemy has an Icon for itself
                    //icon.SetSprite(sprite);
                    Color colour = Color.white;
                    if (waveEnemy.Key == 1) colour = Color.red;
                    else if (waveEnemy.Key == 2) colour = Color.cyan;
                    else if (waveEnemy.Key == 3) colour = Color.blue;
                    else if (waveEnemy.Key == 4) colour = Color.green;

                    //TODO - remove this hack once the enemy database provides a sprite
                    icon.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = colour;
                }
            }
        }

        public void WaveStarted(EnemyWave wave)
        {
            foreach (GameObject icon in _Icons)
            {
                Destroy(icon);
            }
            _Icons.Clear();
            _BackgroundImage.enabled = false;
        }
    }
}
