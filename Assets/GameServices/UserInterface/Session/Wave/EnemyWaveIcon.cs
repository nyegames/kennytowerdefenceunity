﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.GameServices.UserInterface.Session.Wave
{
    public class EnemyWaveIcon : MonoBehaviour
    {
        [SerializeField] private Image _Icon;
        [SerializeField] private TextMeshProUGUI _Count;

        public void SetSprite(Sprite sprite)
        {
            _Icon.sprite = sprite;
        }

        public void SetEnemyCount(int count)
        {
            _Count.text = $"X {count}";
        }
    }
}
