﻿using System;
using Assets.GridMode.Units;

namespace Assets.GameServices.UserInterface.Session.Damage
{
    [Serializable]
    public struct DamageHandle
    {
        public GridUnit Attacker;
        public int Damage;
        public GridUnit Target;
    }
}
