﻿using Assets.GameServices.UserInterface.Session.Damage.Floaty;
using Assets.GridMode.Units;
using Assets.GridMode.Units.Enemies;
using Assets.GridMode.Units.Player;
using Assets.GridMode.Units.Tower;
using Assets.Tools;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.GameServices.UserInterface.Session.Damage
{
    public class DamageFloatys : MonoBehaviour
    {
        [Header("Damage")]
        [SerializeField] private AssetReferenceGameObject _DamageFloatyAssetRef;
        private GameObject _DamageFloatyPrefab;

        [Header("Floaty Visuals")]
        [SerializeField] private Vector2 _OffsetFromTarget;

        private void Start()
        {
            StartCoroutine(LoadFloaties());
        }

        private IEnumerator LoadFloaties()
        {
            AsyncOperationHandle<GameObject> loadDamageFloaty = _DamageFloatyAssetRef.LoadAssetAsync();
            yield return loadDamageFloaty;
            _DamageFloatyPrefab = loadDamageFloaty.Result;
        }

        #region Damage

        private IEnumerator PlayerDamaged(DamageHandle damage)
        {
            yield return new WaitUntil(() => _DamageFloatyPrefab != null);
        }

        private IEnumerator EnemyDamaged(DamageHandle damage)
        {
            yield return new WaitUntil(() => _DamageFloatyPrefab != null);

            Vector2 originalSize = new Vector2(1080, 1920);
            Vector2 size = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);

            float width = Normaliser.Normalise(_OffsetFromTarget.x, 0, originalSize.x, 0, size.x);
            float height = Normaliser.Normalise(_OffsetFromTarget.y, 0, originalSize.y, 0, size.y);
            Vector2 offset = new Vector2(width, height);

            Vector3 pos = Camera.main.WorldToScreenPoint(damage.Target.transform.position + new Vector3(offset.x, offset.y, 0));

            GameObject floatyObj = Instantiate(_DamageFloatyPrefab,
                pos,
                Quaternion.identity,
                transform);
            DamageFloaty floaty = floatyObj.GetComponent<DamageFloaty>();
            floaty.DisplayDamage(damage);
        }

        private IEnumerator TowerDamaged(DamageHandle damage)
        {
            yield return new WaitUntil(() => _DamageFloatyPrefab != null);
        }

        #endregion

        #region Deaths

        private void EnemyDied(EnemyUnit enemy)
        {

        }

        private void TowerDied(TowerUnit tower)
        {

        }

        private void PlayerDied(PlayerUnit player)
        {

        }

        #endregion

        #region Callbacks

        public void OnUnitDamaged(DamageHandle damage)
        {
            switch (damage.Target)
            {
                case EnemyUnit eu:
                    StartCoroutine(EnemyDamaged(damage));
                    break;
                case TowerUnit tu:
                    StartCoroutine(TowerDamaged(damage));
                    break;
                case PlayerUnit pu:
                    StartCoroutine(PlayerDamaged(damage));
                    break;
            }
        }

        public void OnUnitDied(GridUnit unit)
        {
            switch (unit)
            {
                case EnemyUnit eu:
                    EnemyDied(eu);
                    break;
                case TowerUnit tu:
                    TowerDied(tu);
                    break;
                case PlayerUnit pu:
                    PlayerDied(pu);
                    break;
            }
        }

        #endregion
    }
}
