﻿using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TMPro;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Session.Damage.Floaty
{
    public class DamageFloaty : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private CanvasGroup _Group;
        [SerializeField] private TextMeshProUGUI _DamageText;

        [Header("Animation")]
        [SerializeField] private Vector2 _Movement = new Vector2(0, 100);
        [SerializeField] private float _MoveDuration = 1;
        [SerializeField] private float _FadeDuration;


        private bool _Playing = false;
        private DamageHandle _Damage;

        public void DisplayDamage(DamageHandle damage)
        {
            _Damage = damage;
            _DamageText.text = damage.Damage.ToString();
            StartCoroutine(FloatUpAndFade());
        }

        private IEnumerator FloatUpAndFade()
        {
            Vector3 endValue = transform.position + new Vector3(_Movement.x, _Movement.y, 0);
            TweenerCore<Vector3, Vector3, VectorOptions> moveFloaty = transform.DOMove(endValue, _MoveDuration);

            TweenerCore<float, float, FloatOptions> fadeFloaty = _Group.DOFade(0, _FadeDuration);

            yield return fadeFloaty.WaitForCompletion();

            Destroy(gameObject);
        }
    }
}
