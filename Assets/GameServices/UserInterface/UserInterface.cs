﻿using Assets.SharedValues.Boolean;
using System.Collections;
using UnityEngine;

namespace Assets.GameServices.UserInterface
{
    public class UserInterface : MonoBehaviour, IGameService
    {
        public enum GameMode
        {
            NONE,
            MAIN_MENU,
            GAME_SESSION
        }

        [SerializeField] GameMode _CurrentGameMode = GameMode.NONE;
        public GameMode CurrentGameMode => _CurrentGameMode;

        [Header("Main Components")]
        [SerializeField] private CanvasGroup _PlayerInformation;
        [SerializeField] private CanvasGroup _BuildModeCountDown;
        [SerializeField] private CanvasGroup _SessionInformation;

        [Space(10)]
        [SerializeField] private BoolValueAssetRef _BuildModeValueAssetRef;
        private BoolValue _BuildModeValue;

        #region Implementation of IGameService

        public IEnumerator LoadService()
        {
            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<BoolValue> load = _BuildModeValueAssetRef.LoadAssetAsync();
            yield return load;
            _BuildModeValue = load.Result;
        }

        public void ServiceLoaded()
        {

        }

        #endregion

        public void SetConfig(GameMode gameMode)
        {
            _CurrentGameMode = gameMode;
            UpdateConfig(gameMode);
        }

        private void UpdateConfig(GameMode gameMode)
        {
            StopCoroutine("ApplyConfig");
            StartCoroutine("ApplyConfig", gameMode);
        }

        private IEnumerator ApplyConfig(GameMode gameMode)
        {
            yield return new WaitUntil(() => _BuildModeValue != null);

            switch (gameMode)
            {
                case GameMode.NONE:
                    _PlayerInformation.interactable = false;
                    _PlayerInformation.alpha = 0;

                    _BuildModeCountDown.interactable = false;
                    _BuildModeCountDown.alpha = 0;

                    _SessionInformation.interactable = false;
                    _SessionInformation.alpha = 0;

                    break;

                case GameMode.MAIN_MENU:

                    _PlayerInformation.interactable = true;
                    _PlayerInformation.alpha = 1;

                    _BuildModeCountDown.interactable = false;
                    _BuildModeCountDown.alpha = 0;

                    _SessionInformation.interactable = false;
                    _SessionInformation.alpha = 0;

                    break;

                case GameMode.GAME_SESSION:

                    bool buildMode = _BuildModeValue.GetValue();

                    _PlayerInformation.interactable = false;
                    _PlayerInformation.alpha = 0;

                    _BuildModeCountDown.interactable = buildMode;
                    _BuildModeCountDown.alpha = buildMode ? 1 : 0;

                    _SessionInformation.interactable = true;
                    _SessionInformation.alpha = 1;

                    break;
            }
        }

        #region Callbacks

        public void BuildModeChanged(bool buildMode)
        {
            SetConfig(GameMode.GAME_SESSION);
        }

        #endregion
    }
}
