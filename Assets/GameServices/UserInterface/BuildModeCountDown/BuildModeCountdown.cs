﻿using System;
using System.Collections;
using Assets.SharedValues.Float;
using TMPro;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace Assets.GameServices.UserInterface.BuildModeCountDown
{
    public class BuildModeCountdown : MonoBehaviour
    {
        [SerializeField] private FloatValueAssetRef _BuildModeTimeValueAssetRef;
        private FloatValue _BuildModeTime;

        [SerializeField] private CanvasGroup _BuildModeCanvasGroup;
        [SerializeField] private TextMeshProUGUI _BuildModeTimerText;

        [SerializeField] private Button _SkipButton;

        private IEnumerator Start()
        {
            AsyncOperationHandle<FloatValue> loadTime = _BuildModeTimeValueAssetRef.LoadAssetAsync();
            yield return loadTime;
            _BuildModeTime = loadTime.Result;
        }

        private void Update()
        {
            if (_BuildModeTime != null)
            {
                _BuildModeTimerText.text = $"{TimeSpan.FromSeconds(_BuildModeTime.GetValue()):mm\\:ss}";
            }
        }

        public void BuildModeStart()
        {
            _BuildModeCanvasGroup.alpha = 1f;
            _SkipButton.gameObject.SetActive(true);
        }

        public void BuildModeCompleted()
        {
            _BuildModeCanvasGroup.alpha = 0f;
            _SkipButton.gameObject.SetActive(false);
        }

        #region Callbacks

        public void SkipBuildMode()
        {
            _BuildModeTime.SetValue(0);
        }

        #endregion
    }
}
