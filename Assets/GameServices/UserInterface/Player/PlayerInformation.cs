﻿using Assets.GameServices.PlayerData;
using Assets.GameServices.PlayerData.API;
using TMPro;
using UnityEngine;

namespace Assets.GameServices.UserInterface.Player
{
    public class PlayerInformation : MonoBehaviour
    {
        [Header("Info")]
        [SerializeField] private TextMeshProUGUI _PlayerNameText;
        [SerializeField] private TextMeshProUGUI _PlayerLevelText;

        [Header("Currency")]
        [SerializeField] private TextMeshProUGUI _PlayerPrimaryText;
        [SerializeField] private TextMeshProUGUI _PlayerPremiumText;

        // Start is called before the first frame update
        private void Start()
        {
            PlayerProfile? playerProfile = GameService.Service<ActiveProfile>()?.GetActiveProfile();
            if (playerProfile != null)
            {
                PlayerProfileModified(playerProfile.Value);
            }
        }

        public void PlayerProfileModified(PlayerProfile profile)
        {
            _PlayerNameText.text = profile.NickName ?? "No Profile";
            _PlayerLevelText.text = profile.Progression.Level.ToString();

            _PlayerPrimaryText.text = profile.Wallet.Coins.ToString();
            _PlayerPremiumText.text = profile.Wallet.Gems.ToString();
        }
    }
}
