﻿using Assets.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Assets.GameServices
{
    public class GameService : MonoBehaviour
    {
        private static GameService _Instance;
        private static GameService Instance => _Instance ?? (_Instance = FindObjectOfType<GameService>());

        public static T Service<T>() where T : IGameService
        {
            return Instance == null ? default : Instance.GetService<T>();
        }

        [Header("Game Services")]
        [SerializeField] private AssetLabelReference _GameServiceLabel;
        [SerializeField] private List<IGameService> _GameServices;

        [Header("Events")]
        [SerializeField] private GameEventAssetRef _InitialisedEventAssetRef;
        private GameEvent _InitialisedEvent;

        // Start is called before the first frame update
        private IEnumerator Start()
        {
            Application.targetFrameRate = 60;
            DontDestroyOnLoad(gameObject);
            _GameServices = new List<IGameService>();

            AsyncOperationHandle<IList<IResourceLocation>> loadServiceLocations = Addressables.LoadResourceLocationsAsync(_GameServiceLabel);
            yield return loadServiceLocations;

            foreach (IResourceLocation resourceLocation in loadServiceLocations.Result)
            {
                AsyncOperationHandle<GameObject> loadService = Addressables.InstantiateAsync(resourceLocation, transform);
                yield return loadService;
                IGameService gameService = loadService.Result.GetComponent<IGameService>();
                yield return gameService.LoadService();
                _GameServices.Add(gameService);
            }

            IGameService[] gameServices = _GameServices.OrderBy(s => s.GetType().GetCustomAttribute<GameServiceOrder>()?.Order ?? Int32.MaxValue).ToArray();
            foreach (IGameService gameService in gameServices)
            {
                gameService.ServiceLoaded();
            }

            AsyncOperationHandle<GameEvent> loadInit = _InitialisedEventAssetRef.LoadAssetAsync();
            yield return loadInit;
            _InitialisedEvent = loadInit.Result;

            _InitialisedEvent?.Raise();
        }

        public T GetService<T>() where T : IGameService
        {
            return _GameServices.OfType<T>().FirstOrDefault();
        }
    }
}