﻿using Assets.GridMode.Wave.API;
using System;

namespace Assets.GameServices.PlayerData.API
{
    /// <summary>
    /// Information about the players current "session".
    /// A session is the fighting portion of the game where you fight against waves.
    /// If a session exists and is not null, the the player is still fighting and hasnt been defeated yet.
    /// </summary>
    [Serializable]
    public struct PlayerProfileSession
    {
        public bool Active;

        /// <summary>The current wave number you are in</summary>
        public int WaveNumber;

        /// <summary>Currency used to upgrade the towers</summary>
        public int TowerPoints;

        /// <summary>The current wave that you are fighting</summary>
        public EnemyWave CurrentWave;

        //TODO - PlayerState (health, grid position, any upgrades the player has)

        //TODO - TowerData (Where all the towers are, and their upgrades)

        //TODO - SessionCurrencies (Exp stored, picksup collected)

        //TODO - Have more specific information to allow "join in progress" waves. This would be a lot more read/writes so just saving a wave is fine for now.
    }
}
