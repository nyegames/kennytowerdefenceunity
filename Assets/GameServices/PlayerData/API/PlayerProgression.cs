﻿using System;

namespace Assets.GameServices.PlayerData.API
{
    [Serializable]
    public struct PlayerProgression
    {
        public string PlayerID;

        /// <summary>The currency level the player has achieved</summary>
        public long Level;

        /// <summary>The total experience the player has earnt</summary>
        public long TotalExperience;
    }
}
