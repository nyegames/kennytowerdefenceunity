﻿using System;

namespace Assets.GameServices.PlayerData.API
{
    [Serializable]
    public struct PlayerProfile
    {
        /// <summary>Unique GUID for this player</summary>
        public string PlayerID;

        /// <summary>Human name the player has given themselves</summary>
        public string NickName;

        public PlayerWallet Wallet;

        public PlayerProgression Progression;

        public PlayerProfileSession Session;
    }
}
