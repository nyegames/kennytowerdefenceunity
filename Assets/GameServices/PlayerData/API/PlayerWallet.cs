﻿using System;

namespace Assets.GameServices.PlayerData.API
{
    [Serializable]
    public struct PlayerWallet
    {
        public string PlayerID;

        /// <summary>Primary currency that you earn after finishing a wave</summary>
        public long Coins;

        /// <summary>Premium currency that you earn through purchases and challenges</summary>
        public long Gems;
    }
}
