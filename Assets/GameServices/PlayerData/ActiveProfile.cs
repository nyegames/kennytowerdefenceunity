﻿using Assets.Events.Profile;
using Assets.GameServices.PlayerData.API;
using Assets.GridMode.Wave.API;
using Assets.SharedValues.Int32;
using Firebase;
using Firebase.Analytics;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.GameServices.PlayerData
{
    /// <summary>This component will load the <see cref="PlayerProfile"/> from data and become an interface between you and the player profile data</summary>
    [GameServiceOrder(0)]
    public class ActiveProfile : MonoBehaviour, IGameService
    {
        /// <summary>How much exp is required per level.
        /// Each level unlocks rewards for the player (coins, gems etc..)
        /// so having it a static amount allows the player to gauge how much time they can play before getting a level.</summary>
        public const long EXPERIENCE_PER_LEVEL = 5000;

        //https://www.youtube.com/watch?v=MbIH4QT3xF8&feature=emb_title

        //TODO - This should be the guid from the firebase account that you save into the firebase database
        //TODO - Require https://firebase.google.com/docs/database/unity/start setup security rules to stop access from anyone

        /// <summary>Each slash is a new child of a parent, put the playerID as the first argument to put it in a table of players</summary>
        private const String FirebasePlayerPathFormat = @"players/{0}";

        private FirebaseDatabase _Database;
        private FirebaseUser _FirebaseUser;
        private DatabaseReference _PlayerFirebaseDatabaseRef;
        private bool _ProfileDirty;

        [SerializeField] private PlayerProfile _PlayerProfile;
        [Header("Events")]
        [SerializeField] private PlayerProfileEventAssetRef _PlayerProfileCreatedAssetRef;
        private PlayerProfileEvent _PlayerProfileCreated;
        [SerializeField] private PlayerProfileEventAssetRef _PlayerProfileModifiedAssetRef;
        private PlayerProfileEvent _PlayerProfileModified;

        [Header("Values")]
        [SerializeField] private IntValueAssetRef _WaveNumberAssetRef;
        private IntValue _WaveNumber;

        public PlayerProfile GetActiveProfile() => _PlayerProfile;

        public bool LoggedIn { get; private set; }

        private IEnumerator LoadResources()
        {
            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<PlayerProfileEvent> loadCreated = _PlayerProfileCreatedAssetRef.LoadAssetAsync();
            yield return loadCreated;
            _PlayerProfileCreated = loadCreated.Result;

            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<PlayerProfileEvent> loadModified = _PlayerProfileModifiedAssetRef.LoadAssetAsync();
            yield return loadModified;
            _PlayerProfileModified = loadModified.Result;

            UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<IntValue> loadWaveNumber = _WaveNumberAssetRef.LoadAssetAsync();
            yield return loadWaveNumber;
            _WaveNumber = loadWaveNumber.Result;
        }

        private IEnumerator InitFireBase()
        {
            bool dependacyCheck = false;
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(firebaseTask =>
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                dependacyCheck = true;
            });
            yield return new WaitUntil(() => dependacyCheck);

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                .RequestServerAuthCode(false /* Don't force refresh */)
                .Build();

            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.Activate();

            bool loginComplete = false;
            Social.localUser.Authenticate(playGamesSuccess =>
            {
                Task<FirebaseUser> loginTask;
                FirebaseAuth firebaseAuth = FirebaseAuth.DefaultInstance;

                //TODO - if this doesnt work, then sign into firebase as an anonymous user
                if (playGamesSuccess)
                {
                    string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                    Credential credential = PlayGamesAuthProvider.GetCredential(authCode);
                    loginTask = firebaseAuth.SignInWithCredentialAsync(credential);
                }
                else
                {
                    loginTask = firebaseAuth.SignInAnonymouslyAsync();
                }

                loginTask.ContinueWith(task =>
                {
                    if (task.IsCanceled || task.IsFaulted)
                    {
                        if (task.IsCanceled) Debug.LogError("SignInWithCredentialAsync was canceled.");
                        else if (task.IsFaulted) Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    }
                    else
                    {
                        _FirebaseUser = task.Result;
                        Debug.LogFormat("User signed in successfully: {0} ({1})", _FirebaseUser.DisplayName, _FirebaseUser.UserId);

                        LoggedIn = true;
                    }

                    loginComplete = true;
                });
            });

            yield return new WaitUntil(() => loginComplete);
        }

        #region Implementation of IGameService

        public IEnumerator LoadService()
        {
            yield return LoadResources();

            yield return InitFireBase();

            if (_FirebaseUser == null)
            {
                Debug.LogError($"FirebaseUser was not generated");
                //TODO - How do deal with firebase not working? It should always init, even on no internet
                yield break;
            }

            _Database = FirebaseDatabase.DefaultInstance;
            string playerPath = string.Format(FirebasePlayerPathFormat, _FirebaseUser.UserId);
            _PlayerFirebaseDatabaseRef = _Database.GetReference(playerPath);
            Task<DataSnapshot> snapShot = _PlayerFirebaseDatabaseRef.GetValueAsync();
            yield return new WaitUntil(() => snapShot.IsCompleted);

            DataSnapshot snapShotResult = snapShot.Result;
            if (!snapShotResult.Exists)
            {
                if (string.IsNullOrEmpty(_PlayerProfile.PlayerID))
                {
                    string displayName = string.IsNullOrEmpty(_FirebaseUser.DisplayName)
                        ? "Default Player"
                        : _FirebaseUser.DisplayName;
                    _PlayerProfile = CreateDefaultUser(_FirebaseUser.UserId, displayName);
                }
            }
            else
            {
                _PlayerProfile = JsonUtility.FromJson<PlayerProfile>(snapShotResult.GetRawJsonValue());
            }

            _PlayerProfileCreated?.Raise(_PlayerProfile);

            _PlayerFirebaseDatabaseRef.ValueChanged += PlayerFirebaseDatabaseRefOnValueChanged;

            yield return Save();
        }

        public void ServiceLoaded()
        {

        }

        #endregion

        private void OnDestroy()
        {
            if (_PlayerFirebaseDatabaseRef != null)
            {
                _PlayerFirebaseDatabaseRef.ValueChanged -= PlayerFirebaseDatabaseRefOnValueChanged;
            }
        }

        #region User

        private void PlayerFirebaseDatabaseRefOnValueChanged(object sender, ValueChangedEventArgs e)
        {
            string rawJsonValue = e.Snapshot?.GetRawJsonValue();
            if (string.IsNullOrEmpty(rawJsonValue)) return;

            _PlayerProfile = JsonUtility.FromJson<PlayerProfile>(rawJsonValue);
            _ProfileDirty = false;

            _PlayerProfileModified?.Raise(_PlayerProfile);
        }

        private static PlayerProfile CreateDefaultUser(string playerid, string displayName)
        {
            PlayerProfile playerProfile = new PlayerProfile
            {
                NickName = displayName,
                PlayerID = playerid
            };
            playerProfile.Wallet = new PlayerWallet
            {
                PlayerID = playerProfile.PlayerID,
                Coins = 100,
                Gems = 0
            };
            playerProfile.Progression = new PlayerProgression
            {
                PlayerID = playerProfile.PlayerID,
                TotalExperience = 0,
                Level = 0
            };

            return playerProfile;
        }

        #endregion

        public void AwardExp(int exp)
        {
            _PlayerProfile.Progression.TotalExperience += exp;
        }

        public void AwardCoins(int coins)
        {
            _PlayerProfile.Wallet.Coins += coins;
        }

        public void AwardTowerEnergy(int energy)
        {
            _PlayerProfile.Session.TowerPoints += energy;
        }

        public void SetEnemyWave(EnemyWave wave)
        {
            _PlayerProfile.Session.CurrentWave = wave;
        }

        public bool RemoveTowerPoints(int towerPoints)
        {
            if (_PlayerProfile.Session.TowerPoints - towerPoints < 0) return false;

            _PlayerProfile.Session.TowerPoints -= towerPoints;
            _PlayerProfile.Session.TowerPoints = Math.Max(0, _PlayerProfile.Session.TowerPoints);

            _PlayerProfileModified?.Raise(_PlayerProfile);

            return true;
        }

        public void AddTowerPoints(int towerPoints)
        {
            _PlayerProfile.Session.TowerPoints += towerPoints;
            _PlayerProfileModified?.Raise(_PlayerProfile);
        }

        public CustomYieldInstruction StartSession()
        {
            _PlayerProfile.Session = new PlayerProfileSession
            {
                Active = true,
                TowerPoints = 100,//25 points for base + 50 points for Blaster (?)
                WaveNumber = 1,
                // Wave filled in by the wave generator
            };
            _WaveNumber.SetValue(_PlayerProfile.Session.WaveNumber);
            return Save();
        }

        public CustomYieldInstruction WaveCompleted()
        {
            _ProfileDirty = true;
            _PlayerProfile.Session.WaveNumber++;
            _WaveNumber.SetValue(_PlayerProfile.Session.WaveNumber);

            //Save, but also wait for the new profile to sync up before continuing
            CustomYieldInstruction saving = Save();
            return new WaitUntil(() => !saving.keepWaiting && !_ProfileDirty);
        }

        public CustomYieldInstruction SessionEnded()
        {
            _PlayerProfile.Session.Active = false;
            return Save();
        }

        public CustomYieldInstruction Save()
        {
            string playerPath = string.Format(FirebasePlayerPathFormat, _FirebaseUser.UserId);
            Task saving = _Database.GetReference(playerPath).SetRawJsonValueAsync(JsonUtility.ToJson(_PlayerProfile));

            _PlayerProfileModified?.Raise(_PlayerProfile);

            return new WaitUntil(() => saving.IsCompleted);
        }
    }
}
