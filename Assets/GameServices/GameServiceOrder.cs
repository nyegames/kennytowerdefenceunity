﻿namespace Assets.GameServices
{
    public class GameServiceOrder : System.Attribute
    {
        public int Order { get; }
        public GameServiceOrder(int order) => Order = order;
    }
}
