﻿using System;
using UnityEngine.AddressableAssets;

namespace Assets.SharedValues.Boolean
{
    [Serializable]
    public class BoolValueAssetRef : AssetReferenceT<BoolValue>
    {
        public BoolValueAssetRef(string guid) : base(guid)
        {
        }
    }
}
