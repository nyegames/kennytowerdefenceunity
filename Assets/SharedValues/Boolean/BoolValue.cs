﻿using System;
using UnityEngine;

namespace Assets.SharedValues.Boolean
{
    [CreateAssetMenu(menuName = "Values/Bool")]
    public class BoolValue : ScriptableObject
    {
        [SerializeField] private bool _Value;

        public event Action<bool> OnValueChanged;

        public void SetValue(bool value)
        {
            bool p = _Value;
            _Value = value;
            if (p != _Value)
            {
                OnValueChanged?.Invoke(_Value);
            }
        }

        public bool GetValue()
        {
            return _Value;
        }
    }
}
