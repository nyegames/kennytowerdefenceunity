﻿using System;
using UnityEngine.AddressableAssets;

namespace Assets.SharedValues.Float
{
    [Serializable]
    public class FloatValueAssetRef : AssetReferenceT<FloatValue>
    {
        public FloatValueAssetRef(string guid) : base(guid)
        {
        }
    }
}
