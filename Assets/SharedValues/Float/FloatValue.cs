﻿using System;
using UnityEngine;

namespace Assets.SharedValues.Float
{
    [CreateAssetMenu(menuName = "Values/Float")]
    public class FloatValue : ScriptableObject
    {
        [SerializeField] private float _Value;

        public event Action<float> OnValueChanged;

        public void SetValue(float value)
        {
            float p = _Value;
            _Value = value;
            if (Math.Abs(p - _Value) > float.Epsilon)
            {
                OnValueChanged?.Invoke(_Value);
            }
        }

        public float GetValue()
        {
            return _Value;
        }
    }
}
