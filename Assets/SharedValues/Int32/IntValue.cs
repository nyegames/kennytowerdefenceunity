﻿using System;
using UnityEngine;

namespace Assets.SharedValues.Int32
{
    [CreateAssetMenu(menuName = "Values/Int32")]
    public class IntValue : ScriptableObject
    {
        [SerializeField] private int _Value;

        public event Action<int> OnValueChanged;

        public void SetValue(int value)
        {
            int p = _Value;
            _Value = value;
            if (p != _Value)
            {
                OnValueChanged?.Invoke(_Value);
            }
        }

        public int GetValue()
        {
            return _Value;
        }
    }
}
