﻿using System;
using UnityEngine.AddressableAssets;

namespace Assets.SharedValues.Int32
{
    [Serializable]
    public class IntValueAssetRef : AssetReferenceT<IntValue>
    {
        public IntValueAssetRef(string guid) : base(guid)
        {
        }
    }
}
