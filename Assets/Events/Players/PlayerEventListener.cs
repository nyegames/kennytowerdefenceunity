﻿using System.Collections;
using Assets.GridMode.Units.Player;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Players
{
    public class PlayerEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private PlayerEventAssetRef _EventAssetRef;
        private PlayerEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PlayerUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<PlayerEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(PlayerUnit player)
        {
            Response.Invoke(player);
        }
    }
}