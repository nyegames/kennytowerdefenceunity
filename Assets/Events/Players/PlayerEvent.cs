﻿using System.Collections.Generic;
using Assets.GridMode.Units.Player;
using UnityEngine;

namespace Assets.Events.Players
{
    [CreateAssetMenu(menuName = "Events/Players/Player")]
    public sealed class PlayerEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<PlayerEventListener> _EventListeners =
            new List<PlayerEventListener>();

        public void Raise(PlayerUnit player)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(player);
            }
        }

        public void RegisterListener(PlayerEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(PlayerEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}