﻿using System;
using Assets.GridMode.Units.Player;
using UnityEngine.Events;

namespace Assets.Events.Players
{
    [Serializable]
    public class PlayerUnityEvent : UnityEvent<PlayerUnit>
    {

    }
}