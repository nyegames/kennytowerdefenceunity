﻿using System;

namespace Assets.Events.Players
{
    [Serializable]
    public class PlayerEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<PlayerEvent>
    {
        public PlayerEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
