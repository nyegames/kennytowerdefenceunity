﻿using Assets.GameServices.PlayerData.API;
using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Profile
{
    public class PlayerProfileEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private PlayerProfileEventAssetRef _EventAssetRef;
        private PlayerProfileEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PlayerProfileUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<PlayerProfileEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(PlayerProfile player)
        {
            Response.Invoke(player);
        }
    }
}