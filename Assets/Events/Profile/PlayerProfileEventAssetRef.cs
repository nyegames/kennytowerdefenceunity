﻿using System;

namespace Assets.Events.Profile
{
    [Serializable]
    public class PlayerProfileEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<PlayerProfileEvent>
    {
        public PlayerProfileEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
