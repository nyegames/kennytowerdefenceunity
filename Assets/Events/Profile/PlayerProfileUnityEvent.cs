﻿using System;
using Assets.GameServices.PlayerData.API;
using UnityEngine.Events;

namespace Assets.Events.Profile
{
    [Serializable]
    public class PlayerProfileUnityEvent : UnityEvent<PlayerProfile>
    {

    }
}