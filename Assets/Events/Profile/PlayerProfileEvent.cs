﻿using Assets.GameServices.PlayerData.API;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Events.Profile
{
    [CreateAssetMenu(menuName = "Events/PlayerProfile")]
    public sealed class PlayerProfileEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<PlayerProfileEventListener> _EventListeners =
            new List<PlayerProfileEventListener>();

        public void Raise(PlayerProfile PlayerProfile)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(PlayerProfile);
            }
        }

        public void RegisterListener(PlayerProfileEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(PlayerProfileEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}