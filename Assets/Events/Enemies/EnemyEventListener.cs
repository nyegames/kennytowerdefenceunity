﻿using System.Collections;
using Assets.GridMode.Units.Enemies;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Enemies
{
    public class EnemyEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private EnemyEventAssetRef _EventAssetRef;
        private EnemyEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public EnemyUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<EnemyEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(EnemyUnit enemy)
        {
            Response.Invoke(enemy);
        }
    }
}