﻿using System;
using Assets.GridMode.Units.Enemies;
using UnityEngine.Events;

namespace Assets.Events.Enemies
{
    [Serializable]
    public class EnemyUnityEvent : UnityEvent<EnemyUnit>
    {

    }
}