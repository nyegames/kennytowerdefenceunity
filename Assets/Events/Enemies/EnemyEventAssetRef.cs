﻿using System;

namespace Assets.Events.Enemies
{
    [Serializable]
    public class EnemyEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<EnemyEvent>
    {
        public EnemyEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
