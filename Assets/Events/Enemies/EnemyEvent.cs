﻿using System.Collections.Generic;
using Assets.GridMode.Units.Enemies;
using UnityEngine;

namespace Assets.Events.Enemies
{
    [CreateAssetMenu(menuName = "Events/Enemy")]
    public sealed class EnemyEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<EnemyEventListener> _EventListeners =
            new List<EnemyEventListener>();

        public void Raise(EnemyUnit enemy)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(enemy);
            }
        }

        public void RegisterListener(EnemyEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(EnemyEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}