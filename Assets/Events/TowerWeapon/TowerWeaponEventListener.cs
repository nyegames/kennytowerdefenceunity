﻿using System.Collections;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.TowerWeapon
{
    public class TowerWeaponEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private TowerWeaponEventAssetRef _EventAssetRef;
        private TowerWeaponEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TowerWeaponUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<TowerWeaponEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(TowerWeaponType towerType, TowerUnit tower)
        {
            Response.Invoke(towerType, tower);
        }
    }
}