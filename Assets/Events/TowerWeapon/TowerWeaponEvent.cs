﻿using System.Collections.Generic;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using UnityEngine;

namespace Assets.Events.TowerWeapon
{
    [CreateAssetMenu(menuName = "Events/Map/TowerWeapon")]
    public sealed class TowerWeaponEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<TowerWeaponEventListener> _EventListeners =
            new List<TowerWeaponEventListener>();

        public void Raise(TowerWeaponType towerType, TowerUnit tower)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(towerType, tower);
            }
        }

        public void RegisterListener(TowerWeaponEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(TowerWeaponEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}