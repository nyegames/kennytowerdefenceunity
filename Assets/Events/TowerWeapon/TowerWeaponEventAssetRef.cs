﻿using System;

namespace Assets.Events.TowerWeapon
{
    [Serializable]
    public class TowerWeaponEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<TowerWeaponEvent>
    {
        public TowerWeaponEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
