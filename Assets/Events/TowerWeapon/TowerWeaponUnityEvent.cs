﻿using System;
using Assets.GridMode.Units.Tower;
using Assets.GridMode.Units.Weapons;
using UnityEngine.Events;

namespace Assets.Events.TowerWeapon
{
    [Serializable]
    public class TowerWeaponUnityEvent : UnityEvent<TowerWeaponType, TowerUnit>
    {

    }
}