﻿using System;

namespace Assets.Events.User.Command
{
    [Serializable]
    public class UserCommandEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<UserCommandEvent>
    {
        public UserCommandEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
