﻿using System.Collections.Generic;
using Assets.GridMode.Control.User;
using UnityEngine;

namespace Assets.Events.User.Command
{
    [CreateAssetMenu(menuName = "Events/User/Command")]
    public sealed class UserCommandEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<UserCommandEventListener> _EventListeners =
            new List<UserCommandEventListener>();

        public void Raise(UserCommand value)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener(UserCommandEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(UserCommandEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}