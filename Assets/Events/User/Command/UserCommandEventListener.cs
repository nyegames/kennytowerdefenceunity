﻿using System.Collections;
using Assets.GridMode.Control.User;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.User.Command
{
    public class UserCommandEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private UserCommandEventAssetRef _EventAssetRef;
        private UserCommandEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public UserCommandUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<UserCommandEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(UserCommand tower)
        {
            Response.Invoke(tower);
        }
    }
}