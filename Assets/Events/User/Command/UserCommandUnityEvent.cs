﻿using Assets.GridMode.Control.User;
using System;
using UnityEngine.Events;

namespace Assets.Events.User.Command
{
    [Serializable]
    public class UserCommandUnityEvent : UnityEvent<UserCommand>
    {

    }
}