﻿using System.Collections.Generic;
using Assets.GridMode.Wave.API;
using UnityEngine;

namespace Assets.Events.Wave
{
    [CreateAssetMenu(menuName = "Events/Wave/EnemyWave")]
    public sealed class EnemyWaveEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<EnemyWaveEventListener> _EventListeners =
            new List<EnemyWaveEventListener>();

        public void Raise(EnemyWave wave)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(wave);
            }
        }

        public void RegisterListener(EnemyWaveEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(EnemyWaveEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}