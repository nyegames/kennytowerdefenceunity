﻿using System.Collections.Generic;
using Assets.GridMode.Rewards;
using UnityEngine;

namespace Assets.Events.Wave.Rewards
{
    [CreateAssetMenu(menuName = "Events/Wave/WaveRewards")]
    public sealed class WaveRewardsEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<WaveRewardsEventListener> _EventListeners =
            new List<WaveRewardsEventListener>();

        public void Raise(WaveRewards wave)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(wave);
            }
        }

        public void RegisterListener(WaveRewardsEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(WaveRewardsEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}