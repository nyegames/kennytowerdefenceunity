﻿using System.Collections;
using Assets.GridMode.Rewards;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Wave.Rewards
{
    public class WaveRewardsEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private WaveRewardsEventAssetRef _EventAssetRef;
        private WaveRewardsEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public WaveRewardsUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<WaveRewardsEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(WaveRewards wave)
        {
            Response.Invoke(wave);
        }
    }
}