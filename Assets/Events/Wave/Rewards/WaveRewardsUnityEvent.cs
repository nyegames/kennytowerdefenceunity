﻿using System;
using Assets.GridMode.Rewards;
using UnityEngine.Events;

namespace Assets.Events.Wave.Rewards
{
    [Serializable]
    public class WaveRewardsUnityEvent : UnityEvent<WaveRewards>
    {

    }
}