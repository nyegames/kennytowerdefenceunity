﻿using System;

namespace Assets.Events.Wave.Rewards
{
    [Serializable]
    public class WaveRewardsEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<WaveRewardsEvent>
    {
        public WaveRewardsEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
