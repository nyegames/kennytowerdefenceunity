﻿using System.Collections;
using Assets.GridMode.Wave.API;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Wave
{
    public class EnemyWaveEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private EnemyWaveEventAssetRef _EventAssetRef;
        private EnemyWaveEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public EnemyWaveUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<EnemyWaveEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(EnemyWave wave)
        {
            Response.Invoke(wave);
        }
    }
}