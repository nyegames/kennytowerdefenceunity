﻿using System;
using Assets.GridMode.Wave.API;
using UnityEngine.Events;

namespace Assets.Events.Wave
{
    [Serializable]
    public class EnemyWaveUnityEvent : UnityEvent<EnemyWave>
    {

    }
}