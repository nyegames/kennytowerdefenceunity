﻿using System;

namespace Assets.Events.Wave
{
    [Serializable]
    public class EnemyWaveEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<EnemyWaveEvent>
    {
        public EnemyWaveEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
