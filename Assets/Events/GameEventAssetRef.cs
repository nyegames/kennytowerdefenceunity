﻿using System;

namespace Assets.Events
{
    [Serializable]
    public class GameEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<GameEvent>
    {
        public GameEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
