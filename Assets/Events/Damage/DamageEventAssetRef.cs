﻿using System;

namespace Assets.Events.Damage
{
    [Serializable]
    public class DamageEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<DamageEvent>
    {
        public DamageEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
