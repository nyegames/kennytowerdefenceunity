﻿using System.Collections;
using Assets.GameServices.UserInterface.Session.Damage;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Damage
{
    public class DamageEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private DamageEventAssetRef _EventAssetRef;
        private DamageEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public DamageUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<DamageEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(DamageHandle damage)
        {
            Response.Invoke(damage);
        }
    }
}