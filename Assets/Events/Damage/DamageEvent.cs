﻿using System.Collections.Generic;
using Assets.GameServices.UserInterface.Session.Damage;
using UnityEngine;

namespace Assets.Events.Damage
{
    [CreateAssetMenu(menuName = "Events/Damage")]
    public sealed class DamageEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<DamageEventListener> _EventListeners =
            new List<DamageEventListener>();

        public void Raise(DamageHandle damage)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(damage);
            }
        }

        public void RegisterListener(DamageEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(DamageEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}