﻿using System;
using Assets.GameServices.UserInterface.Session.Damage;
using UnityEngine.Events;

namespace Assets.Events.Damage
{
    [Serializable]
    public class DamageUnityEvent : UnityEvent<DamageHandle>
    {

    }
}