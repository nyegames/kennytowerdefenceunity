﻿using System;
using UnityEngine.Events;

namespace Assets.Events.IntEvent
{
    [Serializable]
    public class IntUnityEvent : UnityEvent<int>
    {

    }
}