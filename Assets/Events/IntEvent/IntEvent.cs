﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Events.IntEvent
{
    [CreateAssetMenu(menuName = "Events/Int32")]
    public sealed class IntEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<IntEventListener> _EventListeners =
            new List<IntEventListener>();

        public void Raise(int value)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener(IntEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(IntEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}