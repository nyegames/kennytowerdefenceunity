﻿using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.IntEvent
{
    public class IntEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private IntEventAssetRef _EventAssetRef;
        private IntEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public IntUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<IntEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(int value)
        {
            Response.Invoke(value);
        }
    }
}