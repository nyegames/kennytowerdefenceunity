﻿using System;

namespace Assets.Events.IntEvent
{
    [Serializable]
    public class IntEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<IntEvent>
    {
        public IntEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
