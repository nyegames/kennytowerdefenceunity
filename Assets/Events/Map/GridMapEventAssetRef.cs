﻿using System;

namespace Assets.Events.Map
{
    [Serializable]
    public class GridMapEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<GridMapEvent>
    {
        public GridMapEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
