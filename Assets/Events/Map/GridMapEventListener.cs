﻿using System.Collections;
using Assets.GridMode.Map;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Map
{
    public class GridMapEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private GridMapEventAssetRef _EventAssetRef;
        private GridMapEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public GridMapUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<GridMapEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(GridMap map)
        {
            Response.Invoke(map);
        }
    }
}