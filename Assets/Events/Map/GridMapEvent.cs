﻿using System.Collections.Generic;
using Assets.GridMode.Map;
using UnityEngine;

namespace Assets.Events.Map
{
    [CreateAssetMenu(menuName = "Events/Map/Map")]
    public sealed class GridMapEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GridMapEventListener> _EventListeners =
            new List<GridMapEventListener>();

        public void Raise(GridMap gridMap)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(gridMap);
            }
        }

        public void RegisterListener(GridMapEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(GridMapEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}