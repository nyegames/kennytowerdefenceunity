﻿using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Map.GridUnit
{
    public class GridUnitEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private GridUnitEventAssetRef _EventAssetRef;
        private GridUnitEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public GridUnitUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<GridUnitEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(GridMode.Units.GridUnit unit)
        {
            Response.Invoke(unit);
        }
    }
}