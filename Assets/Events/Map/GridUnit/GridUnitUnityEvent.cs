﻿using System;
using UnityEngine.Events;

namespace Assets.Events.Map.GridUnit
{
    [Serializable]
    public class GridUnitUnityEvent : UnityEvent<GridMode.Units.GridUnit>
    {

    }
}