﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Events.Map.GridUnit
{
    [CreateAssetMenu(menuName = "Events/Map/GridUnit")]
    public sealed class GridUnitEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GridUnitEventListener> _EventListeners =
            new List<GridUnitEventListener>();

        public void Raise(GridMode.Units.GridUnit unit)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(unit);
            }
        }

        public void RegisterListener(GridUnitEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(GridUnitEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}