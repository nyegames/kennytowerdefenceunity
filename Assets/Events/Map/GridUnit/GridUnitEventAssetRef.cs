﻿using System;

namespace Assets.Events.Map.GridUnit
{
    [Serializable]
    public class GridUnitEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<GridUnitEvent>
    {
        public GridUnitEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
