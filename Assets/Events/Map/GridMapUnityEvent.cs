﻿using System;
using Assets.GridMode.Map;
using UnityEngine.Events;

namespace Assets.Events.Map
{
    [Serializable]
    public class GridMapUnityEvent : UnityEvent<GridMap>
    {

    }
}