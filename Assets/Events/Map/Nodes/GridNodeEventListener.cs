﻿using System.Collections;
using Assets.GridMode.Map.Nodes;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Map.Nodes
{
    public class GridNodeEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private GridNodeEventAssetRef _EventAssetRef;
        private GridNodeEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public GridNodeUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<GridNodeEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(GridNode gridNode)
        {
            Response.Invoke(gridNode);
        }
    }
}