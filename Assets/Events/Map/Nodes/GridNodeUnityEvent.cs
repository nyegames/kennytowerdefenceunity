﻿using System;
using Assets.GridMode.Map.Nodes;
using UnityEngine.Events;

namespace Assets.Events.Map.Nodes
{
    [Serializable]
    public class GridNodeUnityEvent : UnityEvent<GridNode>
    {

    }
}