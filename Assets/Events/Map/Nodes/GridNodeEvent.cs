﻿using System.Collections.Generic;
using Assets.GridMode.Map.Nodes;
using UnityEngine;

namespace Assets.Events.Map.Nodes
{
    [CreateAssetMenu(menuName = "Events/Map/GridNode")]
    public sealed class GridNodeEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GridNodeEventListener> _EventListeners =
            new List<GridNodeEventListener>();

        public void Raise(GridNode gridNode)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(gridNode);
            }
        }

        public void RegisterListener(GridNodeEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(GridNodeEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}