﻿using System;

namespace Assets.Events.Map.Nodes
{
    [Serializable]
    public class GridNodeEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<GridNodeEvent>
    {
        public GridNodeEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
