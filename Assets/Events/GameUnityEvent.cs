﻿using System;
using UnityEngine.Events;

namespace Assets.Events
{
    [Serializable]
    public class GameUnityEvent : UnityEvent
    {

    }
}