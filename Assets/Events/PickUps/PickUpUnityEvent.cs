﻿using System;
using Assets.GridMode.Pickups;
using UnityEngine.Events;

namespace Assets.Events.PickUps
{
    [Serializable]
    public class PickUpUnityEvent : UnityEvent<NodePickUp>
    {

    }
}