﻿using System;

namespace Assets.Events.PickUps
{
    [Serializable]
    public class PickUpEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<PickUpEvent>
    {
        public PickUpEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
