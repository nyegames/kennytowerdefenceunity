﻿using System.Collections.Generic;
using Assets.GridMode.Pickups;
using UnityEngine;

namespace Assets.Events.PickUps
{
    [CreateAssetMenu(menuName = "Events/PickUps")]
    public sealed class PickUpEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<PickUpEventListener> _EventListeners =
            new List<PickUpEventListener>();

        public void Raise(NodePickUp pickup)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(pickup);
            }
        }

        public void RegisterListener(PickUpEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(PickUpEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}