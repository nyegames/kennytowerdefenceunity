﻿using System.Collections;
using Assets.GridMode.Pickups;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.PickUps
{
    public class PickUpEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private PickUpEventAssetRef _EventAssetRef;
        private PickUpEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PickUpUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<PickUpEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(NodePickUp pickup)
        {
            Response.Invoke(pickup);
        }
    }
}