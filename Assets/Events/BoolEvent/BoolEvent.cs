﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Events.BoolEvent
{
    [CreateAssetMenu(menuName = "Events/Boolean")]
    public sealed class BoolEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<BoolEventListener> _EventListeners =
            new List<BoolEventListener>();

        public void Raise(bool buildModeActive)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(buildModeActive);
            }
        }

        public void RegisterListener(BoolEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(BoolEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}