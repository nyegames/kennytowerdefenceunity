﻿using System;

namespace Assets.Events.BoolEvent
{
    [Serializable]
    public class BoolEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<BoolEvent>
    {
        public BoolEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
