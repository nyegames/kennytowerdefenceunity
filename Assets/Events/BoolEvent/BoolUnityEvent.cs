﻿using System;
using UnityEngine.Events;

namespace Assets.Events.BoolEvent
{
    [Serializable]
    public class BoolUnityEvent : UnityEvent<bool>
    {

    }
}