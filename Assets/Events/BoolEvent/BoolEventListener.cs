﻿using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.BoolEvent
{
    public class BoolEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private BoolEventAssetRef _EventAssetRef;
        private BoolEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public BoolUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<BoolEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(bool active)
        {
            Response.Invoke(active);
        }
    }
}