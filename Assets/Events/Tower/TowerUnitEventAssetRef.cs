﻿using System;

namespace Assets.Events.Tower
{
    [Serializable]
    public class TowerUnitEventAssetRef : UnityEngine.AddressableAssets.AssetReferenceT<TowerUnitEvent>
    {
        public TowerUnitEventAssetRef(string guid) : base(guid)
        {

        }
    }
}
