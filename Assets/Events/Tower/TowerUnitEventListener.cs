﻿using System.Collections;
using Assets.GridMode.Units.Tower;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Events.Tower
{
    public class TowerUnitEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField] private TowerUnitEventAssetRef _EventAssetRef;
        private TowerUnitEvent _Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TowerUnitUnityEvent Response;

        private void OnEnable()
        {
            StartCoroutine(LoadEvent());
        }

        private IEnumerator LoadEvent()
        {
            AsyncOperationHandle<TowerUnitEvent> loadEvent = _EventAssetRef.LoadAssetAsync();
            yield return loadEvent;
            _Event = loadEvent.Result;
            _Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _Event?.UnregisterListener(this);
        }

        public void OnEventRaised(TowerUnit tower)
        {
            Response.Invoke(tower);
        }
    }
}