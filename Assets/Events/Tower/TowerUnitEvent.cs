﻿using System.Collections.Generic;
using Assets.GridMode.Units.Tower;
using UnityEngine;

namespace Assets.Events.Tower
{
    [CreateAssetMenu(menuName = "Events/Map/Tower")]
    public sealed class TowerUnitEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<TowerUnitEventListener> _EventListeners =
            new List<TowerUnitEventListener>();

        public void Raise(TowerUnit tower)
        {
            for (int i = _EventListeners.Count - 1; i >= 0; i--)
            {
                if (i >= _EventListeners.Count)
                {
                    continue;
                }
                _EventListeners[i].OnEventRaised(tower);
            }
        }

        public void RegisterListener(TowerUnitEventListener listener)
        {
            if (!_EventListeners.Contains(listener))
                _EventListeners.Add(listener);
        }

        public void UnregisterListener(TowerUnitEventListener listener)
        {
            if (_EventListeners.Contains(listener))
                _EventListeners.Remove(listener);
        }
    }
}