﻿using System;
using Assets.GridMode.Units.Tower;
using UnityEngine.Events;

namespace Assets.Events.Tower
{
    [Serializable]
    public class TowerUnitUnityEvent : UnityEvent<TowerUnit>
    {

    }
}