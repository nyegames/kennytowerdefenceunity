﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.StartingScreen
{
    public class StartGame : MonoBehaviour
    {
        public void GameServicesInitialised()
        {
            SceneManager.LoadScene(1);
        }
    }
}
