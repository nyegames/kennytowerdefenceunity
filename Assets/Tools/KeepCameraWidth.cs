using UnityEngine;

namespace Assets.Tools
{
    public class KeepCameraWidth : MonoBehaviour
    {
        //https://www.youtube.com/watch?v=TYNF5PifSmA

        [Header("Defaults")]
        [SerializeField] private bool _PopulateDefaultSize;
        [SerializeField] private float _DefaultWidth;
        [SerializeField] private float _DefaultHeight;
        [SerializeField] private Vector3 _DefaultPosition;

        [Header("Toggles")]
        [SerializeField] private bool _MaintainWidth = true;

        [Header("Information")]
        [SerializeField] private Camera _Camera;
        [SerializeField] [Range(-1, 1)] private float _AnchorPoint = 0f;

        // Start is called before the first frame update
        private void Start()
        {
            _DefaultPosition = _Camera.transform.position;
            if (_PopulateDefaultSize)
            {
                _DefaultWidth = _Camera.orthographicSize * _Camera.aspect;
                _DefaultHeight = _Camera.orthographicSize;
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (_Camera == null) return;

            if (_MaintainWidth)
            {
                float newWidth = _DefaultWidth / _Camera.aspect;
                _Camera.orthographicSize = newWidth;

                //To keep the camera always showing the bottom
                _Camera.transform.position = _DefaultPosition + new Vector3(0, _AnchorPoint * (_DefaultHeight - _Camera.orthographicSize), 0);
            }
            else
            {
                _Camera.transform.position = _DefaultPosition + new Vector3(_AnchorPoint * (_DefaultWidth - _Camera.orthographicSize * _Camera.aspect), 0, 0);
            }
        }
    }
}
