﻿namespace Assets.Tools
{
    public static class Normaliser
    {
        public static float Normalise(this float val, float min, float max)
        {
            return (val - min) / (max - min);
        }

        public static float Normalise(this int val, float min, float max)
        {
            return ((float)val).Normalise(min, max);
        }

        public static float Normalise(this long val, float min, float max)
        {
            return ((float)val).Normalise(min, max);
        }

        public static float Normalise(this double val, float min, float max)
        {
            return ((float)val).Normalise(min, max);
        }

        /// <summary>
        /// Take the current value, and give its current known min/max values it can be.
        /// Then give back a value that would be the same proportion if it were in the target min/max range.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="targetRangeMin"></param>
        /// <param name="targetRangeMax"></param>
        /// <returns></returns>
        public static float Normalise(this float value, float min, float max, float targetRangeMin, float targetRangeMax)
        {
            float oldRange = max - min;
            /*if (OldRange == 0)
            {
                return targetMin;
            }
            else*/
            {
                float newRange = (targetRangeMax - targetRangeMin);
                return (((value - min) * newRange) / oldRange) + targetRangeMin;
            }
        }
    }
}
