﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Assets.Tools
{
    public static class AssetRefExtension
    {
        private static OperationAsync<T> LoadAsset<T>(this MonoBehaviour mono, AssetReference assetReference)
        {
            //TODO - Could use a singleton component

            AsyncOperationHandle<T> handle = assetReference.LoadAssetAsync<T>();
            mono.StartCoroutine(handle);
            return new OperationAsync<T>(() => handle.IsDone, () => handle.Result);
        }
    }
}
