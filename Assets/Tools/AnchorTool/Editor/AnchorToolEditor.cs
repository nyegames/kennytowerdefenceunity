﻿using UnityEditor;
using UnityEngine;

namespace Assets.Tools.AnchorTool.Editor
{
    [CustomEditor(typeof(Tools.AnchorTool.AnchorTool))]
    public class AnchorToolEditor : UnityEditor.Editor
    {
        void OnSceneGUI()
        {
            if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
            {
                Tools.AnchorTool.AnchorTool myTarget = (Tools.AnchorTool.AnchorTool)target;
                myTarget.StopDrag();
            }
        }
    }
}

//This script must be placed in a folder called "Editor" in the root of the "Assets"
//Otherwise the script will not work as intended