﻿using UnityEngine;

namespace Assets.Tools.AnchorTool
{
    [ExecuteInEditMode]
    public class AnchorTool : MonoBehaviour
    {
        public bool ManualRefresh = true;
        public Rect AnchorRect;
        public Vector2 AnchorVector;
        private Rect _AnchorRectOld;
        private Vector2 _AnchorVectorOld;
        private RectTransform _OwnRectTransform;
        private RectTransform _ParentRectTransform;
        private Vector2 _PivotOld;
        private Vector2 _OffsetMinOld;
        private Vector2 _OffsetMaxOld;

        void Update()
        {
#if UNITY_EDITOR
            _OwnRectTransform = gameObject.GetComponent<RectTransform>();
            _ParentRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
            if (_OwnRectTransform.offsetMin != _OffsetMinOld || _OwnRectTransform.offsetMax != _OffsetMaxOld)
            {
                CalculateCurrentWH();
                CalculateCurrentXY();
            }
            if (_OwnRectTransform.pivot != _PivotOld || AnchorVector != _AnchorVectorOld)
            {
                CalculateCurrentXY();
                _PivotOld = _OwnRectTransform.pivot;
                _AnchorVectorOld = AnchorVector;
            }
            if (AnchorRect != _AnchorRectOld)
            {
                AnchorsToCorners();
                _AnchorRectOld = AnchorRect;
            }
            if (ManualRefresh)
            {
                ManualRefresh = false;
                CalculateCurrentWH();
                CalculateCurrentXY();
                AnchorsToCorners();
            }
#endif
        }

        public void UpdateAnchors()
        {
            CalculateCurrentWH();
            CalculateCurrentXY();
            AnchorsToCorners();
        }

        public void StopDrag()
        {
            UpdateAnchors();
        }

        private void CalculateCurrentXY()
        {
            float pivotX = AnchorRect.width * _OwnRectTransform.pivot.x;
            float pivotY = AnchorRect.height * (1 - _OwnRectTransform.pivot.y);
            Vector2 newXY = new Vector2(_OwnRectTransform.anchorMin.x * _ParentRectTransform.rect.width + _OwnRectTransform.offsetMin.x + pivotX - _ParentRectTransform.rect.width * AnchorVector.x,
                -(1 - _OwnRectTransform.anchorMax.y) * _ParentRectTransform.rect.height + _OwnRectTransform.offsetMax.y - pivotY + _ParentRectTransform.rect.height * (1 - AnchorVector.y));
            AnchorRect.x = newXY.x;
            AnchorRect.y = newXY.y;
            _AnchorRectOld = AnchorRect;
        }

        private void CalculateCurrentWH()
        {
            AnchorRect.width = _OwnRectTransform.rect.width;
            AnchorRect.height = _OwnRectTransform.rect.height;
            _AnchorRectOld = AnchorRect;
        }

        private void AnchorsToCorners()
        {
            float pivotX = AnchorRect.width * _OwnRectTransform.pivot.x;
            float pivotY = AnchorRect.height * (1 - _OwnRectTransform.pivot.y);
            _OwnRectTransform.anchorMin = new Vector2(0f, 1f);
            _OwnRectTransform.anchorMax = new Vector2(0f, 1f);
            _OwnRectTransform.offsetMin = new Vector2(AnchorRect.x / _OwnRectTransform.localScale.x, AnchorRect.y / _OwnRectTransform.localScale.y - AnchorRect.height);
            _OwnRectTransform.offsetMax = new Vector2(AnchorRect.x / _OwnRectTransform.localScale.x + AnchorRect.width, AnchorRect.y / _OwnRectTransform.localScale.y);
            _OwnRectTransform.anchorMin = new Vector2(_OwnRectTransform.anchorMin.x + AnchorVector.x + (_OwnRectTransform.offsetMin.x - pivotX) / _ParentRectTransform.rect.width * _OwnRectTransform.localScale.x,
                _OwnRectTransform.anchorMin.y - (1 - AnchorVector.y) + (_OwnRectTransform.offsetMin.y + pivotY) / _ParentRectTransform.rect.height * _OwnRectTransform.localScale.y);
            _OwnRectTransform.anchorMax = new Vector2(_OwnRectTransform.anchorMax.x + AnchorVector.x + (_OwnRectTransform.offsetMax.x - pivotX) / _ParentRectTransform.rect.width * _OwnRectTransform.localScale.x,
                _OwnRectTransform.anchorMax.y - (1 - AnchorVector.y) + (_OwnRectTransform.offsetMax.y + pivotY) / _ParentRectTransform.rect.height * _OwnRectTransform.localScale.y);
            _OwnRectTransform.offsetMin = new Vector2((0 - _OwnRectTransform.pivot.x) * AnchorRect.width * (1 - _OwnRectTransform.localScale.x), (0 - _OwnRectTransform.pivot.y) * AnchorRect.height * (1 - _OwnRectTransform.localScale.y));
            _OwnRectTransform.offsetMax = new Vector2((1 - _OwnRectTransform.pivot.x) * AnchorRect.width * (1 - _OwnRectTransform.localScale.x), (1 - _OwnRectTransform.pivot.y) * AnchorRect.height * (1 - _OwnRectTransform.localScale.y));

            _OffsetMinOld = _OwnRectTransform.offsetMin;
            _OffsetMaxOld = _OwnRectTransform.offsetMax;
        }
    }
}

//X and Y set the position of the Pivot relative to the parent Rect
//Anchor X and Y set where on the parent Rect the Pivot is relative to
//Where (0, 0) is the bottom left corner of parent Rect and (1, 1) the top right