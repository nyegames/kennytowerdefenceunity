﻿using System;
using UnityEngine;

namespace Assets.Tools
{
    public class OperationAsync<T> : CustomYieldInstruction
    {
        private readonly Func<bool> _OnComplete;
        private readonly Func<T> _Result;

        public T Result => _Result.Invoke();

        public OperationAsync(Func<bool> onComplete, Func<T> result)
        {
            _OnComplete = onComplete;
            _Result = result;
        }

        #region Overrides of CustomYieldInstruction

        public override bool keepWaiting => _OnComplete?.Invoke() ?? false;

        #endregion
    }
}
